import java.awt.Component;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.xml.bind.DatatypeConverter;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

public class Calibration_Worker extends SwingWorker<String, String> {
	
	
	//Variable definitions
	public static String SP_NULL = "SPNULL";
	public static String SP_ACCESS_DENIED = "ACCESSDENIED";
	public static String SP_CLOSED = "SPCLOSED";
	public static String SP_OPEN_ERROR = "SP_OPEN_ERROR";
	public static String INVALID_PORT = "INVALID_PORT";
	public static String result = "";
	public static final String CAL_START = "{CAL_START}";
	public static final String  CAL_READADC = "{CAL_READ,ADC}";
	public static final String CAL_READCS = "{CAL_READ,CS}";
	public static final String CAL_ALIGN = "{CAL_ALIGN}";
	public static final String CAL_DAC = "{CAL_DAC";
	public static final String CAL_END = "{CAL_END}";
	public static final String CAL_SAVE = "{CAL_SAVE}";
	public static final String CAL_VERIFY = "{CAL_VERIFY}";
	public static final String CAL_RGB = "{CAL_RGB";
	
	
	
	public static boolean Null_Charflag = false;

	
	public static boolean live_flag = false;
	
	public static SerialPort serialport; 
	public static int totalbytes = 0;
	public static int Timeout_Flag = 0;
	public static int Notify_Flag = 0;
	
	public final static Object lockObject = new Object();
	
	public static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(3);
	
	public static long serial_read_cmd_timeout = 2000; //Max time in ms after which timeout is triggered if at all no data is received
	public static long serial_read_byte_timeout = 50;
	
	public static String Control_String;
	public static SerialPort calibration_port;
	public static boolean Listener_Flag = false;;
	public static boolean Write_flag = false;
	public static int Intensity_Level;
	public static String Which_DAC;
	
	public static int rintensity = 0;
	public static int gintensity = 0;
	public static int bintensity = 0;
	
	//Schedule Future Description
	public static ScheduledFuture<?> future; //for serialport incoming data event
	public static  ScheduledFuture<?> futuree; //for triggering timeout timer
	
	protected static final ByteArrayOutputStream full_buffer = new ByteArrayOutputStream();
	//Constructor of the class
	public Calibration_Worker(String Command_String, String Which_DAC,int Intensity_Level,  SerialPort serialport, boolean Listener_Flag)
	{
		
		this.Control_String = Command_String;
		this.Which_DAC = Which_DAC;
		this.Intensity_Level = Intensity_Level;
		this.calibration_port = serialport;
		this.Listener_Flag = Listener_Flag;
		
		this.rintensity = rintensity;
		this.gintensity = gintensity;
		this.bintensity = bintensity;
		System.out.println("Red Intensity : " + rintensity);
		System.out.println("Green Intensity : " + gintensity);
		System.out.println("Blue Intensity : " + bintensity);
	}
    
	@Override
	protected String doInBackground() throws Exception {
		
		String Mapped_String = Calibration.calibration_btnname_mapping.get(Control_String);
		System.out.println("Control String : "+ Mapped_String);
		
		switch(Mapped_String)
		{
		
		case CAL_START:
			String temp = CAL_START;
			System.out.println("Inside Calibration Start Command Case!!");
			try
			{
				System.out.println("Sending CAL_START Command!!");
				Send_Cmd_and_wait_for_Timeout(CAL_START, Listener_Flag);
				System.out.println("Returning value for doInBackground");
				return CAL_START;
			   //	return Connect_String;
				
			}catch(Exception ex)
			{
				//System.out.println("Exception While Sending " + Connect_String + " Command!!");
				ex.printStackTrace();
			}
			break;
		
		case CAL_RGB:
			System.out.println("Sending CAL_RGB Command Case!!");
			try
			{
				System.out.println("Sending CAL_RGB Command!!");
				
				
				
				
				
				Send_Cmd_and_wait_for_Timeout(CAL_RGB  +Which_DAC + "}", Listener_Flag);
				System.out.println("Returning value for doInBackground");
				return CAL_RGB;
				
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			break;
			
		case CAL_READADC:
			System.out.println("Inside Calibration ReadADC Command Case!!");
			try
			{
				System.out.println("Sending CAL_READADC Command!!");
				Send_Cmd_and_wait_for_Timeout(CAL_READADC, Listener_Flag);
				return CAL_READADC;
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			break;
		
		
		case CAL_READCS:
			System.out.println("Inside Calibration ReadCS Command Case!!");
			try
			{
				System.out.println("Sending CAL_READCS Command!!");
				Send_Cmd_and_wait_for_Timeout(CAL_READCS, Listener_Flag);
				return CAL_READCS;
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			break;
		
		
		case CAL_DAC:
			System.out.println("Inside Calibration DAC Command Case!!");
			try
			{
				//Maintain 3digit DAC values.
				String Intensity = String.format("%03d", Intensity_Level);
				
				String DAC_Frame = CAL_DAC + "," + Which_DAC + ",#" +Intensity + "}";
				System.out.println("DAC Command : " + DAC_Frame);
				System.out.println("Sending CAL_READCS Command!!");
				Send_Cmd_and_wait_for_Timeout(DAC_Frame, Listener_Flag);
				return CAL_DAC;
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			break;
			
		case CAL_ALIGN:
			System.out.println("Inside Calibration ALIGN command case!!");
			try
			{
				System.out.println("Sending CAL_READCS Command!!");
				Send_Cmd_and_wait_for_Timeout(CAL_ALIGN, Listener_Flag);
				return CAL_ALIGN;
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			break;
			
		case CAL_END:
			System.out.println("Inside Calibraiton END Command Case!!");
			try
			{
				System.out.println("Sending CAL_END Command!!");
				Send_Cmd_and_wait_for_Timeout(CAL_END, Listener_Flag);
				return CAL_END;
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			break;
			
		case CAL_SAVE:
			System.out.println("Inside Calibration SAVE Command Case!!");
			try
			{
				System.out.println("Sending CAL_SAVE Command!!");
				Send_Cmd_and_wait_for_Timeout(CAL_SAVE, Listener_Flag);
				return CAL_SAVE;
				
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			break;
			
		case CAL_VERIFY:
			System.out.println("Inside Calibration VERIFY Command Case!!");
			try
			{
				System.out.println("Sending CAL_VERIFY Command!!");
				Send_Cmd_and_wait_for_Timeout(CAL_VERIFY, Listener_Flag);
				return CAL_VERIFY;
				
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			break;
		
			
		default:
			break;
		}
		
		//For adding INtensity to the slider command string
		if(!Which_DAC.equals(""))
		{
			System.out.println("Current_DAC : " + Which_DAC);
			String temp = Mapped_String + "," + Which_DAC + ",#" + Intensity_Level + "}";
			
			
		}
		else
		{
			System.out.println("Current_Command : " + Mapped_String);
		}

		// TODO Auto-generated method stub
		return null;
	}
	
	protected void process(List<String> chunks)
	{
		for(String str : chunks)
		{
			//UI Update for response ok for START
			if(str.equals(CAL_START))
			{
				if(Calibration.rdbtnManual.isSelected())
				{
					//Disable the start button
					Calibration.btnStart.setEnabled(false);
					
					Calibration.btnSave.setEnabled(false);
					Calibration.btnVerify.setEnabled(false);
					Calibration.btnEnd.setEnabled(true);
					
					Calibration.btnReadAdc.setEnabled(true);
					Calibration.btnReadCs.setEnabled(true);
					
					Calibration.btnSave.setEnabled(true);
					Calibration.btnVerify.setEnabled(true);
					
					Calibration.rdbtnAuto.setEnabled(false);
					Calibration.rdbtnManual.setEnabled(false);
					
					for(Component c : Calibration.adcpanel.getComponents())
					{
						c.setEnabled(true);
					}
					
					System.out.println(Calibration.formattedTextField_adcred.isEnabled());
					System.out.println(Calibration.formattedTextField_adcgreen.isEnabled());
					System.out.println(Calibration.formattedTextField_adcblue.isEnabled());
					
					for(Component c : Calibration.rgbpanel.getComponents())
					{
						//
						c.setEnabled(true);
					}
					
					for(Component c : Calibration.colorsensorpanel.getComponents())
					{
						c.setEnabled(true);
					}
				}
				
				
			}

			else if(str.equals(CAL_END))
			{
				Calibration.btnEnd.setEnabled(false);
				Calibration.btnSave.setEnabled(false);
				Calibration.btnReadAdc.setEnabled(false);
				Calibration.btnReadCs.setEnabled(false);
				Calibration.btnVerify.setEnabled(false);
				Calibration.btnStart.setEnabled(true);
				
				
				Calibration.btnSave.setEnabled(false);
				Calibration.btnVerify.setEnabled(false);
				for(Component c : Calibration.rgbpanel.getComponents())
				{
					c.setEnabled(false);
					
				}
				for(Component c : Calibration.adcpanel.getComponents())
				{
					c.setEnabled(false);
				}
				
				for(Component c : Calibration.colorsensorpanel.getComponents())
				{
					c.setEnabled(false);
				}
				for(Component c: Calibration.Wlcpanel.getComponents())
				{
					c.setEnabled(false);
					
				}
				System.out.println("CALIBRATE END");
			}
			else if(str.startsWith(CAL_RGB))
			{
				Calibration.textArea.append("RGB Cal Success!!\r\n\n");
			}
				
				
			else if(str.equals(CAL_READADC))
			{
					String local = result;
					result = "";
					local = local.replace("{", "");
					local = local.replace("}", "");
					System.out.println(local + "\r\n");
					String[] local_arr = local.split(",");
					if(local_arr.length != 5)
					{
						
						JOptionPane.showMessageDialog(Calibration.frmLWlCalibration, "No sufficient data in response for READ_ADC Command", "ADC READ", JOptionPane.ERROR_MESSAGE);
					}
					else
					{
						Calibration.formattedTextField_adcred.setText(local_arr[2]);
						Calibration.formattedTextField_adcgreen.setText(local_arr[3]);
						Calibration.formattedTextField_adcblue.setText(local_arr[4]);
					}
					
			}
			else if(str.equals(CAL_READCS))
			{
				String local = result;
				result = "";
				local = local.replace("{", "");
				local = local.replace("}", "");
				System.out.println(local + "\r\n");
				String[] local_arr = local.split(",");
				if(local_arr.length != 10)
				{
					
					JOptionPane.showMessageDialog(Calibration.frmLWlCalibration, "No sufficient data in response for READ_ADC Command", "ADC READ", JOptionPane.ERROR_MESSAGE);
				}
				else
				{
					Calibration.formattedTextField_cs1a.setText(local_arr[2]);
					Calibration.formattedTextField_cs1b.setText(local_arr[3]);
					Calibration.formattedTextField_cs1c.setText(local_arr[4]);
					Calibration.formattedTextField_cs1d.setText(local_arr[5]);
					
					Calibration.formattedTextField_cs2a.setText(local_arr[6]);
					Calibration.formattedTextField_cs2b.setText(local_arr[7]);
					Calibration.formattedTextField_cs2c.setText(local_arr[8]);
					Calibration.formattedTextField_cs2d.setText(local_arr[9]);
					
				}
			}
			else if(str.startsWith("TIMEOUT"))
			{
				String[] loc = str.split(",");
				String local = "Response Timedout for Sending " + loc[1] + " Command\r\n\n";
				Calibration.textArea.append(local);
			}
			else if(str.equals(SP_NULL))
			{
				JOptionPane.showMessageDialog(Calibration.frmLWlCalibration, "Invalid Serial Port", "COM Port", JOptionPane.ERROR_MESSAGE);
				
			}
			else if(str.equals(SP_CLOSED))
			{
				JOptionPane.showMessageDialog(Calibration.frmLWlCalibration, "Serial Port Closed", "COM Port", JOptionPane.ERROR_MESSAGE);
			}
			else if(str.equals(SP_ACCESS_DENIED))
			{
				JOptionPane.showMessageDialog(Calibration.frmLWlCalibration, "Port is currently opened by some other application", "COM Port", JOptionPane.ERROR_MESSAGE);
			}
			else
			{
				if(!str.startsWith("{CAL_DAC"))
				Calibration.textArea.append(str);
			}
		}
	}
	//#########################################################################################################################
	public void Send_Cmd_and_wait_for_Timeout(String Current_Control, boolean Listener_Flag)
	{
		System.out.println("Listener2Flag : " + Listener_Flag);
	try
	{
		if(calibration_port != null)
		{
			System.out.println("Inside Sending Command and receive response Function!!");
		  //Check whether the serialport is already closed. If closed, open the port
			System.out.println(LocalDateTime.now());
			if(!calibration_port.isOpen())
			{
				//Open the serial port if it is closed
				calibration_port.openPort();
			}
			
			System.out.println(LocalDateTime.now());
			System.out.println("Serial Port is : " + calibration_port.isOpen());
			if(calibration_port.isOpen() == true)
			{
				byte[] temp = null ;
				int bytes_available = calibration_port.bytesAvailable();
				
				System.out.println("Total Bytes available at the serial buffer before sending command : " + bytes_available);
				if(bytes_available > 0)
				{
					temp = new byte[bytes_available];
					int d = calibration_port.readBytes(temp, bytes_available);
				}
			    try {
			    	full_buffer.reset();
					full_buffer.flush();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
		    	if(Listener_Flag)
		    	{
		    		//If listener flag is set, set listener
		    		System.out.println("Data Listener Flag : " + Listener_Flag + "Current_Command : " + Current_Control);
		    		setlistener(calibration_port);
		    		System.out.println("Data Listener Set!!");
		    	}
		    	else
		    	{
		    		//If not listener flag, remove the data listener
		    		System.out.println("Data Listener Flag : " + Listener_Flag);
		    		calibration_port.removeDataListener();
		    		System.out.println("Data Listener Removed!!");
		    	}
		    	//Convert the current string into a byte to write into serial port
		    	byte[] SerialBytes = Current_Control.getBytes();
		    	//Write bytes to serial port
		    	calibration_port.writeBytes(SerialBytes, SerialBytes.length);
		    	String helloHex = DatatypeConverter.printHexBinary(SerialBytes);
				//System.out.println("Hessl : " + helloHex);
		    	System.out.println("Serial Data Sent : " + helloHex);
		    	System.out.println("Written to serial port");
		    	System.out.println(LocalDateTime.now());
		    	Write_flag  = true;
		    	//ManufacturingTests.btnStartButton.setName("PAUSE");
		    	publish("Command Sent " + Current_Control + "\r\n");
		    	if(Write_flag )
			    {
		    		//Wait for response if Listener_Flag is set for current IPC Command
		    		if(Listener_Flag)
		    		{
		    			publish("Waiting For Response...\r\n");
		    			
		    			Stryk_Demo.Response_Animator.Animation.setVisible(true);
			    		System.out.println("Current_Control : " + Current_Control);
			    		if(Current_Control!="")
			    		{
			    			if(Current_Control.startsWith("{CAL_DAC"))
			    			{
			    				serial_read_cmd_timeout = 2000;
			    			}
			    			else if(Current_Control.startsWith("{CAL_RGB"))
			    			{
			    				serial_read_cmd_timeout = 2000;
			    			}
			    			else
			    			{
			    				serial_read_cmd_timeout = Calibration.calibration_cmd_tout_mapping.get(Current_Control);
			    			}
			    		}
			    		int status = 0;
			    		
			    		status = Send_cmd_wait_for_Timeout(serial_read_cmd_timeout, Current_Control);
			    		System.out.println("###################" + serial_read_cmd_timeout + "#####################");

				    	if(status == 1)
				    	{
				    		//JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Reponse Timedout for sending " + Query_String +  "Command", "ManufacturingTests", JOptionPane.ERROR_MESSAGE);
				    		System.out.println("Response Timedout for sending " + Current_Control  +  " Command" + LocalDateTime.now());	
				    		
				    	}
				    	else
				    	{
				    		//JOptionPane.showMessageDialog(ManufacturingTests.frmStrykerManufacturingTests, "Status : " + status, "Status", JOptionPane.INFORMATION_MESSAGE);
				    		System.out.println("Status : " + status);
				    	}
		    		}
		    		else
		    		{
		    			if(future != null)
			    		{
			    			future.cancel(true);
			    		}
			    		if(futuree != null)
			    		{
			    			futuree.cancel(true);
			    		}
		    			//Stryk_Demo.pause_flag = false;
		    		}
		    		//else don't wait for a response or timeout. Just send the command
			    }
		    	else
		    	{
		    		System.out.println("Could not write data into serial port. Write flag is false");
		    	}
		    	 	
			} 
			else 
			{
				publish(SP_OPEN_ERROR);
				System.out.println("Could not open Serial port. Exiting Now!!");
		    	publish("Could not send " + Current_Control + " command \r\n");
		    	publish("Access to COM Port Denied!!. Port is in Access by Another Application!!\r\n\n");
		    	
				
			}
		}
    	else
    	{
    		System.out.println("Serial Port is Null");
    		publish(INVALID_PORT);
    		
    	}
	}catch(Exception ex)
	{
		ex.printStackTrace();
	}
	
	}
	
	//#############################################################################################################
	//#####################################################################################################################################
	//Function to send the given IPC Frame to the device and to wait/not wait for the response to be received or timeout to happen 
	public int Send_cmd_wait_for_Timeout(long timeout, final String Current_Control)
	{
		long tout = timeout;
		
		final Runnable timeout_task_performer = new Runnable()
    	{
			@Override
			public void run() 
			{
				synchronized(lockObject)
				{	
					Timeout_Flag = 1;
					lockObject.notifyAll();
				}
				
			}
			
		};
		final Runnable update_GUI_state = new Runnable() 
		{
			@Override
			public void run() 
			{
				System.out.println("Waiting here for response or timeout here..");
				System.out.println("Pause Flag : " + Stryk_Demo.pause_flag);
				synchronized(lockObject)
				{
					
					while (Timeout_Flag == 0 && Notify_Flag == 0 )
					{	
						try 
						{
							lockObject.wait(10);
						} 
						catch (Exception e)
						{
							e.printStackTrace();
						}
					}
					
					Stryk_Demo.Response_Animator.Animation.setVisible(false);
					Stryk_Demo.print("exited while LOPP");
					System.out.println("Timeout Flag : " + Timeout_Flag);
					System.out.println("Notify_Flag : " + Notify_Flag);
					System.out.println("Null_Charflag : " + Null_Charflag);
					if(futuree != null)
					{
						System.out.println("Futuree not null!!");
						futuree.cancel(true);
					}
					else
					{
						System.out.println("Futuree null!!");
					}
					
				}
				// While ends here
				System.out.println("Here1");
				//No Data Received for sending command to device
				if (Timeout_Flag == 1 && Notify_Flag == 0) 
				{
					
					publish("Response Received : Response Timedout\r\n");
					publish("TIMEOUT," + Current_Control);
					calibration_port.openPort();
					if(calibration_port.isOpen() == true)
					{
						byte[] temp = null;
						int bytes_available = calibration_port.bytesAvailable();
						System.out.println("Total Bytes available at timeout : " + bytes_available);
						if(bytes_available > 0)
						{
							temp = new byte[bytes_available];
							calibration_port.readBytes(temp, bytes_available);
						}
						calibration_port.removeDataListener();
						//serialport.closePort();
					}
						
					System.out.println("Response Timedout. No data received in the Serial Port");
					synchronized(lockObject)
					{
						
						Write_flag = false;
						lockObject.notifyAll();
					}
					if(Stryk_Demo.pause_flag)
					{
						//Stryk_Demo.pause_flag = false;
					}
				}
				//Valid Response received for sending command to the device
				else if (Notify_Flag == 1 && Null_Charflag==true) 
				{
					
					System.out.println("Error Free Data Received!!");
					Notify_Flag = 0;
					
					@SuppressWarnings("unused")
					bytedefinitions b = new bytedefinitions();
					System.out.println("Valid Response in UART");
					System.out.println("Current Control Inside Success Response : " + Current_Control);
					calibration_port.removeDataListener();
					publish(Current_Control);
					//serialport.closePort();
					synchronized(lockObject)
					{
						Write_flag = false;
						lockObject.notifyAll();
					}
				} 
				//Response Received. But with some errors
				else if(Notify_Flag == 1 && Null_Charflag == false)
				{
					//Stryk_Demo.pause_flag = false;
					Notify_Flag = 0;
					Timeout_Flag = 0;
					
					try {
						full_buffer.reset();
						full_buffer.flush();
						//textArea.update(textArea.getGraphics());
						calibration_port.removeDataListener();
						//serialport.closePort();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("Error Response Received!!");
					synchronized(lockObject)
					{
						
						Write_flag = false;
						lockObject.notifyAll();
					}
				}
				else
				{
					System.out.println("Peculiar");
				}
				System.out.println("Here2");
			}
		};
		//Check WriteFlag whether the data is sent to serialport successfully
		if (Write_flag == true)
		{
				Timeout_Flag = 0;
				System.out.println("Write Flag Set");
				//Sets the timeout_task_performer to start exactly after serial_read_cmd_timeout milliseconds
				System.out.println("Triggering Timer...");
				futuree = scheduler.schedule(timeout_task_performer, timeout, TimeUnit.MILLISECONDS);
				System.out.println("Timer Triggered!!");
				//publish("Waiting For Response!!\r\n");
				Thread appThread = new Thread() {
					public void run() {
						try {
							//Entry point of the function
							scheduler.schedule(update_GUI_state, 0, TimeUnit.MILLISECONDS);
							//SwingUtilities.invokeLater(update_GUI_state);
						} catch (Exception ex) {
							System.out.println("Some Error");
							ex.printStackTrace();
						}
					}
				};
				appThread.run();
		} 
		else
		{
			System.out.println("WriteFlag is False");
		}
		while(Write_flag)
    	{
			synchronized(lockObject)
    		{
				try {
					lockObject.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    		//TEST Print $System.out.println("Write Flag Inside While Loop : " + Write_flag);    		
    	}
		System.out.println("Timeout_Flag while returning from function : " + Timeout_Flag);
		return Timeout_Flag;
		
	}
	//################################################################################################################
	Runnable uart_recv_timeout_task = new Runnable() {
		public void run()
		{
			System.out.println("Inside UART Receive Timeout Task");
			//If No data has been collected in the full_buffer
			if (full_buffer.size() == 0) //Previously it was full buffer == null check
			{	
				System.out.println("No data in the response!!");
				calibration_port.removeDataListener(); // Removes data listener associated with serial port Object
				totalbytes = 0;	
			} 
			//Some datahas been collected in the full_buffer
			else 
			{
				System.out.println("Full Buffer size before calling function :" + full_buffer.size());
				byte[] recv_buff = full_buffer.toByteArray(); //Copy the received bytes into a new byte array
				try
				{
					String helloHex = DatatypeConverter.printHexBinary(recv_buff);
					System.out.println("Hessl : " + helloHex);
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				
				try {
					full_buffer.flush();//Flush the receive baos after getting the received values to a new buffer
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Receive Buffer size before calling function :" + recv_buff.length );
				System.out.println("Hi");
				calibration_port.removeDataListener(); //Remove the data listener after successfully receiving some response
				result = "";
				try {
					//Result OK / Fail processed her with respect to Calibration INIT Command
					result = new String(recv_buff,"UTF-8");
					publish("Response Received :  " + result + "\r\n");
					System.out.println("Result : " + result);
					if(result.startsWith("{OK,"))
					{
						
						calibration_port.removeDataListener();
						//calibration_port.closePort();
						//System.out.println("Hi");
						Write_flag = false;
						Notify_Flag = 1;
						Null_Charflag = true;
						//Show up the calibration screen for the first time the {INIT_OK is received
						//Next time onwards it is considered to be the response for the calibration commands
						System.out.println("Calibration Flag is False");
					}
					else if(result.startsWith("{FAIL,"))
					{
						calibration_port.removeDataListener();
						//calibration_port.closePort();
						Write_flag = false;
						Notify_Flag = 1;
						Null_Charflag = false;
						Stryk_Demo.textArea.append("INIT FAILED\r\n");
						System.out.println("Error : Could not Switch to Calibration screen");
						JOptionPane.showMessageDialog(Stryk_Demo.frmStrykerVer, "Error Response Received for sending Init Command", "Calibration - Initialization", JOptionPane.ERROR_MESSAGE);
					}
					else
					{
						System.out.println(result);
					}
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Response to be processed : " + result);
				System.out.println("Inside the function");
				//System.out.println("Trigger Source : " + current_control + " Command");
				System.out.println("Buffer at Entry Point : "+Arrays.toString(recv_buff));
				}
				//recv_buff = null;
				//serialport.removeDataListener(); // Removes data listener associated with serial port Object
				totalbytes = 0;
		}
		};
//#############################################################################################################
	
	public void setlistener(final SerialPort serialport) 
	{
		System.out.println("Inside Serial Port Listener Set Function!!");
		if (serialport != null) 
		{
			serialport.addDataListener(new SerialPortDataListener() 
			{
				private int total_bytes;
				private int numRead;
				@Override
				public int getListeningEvents() 
				{
					System.out.println("Getting the list of events");
					return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
				}

				@Override
				public void serialEvent(SerialPortEvent event) 
				{
					if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE) 
					{
						Stryk_Demo.textArea.append("No response\r\n\n");
						return;
					}

					try 
					{
						//System.out.println("AA");
						byte[] newData = new byte[serialport.bytesAvailable()];
						//	System.out.println("A");
						numRead = serialport.readBytes(newData, newData.length);
						//System.out.println("B");
						total_bytes = total_bytes + numRead;
						full_buffer.write(newData, 0, numRead);
						System.out.println("Some data arrived in the Calibration Worker instance of the serial port - Calibration Port");
						if(future!= null)
						{
							future.cancel(true);
						}

						future = scheduler.schedule(uart_recv_timeout_task, serial_read_byte_timeout,TimeUnit.MILLISECONDS);



						// 	#uart_response_timer.start();
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
						return;
					}

				}

			});

		} 
		else 
		{
			System.out.println("No Serial ports found in the system");
		}
	}
}
