import java.awt.Color;

public class MyCellRenderer extends javax.swing.table.DefaultTableCellRenderer {
	
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, java.lang.Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        java.awt.Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if(ManufacturingTests.pass_flag == true)
        {
        	System.out.println("Setting Green Bg");
        	setBackground(java.awt.Color.GREEN);
        	
        }
        else
        {
        	System.out.println("Setting Red Bg");
        	setBackground(java.awt.Color.RED);
        }
        
        return cellComponent;
    }
    public static void main (String args[])
    {
    	
    }
}