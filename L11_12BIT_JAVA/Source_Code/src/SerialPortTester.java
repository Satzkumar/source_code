import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.Timer;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

public class SerialPortTester {
	public static SerialPort serialport;
	public static SerialPort[] portlist;
	public static Timer timer;
	public static void main(String[] args) {
		System.out.println("Inside Main Function");
		portlist = SerialPort.getCommPorts();
		for(int i=0;i<portlist.length;i++)
		{
			System.out.println(portlist[i].getDescriptivePortName());
		}
		byte[] buff = "String".getBytes();
		try {
			serialport = portlist[0];
			serialport.setBaudRate(115200);
			System.out.println(serialport.getBaudRate());
			serialport.openPort();
			if(serialport.openPort() ==  true)
			{
				
				System.out.println("Selected Port : "+ serialport.getSystemPortName());
				setlistener();
				serialport.writeBytes(buff, buff.length);
				timer = new Timer(2000, taskPerformer);
				timer.setRepeats(false);
				timer.start();
				System.out.println("Listener Set");
			}
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	static ActionListener taskPerformer = new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
			// ...Perform a task...
			System.out.println("Timer has exhausted");
			removeDataListener();
			timer.stop();
		}
	};
	public static void setlistener() {
		if (serialport != null) {
			System.out.println("Serial Port Not Null");
			serialport.addDataListener(new SerialPortDataListener() {

				
				@Override
				public int getListeningEvents() {
					// TODO Auto-generated method stub
					
					return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
				}

				@Override
				public void serialEvent(SerialPortEvent event) {
					
					// TODO Auto-generated method stub
					if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE) {
						return;
						
					}
					else
					{
						System.out.println("The Event is : "+event.getEventType());
						
						System.out.println("Some Data Received");
						timer.restart();
					}
					
					
					
				
			}
			});
			}
				
				

				

		

		 else {
			System.out.println("No Serial ports found in the system");
		}
	}
	public static void removeDataListener()
	{
		try
		{
			serialport.removeDataListener();
		}catch (Exception ex)
		{
			ex.printStackTrace();
		}
		
	}
}
	


