import com.fazecast.jSerialComm.SerialPort;

// Function that adds the available serial ports as a list to the comboBox
	public class scan_serialports 
	{
		
		int sp;
		public static SerialPort[] portlist;
		public static String[] portlistforcombobox = new String[10];
		public static String portname;
		
		public boolean scan_serialport(boolean comportemptyflag){
			
		//##System.out.println("Scanning Serial Ports...");	
		// Serial Port Initialization
		try {
			
			portlist = SerialPort.getCommPorts();
			sp = portlist.length;
			
			if (sp == 0) {
				for (int noport = 0; noport < portlistforcombobox.length; noport++) {
					portlistforcombobox[noport] = null;
				}
				comportemptyflag = true;
				System.out.println("No Valid Com Port Found");
				
			}
			// Some Com Ports are present
			else {
				
				//##System.out.println("Some port(s) are detected.\nRefer the COM Port list for the available ports");
				//#System.out.println("No Of Serial Ports in the system : " + Integer.toString(sp));
				comportemptyflag = false;
				for (int i = 0; i < sp; i++) {
					portname = portlist[i].getSystemPortName();

					//##System.out.println("Port " + Integer.toString(i) + " is : " + portname);
					portlistforcombobox[i] = portname;
					portname = "";
				}
				//#System.out.println("Port list created successfully");

			}
		}

		catch (Exception e) {
			System.out.println("Exception while adding serial Ports to the user list : " + e.toString() + "\r\n");
		}
		//##System.out.println("Exiting scan_serialport() function...");
		return comportemptyflag;
	}
		public String[] get_portlist()
		{
			//##System.out.println("returning portlistforcombobox");
			//##System.out.println("Exiting get_portlist() function...");
			return portlistforcombobox;
			
		}
		public SerialPort[] get_serialports()
		{
			//##System.out.println("returning serialportlist");
			//##System.out.println("Exiting get_serialports() function...");
			return portlist;
			
		}
		
	}