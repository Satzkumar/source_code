import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.util.List;
import java.awt.SystemColor;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import javax.swing.Timer;
import javax.swing.JProgressBar;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.text.DefaultCaret;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

import javax.swing.event.ChangeEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JRadioButton;
import javax.swing.ScrollPaneConstants;
import javax.swing.JToggleButton;
import java.awt.Panel;
import java.awt.ScrollPane;
import javax.swing.border.Border;

/**
 * 
 */

/**
 * @author LakshmiNarasimman R
 *
 */

public class  ManufacturingTests{
	//String Definitions for Various Commands
	public static String check_string = "";
	public static String HashDelimitter = "#################################################################";
	public static String ArrowDelimitter = "------------------------------------------------------------------------------------";
	public static FileHandler fh_handler;
	public static FileHandler logs_handler ;
	public static FileHandler report_handler;
	
	public static long serial_read_cmd_timeout = 10000;
	public static ScheduledFuture<?> futuree;
	public static ScheduledFuture<?> runfuture;

	public static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(3);
	
	public static float Exp_RedLed_Voltage_zeropercent_noload = (float)6;
	public static float Exp_GreenLed_Voltage_zeropercent_noload = (float)6;
	public static float Exp_BlueLed_Voltage_zeropercent_noload = (float)6;
	public static float Exp_LaserLed_Voltage_zeropercent_noload = (float)3.5;
	public static float Exp_Iris1Led_Voltage_zeropercent_noload = (float)0.0;
	public static float Exp_Iris2Led_Voltage_zeropercent_noload = (float)0.0;
	public static float ExpCurrent_LedCurrentValues_zeropercent_noload = (float) 0.0;
	
	public static boolean  Stop_Flag;
	
	
	public static int State_Machine = 0;
	//Fan Tach Counts
	public static int Expected_LightEngine_FanTachCount = 220;
	public static int Expected_HeatSink_FanTachCount = 275;
	public static int Expected_Iris_FanTachCount = 135;
	
	public static int  Initializer_Value = 0;
	public static boolean worker_cancel_flag = false;
	
	//LED TESTS BELOW ARE ON LOAD
	public static float Exp_RedLed_Voltage_zeropercent =   (float)0.9;
	public static float Exp_RedLed_Current_zeropercent =   (float)0.0;
	public static float Exp_GreenLed_Voltage_zeropercent = (float)0.9;
	public static float Exp_GreenLed_Current_zeropercent = (float)0.0;
	public static float Exp_BlueLed_Voltage_zeropercent =  (float)0.9;
	public static float Exp_BlueLed_Current_zeropercent =  (float)0.0;
	public static float Exp_LaserLed_Voltage_zeropercent = (float)1.4;
	public static float Exp_LaserLed_Current_zeropercent = (float)0.0;
	public static float Exp_Iris1Led_Voltage_zeropercent = (float)1.8;
	public static float Exp_Iris1Led_Current_zeropercent = (float)0.0;
	public static float Exp_Iris2Led_Voltage_zeropercent = (float)1.8;
	public static float Exp_Iris2Led_Current_zeropercent = (float)0.0;
	
	public static float Exp_RedLed_Voltage_fiftypercent = (float) 3.2;
	public static float Exp_RedLed_Current_fiftypercent = (float) 13.3;
	public static float Exp_GreenLed_Voltage_fiftypercent = (float)4.8;
	public static float Exp_GreenLed_Current_fiftypercent = (float) 15;
	public static float Exp_BlueLed_Voltage_fiftypercent = (float)3.3;
	public static float Exp_BlueLed_Current_fiftypercent = (float)3.75;
	public static float Exp_LaserLed_Voltage_fiftypercent = (float)1.8;
	public static float Exp_LaserLed_Current_fiftypercent = (float)5;
	public static float Exp_Iris1Led_Voltage_fiftypercent = (float)1.0;
	public static float Exp_Iris1Led_Current_fiftypercent = (float)0.9;
	public static float Exp_Iris2Led_Voltage_fiftypercent = (float)1.0;
	public static float Exp_Iris2Led_Current_fiftypercent = (float)0.9;
	
	public static float Exp_RedLed_Voltage_seventyfivepercent = (float) 2.9;
	public static float Exp_RedLed_Current_seventyfivepercent = (float) 20.62;
	public static float Exp_GreenLed_Voltage_seventyfivepercent = (float) 5.2;
	public static float Exp_GreenLed_Current_seventyfivepercent = (float) 22.5;
	public static float Exp_BlueLed_Voltage_seventyfivepercent = (float)3.5;
	public static float Exp_BlueLed_Current_seventyfivepercent = (float)5.62;
	public static float Exp_LaserLed_Voltage_seventyfivepercent = (float)2.0;
	public static float Exp_LaserLed_Current_seventyfivepercent = (float)7.5;
	public static float Exp_Iris1Led_Voltage_seventyfivepercent = (float)1.1;
	public static float Exp_Iris1Led_Current_seventyfivepercent = (float)1.42;
	public static float Exp_Iris2Led_Voltage_seventyfivepercent = (float)1.1;
	public static float Exp_Iris2Led_Current_seventyfivepercent = (float)1.42;
	
	public static float Exp_RedLed_Voltage_hundredpercent = (float)3.6;
	public static float Exp_RedLed_Current_hundredpercent = (float)26.6;
	public static float Exp_GreenLed_Voltage_hundredpercent = (float)5.6;
	public static float Exp_GreenLed_Current_hundredpercent = (float)30;
	public static float Exp_BlueLed_Voltage_hundredpercent = (float)3.6;
	public static float Exp_BlueLed_Current_hundredpercent = (float)7.5;
	public static float Exp_LaserLed_Voltage_hundredpercent = (float)2.2;
	public static float Exp_LaserLed_Current_hundredpercent = (float)10;
	public static float Exp_Iris1Led_Voltage_hundredpercent = (float)0.7;
	public static float Exp_Iris1Led_Current_hundredpercent = (float)1.8;
	public static float Exp_Iris2Led_Voltage_hundredpercent = (float)0.7;
	public static float Exp_Iris2Led_Current_hundredpercent = (float)1.8;
	
	public static int Exp_envpdvoltage_zero = 0;
	public static float Exp_envpdvoltage_hundred = (float) 2.5;
	
	public static int Exp_iris1pdvoltage_zero = 0;
	public static float Exp_iris1pdvoltage_hundred = (float) 0.8;
	
	public static int Exp_iris2pdvoltage_zero = 0;
	public static float Exp_iris2pdvoltage_hundred = (float) 0.8;
	
	public static int Exp_Value_CS1 = 35;
	public static int Exp_Value_CS2 = 50;
	
	public static int Exp_wluc1pwmmuxtest_intcount = 12;
	public static int Exp_envuc1pwmmuxtest_intcount = 12;
	public static int Exp_irisuc1pwmmuxtest_intcount = 12;
	
	public static int Exp_wluc2pwmmuxtest_intcount = 12;
	public static int Exp_envuc2pwmmuxtest_intcount = 12;
	public static int Exp_irisuc2pwmmuxtest_intcount = 12;
	
	public static int Exp_wlextpwmmuxtest_intcount = 12;
	public static int Exp_envextpwmmuxtest_intcount = 12;
	public static int Exp_irisextwlpwmmuxtest_intcount = 12;
	public static int Exp_irisextenvpwmmuxtest_intcount = 12;
	
	
	bg_worker bg;
	
	
	
	//formats for file name
	public DateTimeFormatter dt = DateTimeFormatter.ofPattern("_yyyy_MM_dd");
	public DateTimeFormatter tm = DateTimeFormatter.ofPattern("HH_mm_ss");
	
	//formats for file logging
	public DateTimeFormatter date = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	public DateTimeFormatter time = DateTimeFormatter.ofPattern("HH:mm:ss");
	
	public String DateToday;
	public String TimeNow;
	public String ParentLogDirectoryName = "LOGS";
	public String ParentReportDirectoryName = "REPORTS";
	public String DateWiseReportDirectoryName;
	public String LogFileName;
	public String LogFilePath;
	public String DateWiseLogFileName;
	public String DateWiseReportFileName;
	public String ReportFileName;
	public String ReportFilePath;
	public LocalDate localDate;
	public LocalDateTime localDateTime;
	public static String Board_Number = "" ;
	public String FWVerNo =  "";
	public boolean isDirectoryCreated = false;
	
	public static Logger logger = Logger.getLogger("Creator");
	public static Logger logs_logger = Logger.getLogger("Logs");
	public static Logger report_logger = Logger.getLogger("Report");
	
	//Serial Port Params
	public static SerialPort serialport = Stryk_Demo.mftserialPort;
	
	public static JFrame frmStrykerManufacturingTests;

	public static ArrayList<Boolean> action_items = new ArrayList<Boolean>();
	public static ArrayList<JToggleButton> togglebuttons = new ArrayList<JToggleButton>();
	public static ArrayList<JLabel> test_items = new ArrayList<JLabel>();
	public static ArrayList<JToggleButton> selection_buttons = new ArrayList<JToggleButton>();
	
	public ImageIcon red_indicator = new javax.swing.ImageIcon(getClass().getResource("resources/red2.png"));
	public ImageIcon green_indicator = new javax.swing.ImageIcon(getClass().getResource("resources/green2.png"));
	//SwingWorker Definitions goes here
	public static SwingWorker<ArrayList<String>, String> autotest_bg_worker;
	public static SwingWorker<List<String>, String> cleartestresults_bg_worker;
	public static SwingWorker<Void, Void> progressbar_bg_worker;
	public static SwingWorker<Void, Void> singletest_bg_worker;
	
	//Button Definitions
	public static JButton btnStartButton;
	public static JButton btnStop;
	
	public ImageIcon red_ind = new javax. swing.ImageIcon(getClass().getResource("/resources/black.png"));
	public ImageIcon green_ind = new javax.swing.ImageIcon(getClass().getResource("/resources/blue.png"));
	
	public JToggleButton btnuc1comtestselector;
	public JToggleButton btnuc2comtestselector;
	public static JToggleButton btnipctestselector;
	public static JToggleButton btnuc1pwrtestselector;
	public static JToggleButton btnuc2pwrtestselector;
	public static JToggleButton btnuc1eepromtestselector;
	public static JToggleButton btnuc2eepromtestselector;
	public static JToggleButton btnuc2flashtestselector;
	public static JToggleButton btnuc1camuarttestselector;
	public static JToggleButton btnuc1debuguarttestselector;
	public static JToggleButton btnuc2camuarttestselector;
	public static JToggleButton btnuc2debuguarttestselector;
	public static JToggleButton btnuc2guiuarttestselector;
	public static JToggleButton btnuc2adduarttestselector;
	public static JToggleButton btnlightenginefantestselector;
	public static JToggleButton btnheatsinkfantestselector;
	public static JToggleButton btnirisfantestselector;
	public static JToggleButton btnwluc1pwmmuxtestselector;
	public static JToggleButton btnenvuc1pwmmuxtestselector;
	public static JToggleButton btnirisuc1pwmmuxtestselector;
	public static JToggleButton btnwluc2pwmmuxtestselector;
	public static JToggleButton btnenvuc2pwmmuxtestselector;
	public static JToggleButton btnirisuc2pwmmuxtestselector;
	public static JToggleButton btnwlextpwmmuxtestselector;
	public static JToggleButton btnenvextpwmmuxtestselector;
	public static JToggleButton btnirisextwlpwmmuxtestselector;
	public static JToggleButton btnirisextenvpwmmuxtestselector;
	
	public static JToggleButton btnredledzerotestselector;
	public static JToggleButton btngreenledzerotestselector;
	public static JToggleButton btnblueledzerotestselector;
	public static JToggleButton btnlaserledzerotestselector;
	public static JToggleButton btniris1ledzerotestselector;
	public static JToggleButton btniris2ledzerotestselector;
	
	public static JToggleButton btnredledfiftytestselector;
	public static JToggleButton btngreenledfiftytestselector;
	public static JToggleButton btnblueledfiftytestselector;
	public static JToggleButton btnlaserledfiftytestselector;
	public static JToggleButton btniris1ledfiftytestselector;
	public static JToggleButton btniris2ledfiftytestselector;
	
	public static JToggleButton btnredledseventyfivetestselector;
	public static JToggleButton btngreenledseventyfivetestselector;
	public static JToggleButton btnblueledseventyfivetestselector;
	public static JToggleButton btnlaserledseventyfivetestselector;
	public static JToggleButton btniris1ledseventyfivetestselector;
	public static JToggleButton btniris2ledseventyfivetestselector;
	
	public static JToggleButton btnredledhundredtestselector;
	public static JToggleButton btngreenledhundredtestselector;
	public static JToggleButton btnblueledhundredtestselector;
	public static JToggleButton btnlaserledhundredtestselector;
	public static JToggleButton btniris1ledhundredtestselector;
	public static JToggleButton btniris2ledhundredtestselector;
	
	public static JToggleButton btniris1fiberdetecton_offtestselector;
	public static JToggleButton btniris2fiberdetecton_offtestselector;
	
	public static JToggleButton btnbeamsensoron_offtestselector;
	public static JToggleButton btnesston_offtestselector;
	public static JToggleButton btnasston_offtestselector;
	public static JToggleButton btnfbpwrenableon_offtestselector;
	public static JToggleButton btnirispgoodon_offtestselector;
	public static JToggleButton btnfanfailuretestselector;
	
	public static JToggleButton btniris1zeropdtestselector;
	public static JToggleButton btniris1hundredpdtestselector;
	public static JToggleButton btniris2zeropdtestselector;
	public static JToggleButton btniris2hundredpdtestselector;
	public static JToggleButton btnenvzeropdtestselector;
	public static JToggleButton btnenvhundredpdtestselector;
	
	public static JToggleButton btncolorsensor1testselector;
	public static JToggleButton btncolorsensor2testselector;
	
	
	public final static Object lockObject = new Object();
	
	//Integer Definitions goes here
	
	
	public static int Current_Query_Number = 0;
	public static int Current_Query_Limit  = 61;
	
	
	public static int pass_count = 0;
	public static int fail_count = 0;
	public static int total_test_count = 0;
	public static int current_row_count = 0;
	
	//Common Components construction parameters
	public static int SelectionButtonYdifference = 20;
	public static int SelectionIndicatorPanelYdifference = 20;
	public static int TestDescriptionTextYdifference = 20;
	public static int TestResultIndicatorYdifference = 20;
	
	//Button Constructor specifications
	public int buttonxbasesubpanel1 = 24;
	public int buttonybasesubpanel1 = 28;
	public int buttonwidthsubpanel1 = 30;
	public int buttonheightsubpanel1 = 17;
	
	public int buttonxbasesubpanel2 = 24;
	public int buttonybasesubpanel2 = 28;
	public int buttonwidthsubpanel2 = 30;
	public int buttonheightsubpanel2 = 17;
	
	public int buttonxbasesubpanel3 = 24;
	public int buttonybasesubpanel3 = 28;
	public int buttonwidthsubpanel3 = 30;
	public int buttonheightsubpanel3 = 17;
	
	public int buttonxbasesubpanel4 = 24;
	public int buttonybasesubpanel4 = 28;
	public int buttonwidthsubpanel4 = 30;
	public int buttonheightsubpanel4 = 17;
	
	
	
	//Panel Constructor Specifications 
	public int panelxbasesubpanel1 = 34;
	public int panelybasesubpanel1 = 35;
	public int panelwidthsubpanel1 = 10;
	public int panelheightsubpanel1 = 4;
	
	public int panelxbasesubpanel2 = 34;
	public int panelybasesubpanel2 = 35;
	public int panelwidthsubpanel2 = 10;
	public int panelheightsubpanel2 = 4;
	
	public int panelxbasesubpanel3 = 34;
	public int panelybasesubpanel3 = 35;
	public int panelwidthsubpanel3 = 10;
	public int panelheightsubpanel3 = 4;
	
	public int panelxbasesubpanel4 = 34;
	public int panelybasesubpanel4 = 35;
	public int panelwidthsubpanel4 = 10;
	public int panelheightsubpanel4 = 4;
	
	//Text Description Constructor Specifications
	public int testdescriptionxbasesubpanel1 = 90;
	public int testdescriptionybasesubpanel1 = 30;
	public int testdescriptionwidthsubpanel1 = 136;
	public int testdescriptionheightsubpanel1 = 14;
	
	public int testdescriptionxbasesubpanel2 = 90;
	public int testdescriptionybasesubpanel2 = 30;
	public int testdescriptionwidthsubpanel2 = 136;
	public int testdescriptionheightsubpanel2 = 14;
	
	public int testdescriptionxbasesubpanel3 = 90;
	public int testdescriptionybasesubpanel3 = 30;
	public int testdescriptionwidthsubpanel3 = 136;
	public int testdescriptionheightsubpanel3 = 14;
	
	public int testdescriptionxbasesubpanel4 = 90;
	public int testdescriptionybasesubpanel4 = 30;
	public int testdescriptionwidthsubpanel4 = 136;
	public int testdescriptionheightsubpanel4 = 14;
	
	
	//Result Indicator Constructor Specifications
	public int resultindicatorxbasesubpanel1 = 221;
	public int resultindicatorybasesubpanel1 = 32;
	public int resultindicatorwidthsubpanel1 = 28;
	public int resultindicatorheightsubpanel1 = 10;
	
	public int resultindicatorxbasesubpanel2 = 221;
	public int resultindicatorybasesubpanel2 = 32;
	public int resultindicatorwidthsubpanel2 = 28;
	public int resultindicatorheightsubpanel2 = 10;
	
	public int resultindicatorxbasesubpanel3 = 221;
	public int resultindicatorybasesubpanel3 = 32;
	public int resultindicatorwidthsubpanel3 = 28;
	public int resultindicatorheightsubpanel3 = 10;
	
	public int resultindicatorxbasesubpanel4 = 221;
	public int resultindicatorybasesubpanel4 = 32;
	public int resultindicatorwidthsubpanel4 = 28;
	public int resultindicatorheightsubpanel4 = 10;
	
	
	//Default Size Definitons for buttons, panels and text areas
	public int SelectionButtonWidth = 30;
	public int SelectionButtonHeight = 17;
	
	public int SelectionIndicatorPanelWidth = 10;
	public int SelectionIndicatorPanelHeight = 4;
	
	public int TestDescriptionTextWidth = 136;
	public int TestDescriptionTextHeight = 14;
	
	public int TestResultIndicatorpanelWidth = 28;
	public int TestResultIndicatorpanelHeight = 10;
	
	public static JTextArea textArea;
	//String Definitions
	
	
	public DefaultTableModel model;
    
	//Time Definitions goes here
	public static long sleep_time = 2;
	 
	//Integer Variables goes here
	public static int pbval = 0;
	public static int pbval_divisor = 0;
	public static int update_value = 0;
	//ButtonGroup Definitions goes here
	public ButtonGroup TestModes = new ButtonGroup();
	public ButtonGroup ManualTestModes = new ButtonGroup();
	
	
	public static boolean busyflag 				= false;
	public static boolean MFTest_Flag 			= false;//Flag to process ascii response fromm serial port
	public static boolean MFTest_ExitFlag 		= false;
	public static boolean pass_flag 			= false;
	public static boolean isdone 				= false;
	public static boolean action_flag 			= false;
	public static boolean finish_button_flag 	= false;
	
	
	public JCheckBox chckbxTestItem3;
	public JCheckBox chckbxTestItem4;
	public JRadioButton rdbtnClearTestSelections;
	
	
	
	
	public static ArrayList<JPanel> subpanellist = new ArrayList<JPanel>();
	public static FileHandler fh ;
	
   
	public static JTextField textField_Totaltests;
	public static JTextField textField_Passedtests;
	public static JTextField textField_Failedtests;
	
	public static int snowidth = 0;
	
	
	//Panel Definitions - Container Panels
	public static JPanel test_control_panel;
	public static JPanel test_count_panel;
	public static JPanel board_number_panel;
	public static JPanel result_and_progress_panel;
	public static JPanel panel_4;
	public static JPanel firmware_version_number_panel;
	public static JPanel logspanel;
	public static JPanel test_configuration_panel;
	public static JPanel MainTestpanel;
	public static JPanel SubTestPanel_1;
	public static JPanel SubTestPanel_2;
	public static JPanel SubTestPanel_3;
	public static JPanel SubTestPanel_4;
	public static JPanel SubTestPanel_5;
	public static JPanel uc2pwrtestselectionindicatorpanel;
	public static JPanel uc1eepromtestselectionindicatorpanel;
	public static JPanel uc2eepromtestselectionindicatorpanel;
	public static JPanel uc2falshtestselectionindicatorpanel;
	public static JPanel uc1camuarttestselectionindicatorpanel;
	public static JPanel uc1debuguarttestselectionindicatorpanel;
	public static JPanel uc2camuarttestselectionindicatorpanel;
	public static JPanel uc2debuguarttestselectionindicatorpanel;
	public static JPanel uc2guiuarttestselectionindicatorpanel;
	public static JPanel uc2adduarttestselectionindicatorpanel;
	public static JPanel lightenginefantestselectionindicatorpanel;
	public static JPanel heatsinkfantestselectionindicatorpanel;
	public static JPanel irisfantestselectionindicatorpanel;
	public static JPanel wluc1pwmmuxtestselectionindicatorpanel;
	public static JPanel envuc1pwmmuxtestselectionindicatorpanel;
	public static JPanel irisuc1pwmmuxtestselectionindicatorpanel;
	public static JPanel wluc2pwmmuxtestselectionindicatorpanel;
	public static JPanel envuc2pwmmuxtestselectionindicatorpanel;
	public static JPanel irisuc2pwmmuxtestselectionindicatorpanel;
	public static JPanel wlextpwmmuxtestselectionindicatorpanel;
	public static JPanel envextpwmmuxtestselectionindicatorpanel;
	public static JPanel irisextwlpwmmuxtestselectionindicatorpanel;
	public static JPanel irisextenvpwmmuxtestselectionindicatorpanel;
	
	public static JPanel redledzerotestselectionindicatorpanel;
	public static JPanel greenledzerotestselectionindicatorpanel;
	public static JPanel blueledzerotestselectionindicatorpanel;
	public static JPanel envledzerotestselectionindicatorpanel;
	public static JPanel iris1ledzerotestselectionindicatorpanel;
	public static JPanel iris2ledzerotestselectionindicatorpanel;
	
	public static JPanel redledfiftytestselectionindicatorpanel;
	public static JPanel greenledfiftytestselectionindicatorpanel;
	public static JPanel blueledfiftytestselectionindicatorpanel;
	public static JPanel envledfiftytestselectionindicatorpanel;
	public static JPanel iris1ledfiftytestselectionindicatorpanel;
	public static JPanel iris2ledfiftytestselectionindicatorpanel;
	
	public static JPanel redledseventyfivetestselectionindicatorpanel;
	public static JPanel greenledseventyfivetestselectionindicatorpanel;
	public static JPanel blueledseventyfivetestselectionindicatorpanel;
	public static JPanel envledseventyfivetestselectionindicatorpanel;
	public static JPanel iris1ledseventyfivetestselectionindicatorpanel;
	public static JPanel iris2ledseventyfivetestselectionindicatorpanel;
	
	public static JPanel redledhundredtestselectionindicatorpanel;
	public static JPanel greenledhundredtestselectionindicatorpanel;
	public static JPanel blueledhundredtestselectionindicatorpanel;
	public static JPanel envledhundredtestselectionindicatorpanel;
	public static JPanel iris1ledhundredtestselectionindicatorpanel;
	public static JPanel iris2ledundredtestselectionindicatorpanel;
	
	public static JPanel iris1fiberdetecttestselectionindicatorpanel;
	public static JPanel iris2fiberdetecttestselectionindicatorpanel;
	public static JPanel beamsensortestselectionindicatorpanel;
	public static JPanel essttestselectionindicatorpanel;
	public static JPanel assttestselectionindicatorpanel;
	public static JPanel fbpwrenabletestselectionindicatorpanel;
	public static JPanel irispgoodtestselectionindicatorpanel;
	public static JPanel iris1zeropdtestselectionindicatorpanel;
	public static JPanel iris1hundredpdtestselectionindicatorpanel;
	public static JPanel iris2zeropdtestselectionindicatorpanel;
	public static JPanel iris2hundredpdtestselectionindicatorpanel;
	public static JPanel envzeropdtestselectionindicatorpanel;
	public static JPanel envhundredpdtestselectionindicatorpanel;
	public static JPanel colorsensor1testselectionindicatorpanel;
	public static JPanel colorsensor2testselectionindicatorpanel;
	
	public static String RESPONSE_OK		= "{OK,";
	public static String RESPONSE_ERROR		= "{FAIL,";
	//Test Result Indicator Panels
	public static JPanel uc1comtestresultindicatorpanel;
	public static JPanel uc2comtestresultindicatorpanel;
	public static JPanel ipctestresultindicatorpanel;
	public static JPanel uc1pwrtestresultindicatorpanel;
	public static JPanel uc2pwrtestresultindicatorpanel;
	public static JPanel uc1eepromtestresultindicatorpanel;
	public static JPanel uc2eepromtestresultindicatorpanel;
	public static JPanel uc2falshtestresultindicatorpanel;
	public static JPanel uc1camuarttestresultindicatorpanel;
	public static JPanel uc1debuguarttestresultindicatorpanel;
	public static JPanel uc2camuarttestresultindicatorpanel;
	public static JPanel uc2debuguarttestresultindicatorpanel;
	public static JPanel uc2guiuarttestresultindicatorpanel;
	public static JPanel uc2adduarttestresultindicatorpanel;
	public static JPanel lightenginefantestresultindicatorpanel;
	public static JPanel heatsinkfantestresultindicatorpanel;
	public static JPanel irisfantestresultindicatorpanel;
	public static JPanel wluc1pwmmuxtestresultindicatorpanel;
	public static JPanel envuc1pwmmuxtestresultindicatorpanel;
	public static JPanel irisuc1pwmmuxtestresultindicatorpanel;
	public static JPanel wluc2pwmmuxtestresultindicatorpanel;
	public static JPanel envuc2pwmmuxtestresultindicatorpanel;
	public static JPanel irisuc2pwmmuxtestresultindicatorpanel;
	public static JPanel wlextpwmmuxtestresultindicatorpanel;
	public static JPanel envextpwmmuxtestresultindicatorpanel;
	public static JPanel irisextwlpwmmuxtestresultindicatorpanel;
	public static JPanel irisextenvpwmmuxtestresultindicatorpanel;
	
	public static JPanel redledzerotestresultindicatorpanel;
	public static JPanel greenledzerotestresultindicatorpanel;
	public static JPanel blueledzerotestresultindicatorpanel;
	public static JPanel envledzerotestresultindicatorpanel;
	public static JPanel iris1ledzerotestresultindicatorpanel;
	public static JPanel iris2ledzerotestresultindicatorpanel;
	
	public static JPanel redledfiftytestresultindicatorpanel;
	public static JPanel greenledfiftytestresultindicatorpanel;
	public static JPanel blueledfiftytestresultindicatorpanel;
	public static JPanel envledfiftytestresultindicatorpanel;
	public static JPanel iris1ledfiftytestresultindicatorpanel;
	public static JPanel iris2ledfiftytestresultindicatorpanel;
	
	public static JPanel redledseventyfivetestresultindicatorpanel;
	public static JPanel greenledseventyfivetestresultindicatorpanel;
	public static JPanel blueledseventyfivetestresultindicatorpanel;
	public static JPanel envledseventyfivetestresultindicatorpanel;
	public static JPanel iris1ledseventyfivetestresultindicatorpanel;
	public static JPanel iris2ledseventyfivetestresultindicatorpanel;
	
	public static JPanel redledhundredtestresultindicatorpanel;
	public static JPanel greenledhundredtestresultindicatorpanel;
	public static JPanel blueledhundredtestresultindicatorpanel;
	public static JPanel envledhundredtestresultindicatorpanel;
	public static JPanel iris1ledhundredtestresultindicatorpanel;
	public static JPanel iris2ledundredtestresultindicatorpanel;
	
	public static JPanel iris1fiberdetecttestresultindicatorpanel;
	public static JPanel iris2fiberdetecttestresultindicatorpanel;
	public static JPanel beamsensortestresultindicatorpanel;
	public static JPanel essttestresultindicatorpanel;
	public static JPanel assttestresultindicatorpanel;
	public static JPanel fbpwrenabletestresultindicatorpanel;
	public static JPanel irispgoodtestresultindicatorpanel;
	public static JPanel iris1zeropdtestresultindicatorpanel;
	public static JPanel iris1hundredpdtestresultindicatorpanel;
	public static JPanel iris2zeropdtestresultindicatorpanel;
	public static JPanel iris2hundredpdtestresultindicatorpanel;
	public static JPanel envzeropdtestresultindicatorpanel;
	public static JPanel envhundredpdtestresultindicatorpanel;
	public static JPanel colorsensor1testresultindicatorpanel;
	public static JPanel colorsensor2testresultindicatorpanel;
	public JLabel lblBoardNumber;
	public static JLabel lblTestStatus;
	public JLabel lblResult;
	
	
	//Test Label names
	public static JLabel lbluc1comtest;
	public static JLabel lbluc2comtest;
	public static JLabel lblipctest;
	public static JLabel lbluc1pwrtest;
	public static JLabel lbluc2pwrtest;
	public static JLabel lbluc1eepromtest;
	public static JLabel lbluc2eepromtest;
	public static JLabel lbluc2flashtest;
	public static JLabel lbluc1camuarttest;
	public static JLabel lbluc1debuguarttest;
	public static JLabel lbluc2camuarttest;
	public static JLabel lbluc2debuguarttest;
	public static JLabel lbluc2guiuarttest;
	public static JLabel lbluc2adduarttest;
	public static JLabel lbllightenginefantest;
	public static JLabel lblheatsinkfantest;
	public static JLabel lblirisfantest;
	public static JLabel lblfanfailuretest;
	public static JLabel lblwluc1pwmmuxtest;
	public static JLabel lblenvuc1pwmmuxtest;
	public static JLabel lblirisuc1pwmmuxtest;
	public static JLabel lblwluc2pwmmuxtest;
	public static JLabel lblenvuc2pwmmuxtest;
	public static JLabel lblirisuc2pwmmuxtest;
	public static JLabel lblwlextpwmmuxtest;
	public static JLabel lblenvextpwmmuxtest;
	public static JLabel lblirisextwlpwmmuxtest;
	public static JLabel lblirisextenvpwmmuxtest;
	public static JLabel lblredledzerotest;
	public static JLabel lblgreenledzerotest;
	public static JLabel lblblueledzerotest;
	public static JLabel lbllaserledzerotest;
	public static JLabel lbliris1ledzerotest;
	public static JLabel lbliris2ledzerotest;
	public static JLabel lblredledfiftytest;
	public static JLabel lblgreenledfiftytest;
	public static JLabel lblblueledfiftytest;
	public static JLabel lbllaserledfiftytest;
	public static JLabel lbliris1ledfiftytest;
	public static JLabel lbliris2ledfiftytest;
	public static JLabel lblredledseventyfivetest;
	public static JLabel lblgreenledseventyfivetest;
	public static JLabel lblblueledseventyfivetest;
	public static JLabel lbllaserledseventyfivetest;
	public static JLabel lbliris1ledseventyfivetest;
	public static JLabel lbliris2ledseventyfivetest;
	public static JLabel lblredledhundredtest;
	public static JLabel lblgreenledhundredtest;
	public static JLabel lblblueledhundredtest;
	public static JLabel lbllaserledhundredtest;
	public static JLabel lbliris1ledhundredtest;
	public static JLabel lbliris2ledhundredtest;
	public static JLabel lbliris1fiberdetecttest;
	public static JLabel lbliris2fiberdetecttest;
	public static JLabel lblbeamsensortest;
	public static JLabel lblessttest;
	public static JLabel lblassttest;
	public static JLabel lblfrontboardtest;
	public static JLabel lblirispgoodtest;
	public static JLabel lbliris1zeropdtest;
	public static JLabel lbliris1hundredpdtest;
	public static JLabel lbliris2zeropdtest;
	public static JLabel lbliris2hundredpdtest;
	public static JLabel lbllaserzeropdtest;
	public static JLabel lbllaserhundredpdtest;
	public static JLabel lblcolorsensor1test;
	public static JLabel lblcolorsensor2test;
	public static JTextField textField_FirmWareVerNo;
	public static JTextField textField_Firmware_UC1;
	public static JTextField txtLboard;
	
	public static JScrollPane scrollPane;
	
	public static JRadioButton rdbtnClear;
	
	public static long serial_read_byte_timeout = 50;
	
	
	public static JCheckBox chckbxGroupTests;
	public static JCheckBox chckbxLightEngine;
	public static JCheckBox chckbxIrisModule;
	public static JCheckBox chckbxFans;
	public static JCheckBox chckbxExternalCamFeed;
	public static JCheckBox chckbxUartLoopBack;
	public static JCheckBox chckbxOpticFiber;
	public static JCheckBox chckbxEsst;
	public static JCheckBox chckbxAsst;
	public static JCheckBox chckbxColorSensors;
	private JLabel lblTestSelection;
	private JLabel lblTestItem;
	private JLabel lblTestResult;
	
	//HashMap Declarations
	public static HashMap<String, String> btn_name_mapping 				= new HashMap<String, String>();
	public static HashMap<Integer,String> TestItems 					= new HashMap<Integer,String>();
	public static HashMap<String, String> test_name_mappings 			= new HashMap<String, String>();
	public static HashMap<String, String> panel_name_mappings 			= new HashMap<String, String>();
	public static HashMap<String, JPanel> result_indicator_mappings 	= new HashMap<String, JPanel>();
	public static HashMap<String, String> command_name_mappings			= new HashMap<String, String>();
	
	
	private JLabel label_1;
	private JLabel label_2;
	private JLabel label_3;
	private JLabel label_4;
	private JLabel label_5;
	private JLabel label_6;
	
	public static String btnname_mapping_topanel = "";
	
	public static int notify_flag;
	public static int timeoutflag;
	
	public static String Pass = "PASS";
	public static String Fail = "FAIL";
	public static String uc1comtest  = "uC1COMTEST";
	public static String uc2comtest  = "uC2COMTEST";
	public static String ipctest     = "IPCTEST";
	public static String uc1pwrtest  = "uC1PWRTEST";
	public static String uc2pwrtest  = "uC2PWRTEST";
	public static String uc1eepromtest = "uC1EEPROMTEST";
	public static String uc2eepromtest = "uC2EEPROMTEST";
	public static String uc2flashtest  = "uC2FLASHTEST";
	public static String uc1camuarttest = "uC1CAMUARTTEST";
	public static String uc1debuguarttest = "uC1DEBUGUARTTEST";
	public static  String uc2camuarttest   = "uC2CAMUARTTEST";
	public static String uc2debuguarttest = "uC2DEBUGUARTTEST";
	public static String uc2guiuarttest = "uC2GUIUARTTEST";
	public static String uc2adduarttest = "uC2ADDUARTTEST";
	public static String lightenginefantest = "LIGHTENGINEFANTEST";
	public static String heatsinkfantest = "HEATSINKFANTEST";
	public static String irisfantest = "IRISFANTEST";
	public static String fanfailuretest = "FANFAILURETEST";
	public static String wluc1pwmmuxtest = "WLuC1PWMMUXTEST";
	public static String envuc1pwmmuxtest = "ENVuC1PWMMUXTEST";
	public static String irisuc1pwmmuxtest = "IRISuC1PWMMUXTEST";
	public static String wluc2pwmmuxtest = "WLuC2PWMMUXTEST";
	public static String envuc2pwmmuxtest = "ENVuC2PWMMUXTEST";
	public static String irisuc2pwmmuxtest = "IRISuC2PWMMUXTEST";
	public static String wlextpwmmuxtest = "WLEXTPWMMUXTEST";
	public static String envextpwmmuxtest = "ENVEXTPWMMUXTEST";
	public static String irisextwlpwmmuxtest = "IRISEXTWLPWMMUXTEST";
	public static String irisextenvpwmmuxtest = "IRISEXTENVPWMMUXTEST";
	
	public static String redledtestzero = "REDLEDTEST-0%";
	public static String greenledtestzero = "GREENLEDTEST-0%";
	public static String blueledtestzero = "BLUELEDTEST-0%";
	public static String laserledtestzero = "LASERLEDTEST-0%";
	public static String iris1ledtestzero = "IRIS1LEDTEST-0%";
	public static String iris2ledtestzero = "IRIS2LEDTEST-0%";
	
	public static String redledtestfifty = "REDLEDTEST-50%";
	public static String greenledtestfifty = "GREENLEDTEST-50%";
	public static String blueledtestfifty = "BLUELEDTEST-50%";
	public static String laserledtestfifty = "LASERLEDTEST-50%";
	public static String iris1ledtestfifty = "IRIS1LEDTEST-50%";
	public static String iris2ledtestfifty = "IRIS2LEDTEST-50%";
	
	public static String redledtestseventyfive = "REDLEDTEST-75%";
	public static String greenledtestseventyfive = "GREENLEDTEST-75%";
	public static String blueledtestseventyfive = "BLUELEDTEST-75%";
	public static String laserledtestseventyfive = "LASERLEDTEST-75%";
	public static String iris1ledtestseventyfive = "IRIS1LEDTEST-75%";
	public static String iris2ledtestseventyfive = "IRIS2LEDTEST-75%";
	
	public static String redledtesthundred = "REDLEDTEST-100%";
	public static String greenledtesthundred = "GREENLEDTEST-100%";
	public static String blueledtesthundred = "BLUELEDTEST-100%";
	public static String laserledtesthundred = "LASERLEDTEST-100%";
	public static String iris1ledtesthundred = "IRIS1LEDTEST-100%";
	public static String iris2ledtesthundred = "IRIS2LEDTEST-100%";
	
	public static String iris1fiberdetecttest = "IRIS1FIBERDETECTTEST";
	public static String iris2fiberdetecttest = "IRIS2FIBERDETECTTEST";
	public static String beamsensortest = "BEAMSENSORTEST";
	public static String essttest = "ESSTTEST";
	public static String assttest = "ASSTTTEST";
	public static String frontboardtest = "FRONTBOARDPOWERTEST";
	public static String irispgoodontest = "IRISPGOODTESTON";
	public static String irispgoodofftest = "IRISPGOODTESTOFF";
	public static String iris1pdtestzero = "PDTEST-IRIS1-0%";
	public static String iris1pdtesthundred = "PDTEST-IRIS1-100%";
	public static String iris2pdtestzero = "PDTEST-IRIS2-0%";
	public static String iris2pdtesthundred = "PDTEST-IRIS2-100%";
	public static String envpdtestzero = "PDTEST-ENV-0%";
	public static String envpdtesthundred = "PDTEST-ENV-100%";
	public static String colorsensor1test = "COLORSENSOR1TEST";
	public static String colorsensor2test = "COLORSENSOR2TEST";
	public JScrollPane scrollPane_1;
	public JScrollPane scrollPane_2;
	public JScrollPane scrollPane_3;
	public JScrollPane scrollPane_4;
	public static ScheduledFuture<?> future;
	public static ByteArrayOutputStream full_buffer = new ByteArrayOutputStream();
	public static  int numRead;
	public static int total_bytes;
	
	public boolean writeflag;
	/**
	 * @wbp.parser.entryPoint
	 * 
	 */
	public ManufacturingTests() {
		if(serialport !=  null)
		{
			
		}
		else
		{
			JOptionPane.showMessageDialog(frmStrykerManufacturingTests, "Invalid Com Port. The Application Will Now Exit", "Com Port", JOptionPane.ERROR_MESSAGE);
			
			//Stryk_Demo sd = new Stryk_Demo();
			//Stryk_Demo.frmStrykerVer.setVisible(true);
			System.exit(0);
		}
		
		//Create Log Parent Directory
				isDirectoryCreated = Create_Directory(ParentLogDirectoryName);
				if(isDirectoryCreated)
				{
					System.out.println("Log Parent Directory Created Successfully!!");
				}
				else
				{
					System.out.println("Log Parent Directory Alreay Exists!!");
				}
				
				//Create Report Parent Directory
				isDirectoryCreated = Create_Directory(ParentReportDirectoryName);
				if(isDirectoryCreated)
				{
					System.out.println("Report Parent Directory Created Successfully!!");
				}
				else
				{
					System.out.println("Report Parent Directory Alreay Exists!!");
				}
				
				
				DateToday = get_current_date_filename();
				
				LogFileName = ParentLogDirectoryName + DateToday + ".txt";
				
				LogFilePath = ParentLogDirectoryName + File.separator ;
				
				
				//Create DateWise Log File inside the Parent Log Directory ---->  LOGS/LOGS_2017_11_16.txt
				DateWiseLogFileName = LogFilePath + LogFileName;
				try {
					FileHandler logfilehandler = new FileHandler();
					logs_logger = Create_Handler(logs_logger, DateWiseLogFileName,logfilehandler);
				} catch (SecurityException | IOException e3) {
					// TODO Auto-generated catch block
					e3.printStackTrace();
				}
				
				
				System.out.println(DateWiseLogFileName);
				File logfile = new File(DateWiseLogFileName);
				try 
				{
					if(logfile.createNewFile())
					{
						System.out.println("Created File : "+ "\"" +  LogFileName  + "\""+ " under the path : " + LogFilePath);
					}
					else
					{
						System.out.println("File : " + LogFileName + "Already exists in the path : " + LogFilePath);
					}
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
				//Create DateWise Report Parent Directory in the JavaApp's Current Directory --->  REPORTS/REPORTS_2017_11_16
				DateWiseReportDirectoryName =  ParentReportDirectoryName + File.separator + ParentReportDirectoryName + DateToday;
				
				isDirectoryCreated = Create_Directory(DateWiseReportDirectoryName);
				if(isDirectoryCreated)
				{
					System.out.println("Report Directory Created Successfully!!");
				}
				else
				{
					System.out.println("Report Directory Alreay Exists!!");
				}
				
		
		//HashMap for returning Button Names, based on test names
		btn_name_mapping.put(uc1comtest, "btnuc1comtestselector");
		btn_name_mapping.put(uc2comtest, "btnuc2comtestselector");
		btn_name_mapping.put(ipctest, "btnipctestselector");
		btn_name_mapping.put(uc1pwrtest, "btnuc1pwrtestselector");
		btn_name_mapping.put(uc2pwrtest, "btnuc2pwrtestselector");
		btn_name_mapping.put(uc1eepromtest, "btnuc1eepromtestselector");
		btn_name_mapping.put(uc2eepromtest, "btnuc2eepromtestselector");
		btn_name_mapping.put(uc2flashtest, "btnuc2flashtestselector");
		btn_name_mapping.put(uc1camuarttest, "btnuc1camuarttestselector");
		btn_name_mapping.put(uc1debuguarttest, "btnuc1debuguarttestselector");
		btn_name_mapping.put(uc2camuarttest, "btnuc2camuarttestselector");
		btn_name_mapping.put(uc2debuguarttest, "btnuc2debuguarttestselector");
		btn_name_mapping.put(uc2guiuarttest, "btnuc2guiuarttestselector");
		btn_name_mapping.put(uc2adduarttest, "btnuc2adduarttestselector");
		
		btn_name_mapping.put(lightenginefantest, "btnlightenginefantestselector");
		btn_name_mapping.put(heatsinkfantest, "btnheatsinkfantestselector");
		btn_name_mapping.put(irisfantest, "btnirisfantestselector");
		
		btn_name_mapping.put(fanfailuretest, "btnfanfailuretestselector");
		
		btn_name_mapping.put(wluc1pwmmuxtest, "btnwluc1pwmmuxtestselector");
		btn_name_mapping.put(envuc1pwmmuxtest, "btnenvuc1pwmmuxtestselector");
		btn_name_mapping.put(irisuc1pwmmuxtest, "btnirisuc1pwmmuxtestselector");
		btn_name_mapping.put(wluc2pwmmuxtest, "btnwluc2pwmmuxtestselector");
		btn_name_mapping.put(envuc2pwmmuxtest, "btnenvuc2pwmmuxtestselector");
		btn_name_mapping.put(irisuc2pwmmuxtest, "btnirisuc2pwmmuxtestselector");
		btn_name_mapping.put(wlextpwmmuxtest, "btnwlextpwmmuxtestselector");
		btn_name_mapping.put(envextpwmmuxtest,"btnenvextpwmmuxtestselector");
		btn_name_mapping.put(irisextwlpwmmuxtest, "btnirisextwlpwmmuxtestselector");
		btn_name_mapping.put(irisextenvpwmmuxtest, "btnirisextenvpwmmuxtestselector");
		
		btn_name_mapping.put(redledtestzero, "btnredledzerotestselector");
		btn_name_mapping.put(greenledtestzero,"btngreenledzerotestselector");
		btn_name_mapping.put(blueledtestzero, "btnblueledzerotestselector");
		btn_name_mapping.put(laserledtestzero, "btnlaserledzerotestselector");
		btn_name_mapping.put(iris1ledtestzero, "btniris1ledzerotestselector");
		btn_name_mapping.put(iris2ledtestzero, "btniris2ledzerotestselector");
		
		btn_name_mapping.put(redledtestfifty, "btnredledfiftytestselector");
		btn_name_mapping.put(greenledtestfifty, "btngreenledfiftytestselector");
		btn_name_mapping.put(blueledtestfifty, "btnblueledfiftytestselector");
		btn_name_mapping.put(laserledtestfifty, "btnlaserledfiftytestselector");
		btn_name_mapping.put(iris1ledtestfifty, "btniris1ledfiftytestselector");
		btn_name_mapping.put(iris2ledtestfifty, "btniris2ledfiftytestselector");
		
		btn_name_mapping.put(redledtestseventyfive, "btnredledseventyfivetestselector");
		btn_name_mapping.put(greenledtestseventyfive, "btngreenledseventyfivetestselector");
		btn_name_mapping.put(blueledtestseventyfive, "btnblueledseventyfivetestselector");
		btn_name_mapping.put(laserledtestseventyfive, "btnlaserledseventyfivetestselector");
		btn_name_mapping.put(iris1ledtestseventyfive, "btniris1ledseventyfivetestselector");
		btn_name_mapping.put(iris2ledtestseventyfive, "btniris2ledseventyfivetestselector");
		
		btn_name_mapping.put(redledtesthundred, "btnredledhundredtestselector");
		btn_name_mapping.put(greenledtesthundred, "btngreenledhundredtestselector");
		btn_name_mapping.put(blueledtesthundred, "btnblueledhundredtestselector");
		btn_name_mapping.put(laserledtesthundred, "btnlaserledhundredtestselector");
		btn_name_mapping.put(iris1ledtesthundred, "btniris1ledhundredtestselector");
		btn_name_mapping.put(iris2ledtesthundred, "btniris2ledhundredtestselector");
		
		btn_name_mapping.put(iris1fiberdetecttest, "btniris1fiberdetecton_offtestselector");
		btn_name_mapping.put(iris2fiberdetecttest, "btniris2fiberdetecton_offtestselector");
		
		btn_name_mapping.put(beamsensortest, "btnbeamsensoron_offtestselector");
		btn_name_mapping.put(essttest, "btnesston_offtestselector");
		btn_name_mapping.put(assttest, "btnasston_offtestselector");
		btn_name_mapping.put(frontboardtest, "btnfbpwrenableon_offtestselector");
		btn_name_mapping.put(irispgoodontest, "btnirispgoodon_testselector");
		btn_name_mapping.put(irispgoodofftest,"btnirispgoodoff_testselector");
		btn_name_mapping.put(iris1pdtestzero, "btniris1zeropdtestselector");
		btn_name_mapping.put(iris1pdtesthundred, "btniris1hundredpdtestselector");
		btn_name_mapping.put(iris2pdtestzero, "btniris2zeropdtestselector");
		btn_name_mapping.put(iris2pdtesthundred, "btniris2hundredpdtestselector");
		btn_name_mapping.put(envpdtestzero, "btnenvzeropdtestselector");
		btn_name_mapping.put(envpdtesthundred, "btnenvhundredpdtestselector");
		btn_name_mapping.put(colorsensor1test, "btncolorsensor1testselector");
		btn_name_mapping.put(colorsensor2test,"btncolorsensor2testselector");
		
		//command name mapping
		command_name_mappings.put("btnuc1comtestselector",uc1comtest);
		command_name_mappings.put( "btnuc2comtestselector",uc2comtest);
		command_name_mappings.put( "btnipctestselector",ipctest);
		command_name_mappings.put( "btnuc1pwrtestselector",uc1pwrtest);
		command_name_mappings.put("btnuc2pwrtestselector",uc2pwrtest);
		command_name_mappings.put("btnuc1eepromtestselector",uc1eepromtest);
		command_name_mappings.put("btnuc2eepromtestselector",uc2eepromtest);
		command_name_mappings.put("btnuc2flashtestselector",uc2flashtest);
		command_name_mappings.put("btnuc1camuarttestselector",uc1camuarttest);
		command_name_mappings.put("btnuc1debuguarttestselector",uc1debuguarttest);
		command_name_mappings.put("btnuc2camuarttestselector",uc2camuarttest);
		command_name_mappings.put("btnuc2debuguarttestselector",uc2debuguarttest);
		command_name_mappings.put("btnuc2guiuarttestselector",uc2guiuarttest );
		command_name_mappings.put("btnuc2adduarttestselector",uc2adduarttest);
		
		command_name_mappings.put("btnlightenginefantestselector",lightenginefantest);
		command_name_mappings.put("btnheatsinkfantestselector",heatsinkfantest );
		command_name_mappings.put( "btnirisfantestselector",irisfantest);
		
		command_name_mappings.put("btnfanfailuretestselector",fanfailuretest);
		
		command_name_mappings.put("btnwluc1pwmmuxtestselector",wluc1pwmmuxtest);
		command_name_mappings.put("btnenvuc1pwmmuxtestselector",envuc1pwmmuxtest);
		command_name_mappings.put("btnirisuc1pwmmuxtestselector",irisuc1pwmmuxtest);
		command_name_mappings.put("btnwluc2pwmmuxtestselector",wluc2pwmmuxtest);
		command_name_mappings.put("btnenvuc2pwmmuxtestselector",envuc2pwmmuxtest);
		command_name_mappings.put("btnirisuc2pwmmuxtestselector",irisuc2pwmmuxtest);
		command_name_mappings.put("btnwlextpwmmuxtestselector",wlextpwmmuxtest);
		command_name_mappings.put("btnenvextpwmmuxtestselector",envextpwmmuxtest);
		command_name_mappings.put("btnirisextwlpwmmuxtestselector",irisextwlpwmmuxtest);
		command_name_mappings.put("btnirisextenvpwmmuxtestselector",irisextenvpwmmuxtest);
		
		command_name_mappings.put("btnredledzerotestselector",redledtestzero);
		command_name_mappings.put("btngreenledzerotestselector",greenledtestzero);
		command_name_mappings.put("btnblueledzerotestselector",blueledtestzero);
		command_name_mappings.put("btnlaserledzerotestselector",laserledtestzero);
		command_name_mappings.put("btniris1ledzerotestselector",iris1ledtestzero);
		command_name_mappings.put("btniris2ledzerotestselector",iris2ledtestzero);
		
		command_name_mappings.put("btnredledfiftytestselector",redledtestfifty);
		command_name_mappings.put("btngreenledfiftytestselector",greenledtestfifty);
		command_name_mappings.put("btnblueledfiftytestselector",blueledtestfifty);
		command_name_mappings.put("btnlaserledfiftytestselector",laserledtestfifty);
		command_name_mappings.put("btniris1ledfiftytestselector",iris1ledtestfifty);
		command_name_mappings.put("btniris2ledfiftytestselector",iris2ledtestfifty);
		
		command_name_mappings.put("btnredledseventyfivetestselector",redledtestseventyfive);
		command_name_mappings.put("btngreenledseventyfivetestselector",greenledtestseventyfive);
		command_name_mappings.put("btnblueledseventyfivetestselector",blueledtestseventyfive);
		command_name_mappings.put("btnlaserledseventyfivetestselector",laserledtestseventyfive);
		command_name_mappings.put("btniris1ledseventyfivetestselector",iris1ledtestseventyfive);
		command_name_mappings.put("btniris2ledseventyfivetestselector",iris2ledtestseventyfive);
		
		command_name_mappings.put("btnredledhundredtestselector",redledtesthundred);
		command_name_mappings.put("btngreenledhundredtestselector",greenledtesthundred);
		command_name_mappings.put("btnblueledhundredtestselector",blueledtesthundred);
		command_name_mappings.put("btnlaserledhundredtestselector",laserledtesthundred);
		command_name_mappings.put("btniris1ledhundredtestselector",iris1ledtesthundred);
		command_name_mappings.put("btniris2ledhundredtestselector",iris2ledtesthundred);
		
		command_name_mappings.put("btniris1fiberdetecton_offtestselector",iris1fiberdetecttest);
		command_name_mappings.put("btniris2fiberdetecton_offtestselector",iris2fiberdetecttest);
		
		command_name_mappings.put("btnbeamsensoron_offtestselector",beamsensortest);
		command_name_mappings.put("btnesston_offtestselector",essttest);
		command_name_mappings.put("btnasston_offtestselector",assttest);
		command_name_mappings.put("btnfbpwrenableon_offtestselector",frontboardtest);
		command_name_mappings.put("btnirispgoodon_testselector",irispgoodontest);
		command_name_mappings.put("btnirispgoodoff_testselector",irispgoodofftest);
		command_name_mappings.put("btniris1zeropdtestselector",iris1pdtestzero);
		command_name_mappings.put("btniris1hundredpdtestselector",iris1pdtesthundred);
		command_name_mappings.put("btniris2zeropdtestselector",iris2pdtestzero);
		command_name_mappings.put("btniris2hundredpdtestselector",iris2pdtesthundred);
		command_name_mappings.put("btnenvzeropdtestselector",envpdtestzero);
		command_name_mappings.put("btnenvhundredpdtestselector",envpdtesthundred);
		command_name_mappings.put("btncolorsensor1testselector",colorsensor1test);
		command_name_mappings.put("btncolorsensor2testselector",colorsensor2test);
		//Label Mappings for Test Items
		test_name_mappings.put(uc1comtest, "lbluc1comtest");
		test_name_mappings.put(uc2comtest, "lbluc2comtest");
		test_name_mappings.put(ipctest, "lblipctest");
		test_name_mappings.put(uc1pwrtest, "lbluc1pwrtest");
		test_name_mappings.put(uc2pwrtest, "lbluc2pwrtest");
		test_name_mappings.put(uc1eepromtest, "lbluc1eepromtest");
		test_name_mappings.put(uc2eepromtest, "lbluc2eepromtest");
		test_name_mappings.put(uc2flashtest, "lbluc2flashtest");
		test_name_mappings.put(uc1camuarttest, "lbluc1camuarttest");
		test_name_mappings.put(uc1debuguarttest, "lbluc1debuguarttest");
		test_name_mappings.put(uc2camuarttest, "lbluc2camuarttest");
		test_name_mappings.put(uc2debuguarttest, "lbluc2debuguarttest");
		test_name_mappings.put(uc2guiuarttest, "lbluc2guiuarttest");
		test_name_mappings.put(uc2adduarttest, "lbluc2adduarttest");
		
		test_name_mappings.put(lightenginefantest, "lbllightenginefantest");
		test_name_mappings.put(heatsinkfantest, "lblheatsinkfantest");
		test_name_mappings.put(irisfantest, "lblirisfantest");
		
		test_name_mappings.put(fanfailuretest, "lblfanfailuretest");
		
		test_name_mappings.put(wluc1pwmmuxtest, "lblwluc1pwmmuxtest");
		test_name_mappings.put(envuc1pwmmuxtest, "lblenvuc1pwmmuxtest");
		test_name_mappings.put(irisuc1pwmmuxtest, "lblirisuc1pwmmuxtest");
		test_name_mappings.put(wluc2pwmmuxtest, "lblwluc2pwmmuxtest");
		test_name_mappings.put(envuc2pwmmuxtest, "lblenvuc2pwmmuxtest");
		test_name_mappings.put(irisuc2pwmmuxtest, "lblirisuc2pwmmuxtest");
		test_name_mappings.put(wlextpwmmuxtest, "lblwlextpwmmuxtest");
		test_name_mappings.put(envextpwmmuxtest,"lblenvextpwmmuxtest");
		test_name_mappings.put(irisextwlpwmmuxtest, "lblirisextwlpwmmuxtest");
		test_name_mappings.put(irisextenvpwmmuxtest, "lblirisextenvpwmmuxtest");
		
		test_name_mappings.put(redledtestzero, "lblredledzerotest");
		test_name_mappings.put(greenledtestzero, "lblgreenledzerotest");
		test_name_mappings.put(blueledtestzero, "lblblueledzerotest");
		test_name_mappings.put(laserledtestzero, "lbllaserledzerotest");
		test_name_mappings.put(iris1ledtestzero, "lbliris1ledzerotest");
		test_name_mappings.put(iris2ledtestzero, "lbliris2ledzerotest");
		
		test_name_mappings.put(redledtestfifty, "lblredledfiftytest");
		test_name_mappings.put(greenledtestfifty, "lblgreenledfiftytest");
		test_name_mappings.put(blueledtestfifty, "lblblueledfiftytest");
		test_name_mappings.put(laserledtestfifty, "lbllaserledfiftytest");
		test_name_mappings.put(iris1ledtestfifty, "lbliris1ledfiftytest");
		test_name_mappings.put(iris2ledtestfifty, "lbliris2ledfiftytest");
		
		test_name_mappings.put(redledtestseventyfive, "lblredledseventyfivetest");
		test_name_mappings.put(greenledtestseventyfive, "lblgreenledseventyfivetest");
		test_name_mappings.put(blueledtestseventyfive, "lblblueledseventyfivetest");
		test_name_mappings.put(laserledtestseventyfive, "lbllaserledseventyfivetest");
		test_name_mappings.put(iris1ledtestseventyfive, "lbliris1ledseventyfivetest");
		test_name_mappings.put(iris2ledtestseventyfive, "lbliris2ledseventyfivetest");
		
		test_name_mappings.put(redledtesthundred, "lblredledhundredtest");
		test_name_mappings.put(greenledtesthundred, "lblgreenledhundredtest");
		test_name_mappings.put(blueledtesthundred, "lblblueledhundredtest");
		test_name_mappings.put(laserledtesthundred, "lbllaserledhundredtest");
		test_name_mappings.put(iris1ledtesthundred, "lbliris1ledhundredtest");
		test_name_mappings.put(iris2ledtesthundred, "lbliris2ledhundredtest");
		
		test_name_mappings.put(iris1fiberdetecttest, "lbliris1fiberdetecttest");
		test_name_mappings.put(iris2fiberdetecttest, "lbliris2fiberdetecttest");
		
		test_name_mappings.put(beamsensortest, "lblbeamsensortest");
		test_name_mappings.put(essttest, "lblessttest");
		test_name_mappings.put(assttest, "lblassttest");
		test_name_mappings.put(frontboardtest, "lblfrontboardtest");
		test_name_mappings.put(irispgoodontest, "lblirispgoodontest");
		test_name_mappings.put(irispgoodofftest, "lblirispgoodofftest");
		test_name_mappings.put(iris1pdtestzero, "lbliris1zeropdtest");
		test_name_mappings.put(iris1pdtesthundred, "lbliris1hundredpdtest");
		test_name_mappings.put(iris2pdtestzero, "lbliris2zeropdtest");
		test_name_mappings.put(iris2pdtesthundred, "lbliris2hundredpdtest");
		test_name_mappings.put(envpdtestzero, "lbllaserzeropdtest");
		test_name_mappings.put(envpdtesthundred, "lbllaserhundredpdtest");
		test_name_mappings.put(colorsensor1test, "lblcolorsensor1test");
		test_name_mappings.put(colorsensor2test,"lblcolorsensor2test");
		
		
		//Label Name Mappings
		panel_name_mappings.put(uc1comtest, "paneluc1comtest");
		panel_name_mappings.put(uc2comtest, "paneluc2comtest");
		panel_name_mappings.put(ipctest, "panelipctest");
		panel_name_mappings.put(uc1pwrtest, "paneluc1pwrtest");
		panel_name_mappings.put(uc2pwrtest, "paneluc2pwrtest");
		panel_name_mappings.put(uc1eepromtest, "paneluc1eepromtest");
		panel_name_mappings.put(uc2eepromtest, "paneluc2eepromtest");
		panel_name_mappings.put(uc2flashtest, "paneluc2flashtest");
		panel_name_mappings.put(uc1camuarttest, "paneluc1camuarttest");
		panel_name_mappings.put(uc1debuguarttest, "paneluc1debuguarttest");
		panel_name_mappings.put(uc2camuarttest, "paneluc2camuarttest");
		panel_name_mappings.put(uc2debuguarttest, "paneluc2debuguarttest");
		panel_name_mappings.put(uc2guiuarttest, "paneluc2guiuarttest");
		panel_name_mappings.put(uc2adduarttest, "paneluc2adduarttest");
		
		panel_name_mappings.put(lightenginefantest, "panellightenginefantest");
		panel_name_mappings.put(heatsinkfantest, "panelheatsinkfantest");
		panel_name_mappings.put(irisfantest, "panelirisfantest");
		
		panel_name_mappings.put(fanfailuretest, "panelfanfailuretest");
		
		panel_name_mappings.put(wluc1pwmmuxtest, "panelwluc1pwmmuxtest");
		panel_name_mappings.put(envuc1pwmmuxtest, "panelenvuc1pwmmuxtest");
		panel_name_mappings.put(irisuc1pwmmuxtest, "panelirisuc1pwmmuxtest");
		panel_name_mappings.put(wluc2pwmmuxtest, "panelwluc2pwmmuxtest");
		panel_name_mappings.put(envuc2pwmmuxtest, "panelenvuc2pwmmuxtest");
		panel_name_mappings.put(irisuc2pwmmuxtest, "panelirisuc2pwmmuxtest");
		panel_name_mappings.put(wlextpwmmuxtest, "panelwlextpwmmuxtest");
		panel_name_mappings.put(envextpwmmuxtest,"panelenvextpwmmuxtest");
		panel_name_mappings.put(irisextwlpwmmuxtest, "panelirisextwlpwmmuxtest");
		panel_name_mappings.put(irisextenvpwmmuxtest, "panelirisextenvpwmmuxtest");
		
		panel_name_mappings.put(redledtestzero, "panelredledzerotest");
		panel_name_mappings.put(greenledtestzero, "panelgreenledzerotest");
		panel_name_mappings.put(blueledtestzero, "panelblueledzerotest");
		panel_name_mappings.put(laserledtestzero, "panellaserledzerotest");
		panel_name_mappings.put(iris1ledtestzero, "paneliris1ledzerotest");
		panel_name_mappings.put(iris2ledtestzero, "paneliris2ledzerotest");
		
		panel_name_mappings.put(redledtestfifty, "panelredledfiftytest");
		panel_name_mappings.put(greenledtestfifty, "panelgreenledfiftytest");
		panel_name_mappings.put(blueledtestfifty, "panelblueledfiftytest");
		panel_name_mappings.put(laserledtestfifty, "panellaserledfiftytest");
		panel_name_mappings.put(iris1ledtestfifty, "paneliris1ledfiftytest");
		panel_name_mappings.put(iris2ledtestfifty, "paneliris2ledfiftytest");
		
		panel_name_mappings.put(redledtestseventyfive, "panelredledseventyfivetest");
		panel_name_mappings.put(greenledtestseventyfive, "panelgreenledseventyfivetest");
		panel_name_mappings.put(blueledtestseventyfive, "panelblueledseventyfivetest");
		panel_name_mappings.put(laserledtestseventyfive, "panellaserledseventyfivetest");
		panel_name_mappings.put(iris1ledtestseventyfive, "paneliris1ledseventyfivetest");
		panel_name_mappings.put(iris2ledtestseventyfive, "paneliris2ledseventyfivetest");
		
		panel_name_mappings.put(redledtesthundred, "panelredledhundredtest");
		panel_name_mappings.put(greenledtesthundred, "panelgreenledhundredtest");
		panel_name_mappings.put(blueledtesthundred, "panelblueledhundredtest");
		panel_name_mappings.put(laserledtesthundred, "panellaserledhundredtest");
		panel_name_mappings.put(iris1ledtesthundred, "paneliris1ledhundredtest");
		panel_name_mappings.put(iris2ledtesthundred, "paneliris2ledhundredtest");
		
		panel_name_mappings.put(iris1fiberdetecttest, "paneliris1fiberdetecttest");
		panel_name_mappings.put(iris2fiberdetecttest, "paneliris2fiberdetecttest");
		
		panel_name_mappings.put(beamsensortest, "panelbeamsensortest");
		panel_name_mappings.put(essttest, "panelessttest");
		panel_name_mappings.put(assttest, "panelassttest");
		panel_name_mappings.put(frontboardtest, "panelfrontboardtest");
		panel_name_mappings.put(irispgoodontest, "panelirispgoodontest");
		panel_name_mappings.put(irispgoodofftest, "panelirispgoodofftest");
		panel_name_mappings.put(iris1pdtestzero, "paneliris1zeropdtest");
		panel_name_mappings.put(iris1pdtesthundred, "paneliris1hundredpdtest");
		panel_name_mappings.put(iris2pdtestzero, "paneliris2zeropdtest");
		panel_name_mappings.put(iris2pdtesthundred, "paneliris1hundredpdtest");
		panel_name_mappings.put(envpdtestzero, "paneliris1hundredpdtest");
		panel_name_mappings.put(envpdtesthundred, "paneliris1hundredpdtest");
		panel_name_mappings.put(colorsensor1test, "panelcolorsensor1test");
		panel_name_mappings.put(colorsensor2test,"panelcolorsensor1test");
	
		//Test Entry HashMap. The tests below are only shown in the UI and Run
		
		TestItems.put(1,uc1comtest);  
		TestItems.put(2,uc2comtest);
		TestItems.put(3,ipctest);  
		TestItems.put(4,uc1pwrtest);
		TestItems.put(5,uc2pwrtest);  
		TestItems.put(6,uc1eepromtest);
		TestItems.put(7,uc2eepromtest);  
		TestItems.put(8,uc2flashtest);
		TestItems.put(9,uc1camuarttest);  
		TestItems.put(10,uc1debuguarttest);
		TestItems.put(11,uc2camuarttest);  
		TestItems.put(12,uc2debuguarttest);
		TestItems.put(13,uc2guiuarttest);  
		TestItems.put(14,uc2adduarttest);
		TestItems.put(15,lightenginefantest);
		TestItems.put(16,heatsinkfantest);
		TestItems.put(17,irisfantest);  
		
		TestItems.put(18,wluc1pwmmuxtest);
		TestItems.put(19,envuc1pwmmuxtest);  
		TestItems.put(20,irisuc1pwmmuxtest);
		TestItems.put(21, wluc2pwmmuxtest);
		TestItems.put(22, envuc2pwmmuxtest);
		TestItems.put(23, irisuc2pwmmuxtest);
		TestItems.put(24, wlextpwmmuxtest);
		TestItems.put(25, envextpwmmuxtest);
		TestItems.put(26, irisextwlpwmmuxtest);
		TestItems.put(27, irisextenvpwmmuxtest);
	
		TestItems.put(28, redledtestzero);
		TestItems.put(29, greenledtestzero);
		TestItems.put(30, blueledtestzero);
		TestItems.put(31, laserledtestzero);
		TestItems.put(32, iris1ledtestzero);
		TestItems.put(33, iris2ledtestzero);
	
		TestItems.put(34, redledtestfifty);
		TestItems.put(35, greenledtestfifty);
		TestItems.put(36, blueledtestfifty);
		TestItems.put(37, laserledtestfifty);
		TestItems.put(38, iris1ledtestfifty);
		TestItems.put(39, iris2ledtestfifty);
		
		TestItems.put(40, redledtestseventyfive);
		TestItems.put(41, greenledtestseventyfive);
		TestItems.put(42, blueledtestseventyfive);
		TestItems.put(43, laserledtestseventyfive);
		TestItems.put(44, iris1ledtestseventyfive);
		TestItems.put(45, iris2ledtestseventyfive);
		
		TestItems.put(46, redledtesthundred);
		TestItems.put(47, greenledtesthundred);
		TestItems.put(48, blueledtesthundred);
		TestItems.put(49, laserledtesthundred);
		TestItems.put(50, iris1ledtesthundred);
		TestItems.put(51, iris2ledtesthundred);
		TestItems.put(52, iris1fiberdetecttest);
		TestItems.put(53, iris2fiberdetecttest);
		TestItems.put(54, beamsensortest);
		TestItems.put(55, essttest);
		TestItems.put(56, assttest);
		TestItems.put(57, frontboardtest);
		TestItems.put(58, irispgoodontest);
		TestItems.put(59, irispgoodofftest);
		TestItems.put(60, fanfailuretest);
		TestItems.put(61, iris1pdtestzero);
		TestItems.put(62, iris1pdtesthundred);
		TestItems.put(63, iris2pdtestzero);
		TestItems.put(64, iris2pdtesthundred);
		TestItems.put(65, envpdtestzero);
		TestItems.put(66, envpdtesthundred);
		TestItems.put(67, colorsensor1test);
		TestItems.put(68, colorsensor2test);
	
		//for(Entry<Integer, String> m:TestItems.entrySet())
		//{  
		//		System.out.println(m.getKey()+" "+m.getValue());  
		//}  
		model = (DefaultTableModel) toTableModel(TestItems);
		String res = "";
		String res1 = "";
		// TODO Auto-generated constructor stub
		frmStrykerManufacturingTests = new JFrame();
		frmStrykerManufacturingTests.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try
				{
					MFTest_Flag = true;
					serialport.openPort();
					serialport.removeDataListener();
					serialport.writeBytes("{MTESTEXIT}".getBytes(), "{MTESTEXIT}".length());
					
				}catch(Exception ex )
				{
					ex.printStackTrace();
				}
			}
		});
		frmStrykerManufacturingTests.setTitle("Manufacturing Test");
		frmStrykerManufacturingTests.getContentPane().setLayout(null);
		frmStrykerManufacturingTests.setAlwaysOnTop(true);
		frmStrykerManufacturingTests.setResizable(false);
		frmStrykerManufacturingTests.setForeground(UIManager.getColor("InternalFrame.borderColor"));
		frmStrykerManufacturingTests.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		frmStrykerManufacturingTests.getContentPane().setBackground(SystemColor.textHighlightText);
		//frmStrykerManufacturingTests.getContentPane().setBackground(Color.decode("#1baaa6"));
		frmStrykerManufacturingTests.getContentPane().setForeground(SystemColor.textHighlight);
		//frmStrykerManufacturingTests.getContentPane().setBackground(SystemColor.textHighlightText);
		
		frmStrykerManufacturingTests.setBounds(10, 10, 1293, 632);
		frmStrykerManufacturingTests.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmStrykerManufacturingTests.setVisible(true);
		
		
		board_number_panel = new JPanel();
		board_number_panel.setBackground(UIManager.getColor("InternalFrame.borderHighlight"));
		board_number_panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		board_number_panel.setBounds(15, 5, 223, 38);
		frmStrykerManufacturingTests.getContentPane().add(board_number_panel);
		board_number_panel.setLayout(null);
		
		lblBoardNumber = new JLabel("BOARD NO.");
		lblBoardNumber.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblBoardNumber.setBounds(8, 10, 78, 14);
		board_number_panel.add(lblBoardNumber);
		
		txtLboard = new JTextField();
		txtLboard.setText("L11_BOARD_0000");
		txtLboard.setBounds(127, 5, 86, 23);
		board_number_panel.add(txtLboard);
		txtLboard.setColumns(10);
		
		this.firmware_version_number_panel = new JPanel();
		firmware_version_number_panel.setBackground(UIManager.getColor("InternalFrame.borderHighlight"));
		firmware_version_number_panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		firmware_version_number_panel.setBounds(15, 50, 223, 38);
		frmStrykerManufacturingTests.getContentPane().add(firmware_version_number_panel);
		firmware_version_number_panel.setLayout(null);
		
		textField_FirmWareVerNo = new JTextField();
		textField_FirmWareVerNo.setEditable(false);
		textField_FirmWareVerNo.setBackground(SystemColor.textHighlightText);
		textField_FirmWareVerNo.setBounds(140, 7, 65, 23);
		firmware_version_number_panel.add(textField_FirmWareVerNo);
		textField_FirmWareVerNo.setColumns(10);
		
		textField_Firmware_UC1 = new JTextField();
		textField_Firmware_UC1.setEditable(false);
		textField_Firmware_UC1.setBounds(37, 7, 65, 23);
		textField_Firmware_UC1.setBackground(SystemColor.textHighlightText);
		firmware_version_number_panel.add(textField_Firmware_UC1);
		textField_Firmware_UC1.setColumns(10);
		
		JLabel lblUc = new JLabel("uC1");
		lblUc.setBounds(12, 11, 35, 15);
		firmware_version_number_panel.add(lblUc);
		
		JLabel lblUc_1 = new JLabel("uC2");
		lblUc_1.setBounds(112, 11, 30, 15);
		firmware_version_number_panel.add(lblUc_1);
		
		this.textArea = new JTextArea();
		DefaultCaret caret = (DefaultCaret)textArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		textArea.setCaretPosition(textArea.getDocument().getLength());
		caret.setUpdatePolicy(DefaultCaret.OUT_BOTTOM);
		textArea.setSize(new Dimension(1000,10000));//Way the textArea was handled has been changed here
		
		//textArea.setPreferredSize(new Dimension(1000,10000));
		logspanel = new JPanel();
		logspanel.setLayout(null);
		logspanel.setBorder(new TitledBorder(new TitledBorder(null, "LOGS", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)), "", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(51, 51, 51)));
		logspanel.setBackground((Color) null);
		logspanel.setBounds(914, 88, 358, 507);
		logspanel.setPreferredSize(new Dimension(1000,1000));
		frmStrykerManufacturingTests.getContentPane().add(logspanel);
		JScrollPane scrollPane = new JScrollPane(textArea,// Create a ScrollPane and Add it to the TextArea
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		//
		scrollPane.setBounds(15, 25, 330, 471);
		logspanel.add(scrollPane);		
		//scrollPane.setRowHeaderView(textArea);
		scrollPane.setViewportView(textArea);
		textArea.setEditable(false);
		
		
		
		
		rdbtnClear = new JRadioButton("Clear Logs");
		rdbtnClear.addItemListener(new ItemListener() {
			private Timer timer;

			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					textArea.setText("");
					
					rdbtnClear.setEnabled(false);
					

						ActionListener taskPerformer = new ActionListener() {
							public void actionPerformed(ActionEvent evt) {
				// ...Perform a task...
							rdbtnClear.setSelected(false);
							rdbtnClear.setEnabled(true);

							timer.stop();
						}
					};
					this.timer = new Timer(100, taskPerformer);
					timer.setRepeats(false);
					timer.start();

				}
			}
		});
		rdbtnClear.setBackground(SystemColor.textHighlightText);
		rdbtnClear.setBounds(255, 7, 83, 23);
		logspanel.add(rdbtnClear);
		
		MainTestpanel = new JPanel();
		MainTestpanel.setBackground(SystemColor.textHighlightText);
		MainTestpanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		MainTestpanel.setBounds(10, 107, 905, 510);
		MainTestpanel.setPreferredSize(new Dimension(1170,500)); //This is what helped to make the scroll bars appear.
		
		MainTestpanel.setAutoscrolls(true);
		//frmStrykerManufacturingTests.getContentPane().add(MainTestpanel);
		//MainTestpanel.setLayout(null);
		
		
		
		
		
		SubTestPanel_1 = new JPanel();
		SubTestPanel_1.setForeground(SystemColor.inactiveCaption);
		SubTestPanel_1.setBackground(SystemColor.textHighlightText);
		SubTestPanel_1.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		SubTestPanel_1.setBounds(10, 10, 275, 452);
		SubTestPanel_1.setVisible(true);
		//scrollPane_4.add(SubTestPanel_1);
		//scrollPane_4.getViewport().setView(SubTestPanel_1);
		SubTestPanel_1.setLayout(null);
		subpanellist.add(SubTestPanel_1);
		
		lblTestSelection = new JLabel("SELECTION");
		lblTestSelection.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		lblTestSelection.setBounds(21, 10, 46, 14);
		SubTestPanel_1.add(lblTestSelection);
		
		lblTestItem = new JLabel("TEST ITEM");
		lblTestItem.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		lblTestItem.setBounds(109, 10, 74, 14);
		SubTestPanel_1.add(lblTestItem);
		
		lblTestResult = new JLabel("RESULT");
		lblTestResult.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		lblTestResult.setBounds(219, 10, 30, 14);
		SubTestPanel_1.add(lblTestResult);
		
		//Pane-1/Button-20
		SubTestPanel_2 = new JPanel();
		SubTestPanel_2.setBackground(SystemColor.textHighlightText);
		SubTestPanel_2.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		SubTestPanel_2.setBounds(295, 10, 275, 452);
		//scrollPane_4.add(SubTestPanel_2);
		//scrollPane_4.getViewport().setView(SubTestPanel_2);
		
		SubTestPanel_2.setLayout(null);
		subpanellist.add(SubTestPanel_2);
		
		label_1 = new JLabel("SELECTION");
		label_1.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		label_1.setBounds(21, 10, 46, 14);
		SubTestPanel_2.add(label_1);
		
		label_2 = new JLabel("TEST ITEM");
		label_2.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		label_2.setBounds(109, 10, 74, 14);
		SubTestPanel_2.add(label_2);
		
		label_3 = new JLabel("RESULT");
		label_3.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		label_3.setBounds(219, 10, 30, 14);
		SubTestPanel_2.add(label_3);
		
		scrollPane_2 = new JScrollPane();
		//MainTestpanel.add(scrollPane_2);
		
		SubTestPanel_3 = new JPanel();
		SubTestPanel_3.setBackground(SystemColor.textHighlightText);
		SubTestPanel_3.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		SubTestPanel_3.setBounds(580, 10, 275, 452);
		//scrollPane_4.add(SubTestPanel_3);
		//scrollPane_4.getViewport().setView(SubTestPanel_3);
		
		SubTestPanel_3.setLayout(null);
		subpanellist.add(SubTestPanel_3);
		
		label_4 = new JLabel("SELECTION");
		label_4.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		label_4.setBounds(21, 10, 46, 14);
		SubTestPanel_3.add(label_4);
		
		label_5 = new JLabel("TEST ITEM");
		label_5.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		label_5.setBounds(109, 10, 74, 14);
		SubTestPanel_3.add(label_5);
		
		label_6 = new JLabel("RESULT");
		label_6.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		label_6.setBounds(219, 10, 30, 14);
		SubTestPanel_3.add(label_6);
		
		scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(605, 10, 275, 452);
		//MainTestpanel.add(scrollPane_3);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 10, 276, 452);
		
		Container cont = new Container();
		cont.setBackground(SystemColor.textHighlightText);
		cont.add(SubTestPanel_1);
		cont.add(SubTestPanel_2);
		cont.add(SubTestPanel_3);
		
		//cont.setPreferredSize(cont.getPreferredSize());
		MainTestpanel.setLayout(new BorderLayout(0, 0));
		MainTestpanel.add(cont, BorderLayout.SOUTH);
		
		SubTestPanel_4 = new JPanel();
		SubTestPanel_4.setBounds(865, 10, 275, 452);
		cont.add(SubTestPanel_4);
		SubTestPanel_4.setBackground(SystemColor.textHighlightText);
		SubTestPanel_4.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		//scrollPane_4.add(SubTestPanel_4);
		//scrollPane_4.getViewport().setView(SubTestPanel_4);
		
		SubTestPanel_4.setLayout(null);
		subpanellist.add(SubTestPanel_4);
		
		JLabel label_7 = new JLabel("SELECTION");
		label_7.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		label_7.setBounds(22, 11, 46, 14);
		SubTestPanel_4.add(label_7);
		
		JLabel label_8 = new JLabel("TEST ITEM");
		label_8.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		label_8.setBounds(110, 11, 74, 14);
		SubTestPanel_4.add(label_8);
		
		JLabel label_9 = new JLabel("RESULT");
		label_9.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		label_9.setBounds(220, 11, 30, 14);
		SubTestPanel_4.add(label_9);
		scrollPane_4 = new JScrollPane(MainTestpanel);
		scrollPane_4.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane_4.setBounds(14, 95, 890, 496);
		//scrollPane_4.setPreferredSize(new Dimension(10000,500));
		
		scrollPane_4.setViewportView(MainTestpanel);
		
		MainTestpanel.setLayout(new GridLayout(0,1,0,1));
		
		
		
		
		//scrollPane_4.setPreferredSize(scrollPane_4.getPreferredSize());
		
		//MainTestpanel.add(scrollPane_4, BorderLayout.CENTER);
		
		frmStrykerManufacturingTests.getContentPane().add(scrollPane_4);
		
		result_and_progress_panel = new JPanel();
		result_and_progress_panel.setBounds(990, 50, 282, 38);
		frmStrykerManufacturingTests.getContentPane().add(result_and_progress_panel);
		result_and_progress_panel.setBackground(SystemColor.textHighlightText);
		result_and_progress_panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		result_and_progress_panel.setLayout(null);
		
		panel_4 = new JPanel();
		panel_4.setBounds(65, 8, 147, 18);
		panel_4.setBackground(Color.PINK);
		result_and_progress_panel.add(panel_4);
		
		lblTestStatus = new JLabel("READY");
		lblTestStatus.setBounds(225, 10, 37, 17);
		result_and_progress_panel.add(lblTestStatus);
		lblTestStatus.setFont(new Font("Calibri Light", Font.PLAIN, 12));
		
		lblResult = new JLabel("RESULT");
		lblResult.setBounds(10, 11, 46, 14);
		result_and_progress_panel.add(lblResult);
		lblResult.setBackground(UIManager.getColor("RadioButton.interiorBackground"));
		lblResult.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		
		test_count_panel = new JPanel();
		test_count_panel.setBounds(248, 50, 366, 38);
		frmStrykerManufacturingTests.getContentPane().add(test_count_panel);
		test_count_panel.setBackground(SystemColor.textHighlightText);
		test_count_panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		test_count_panel.setLayout(null);
		
		JLabel lblTotalTests = new JLabel("TOTAL TESTS :");
		lblTotalTests.setBounds(16, 12, 79, 14);
		test_count_panel.add(lblTotalTests);
		lblTotalTests.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		
		this.textField_Totaltests = new JTextField();
		textField_Totaltests.setText("0");
		textField_Totaltests.setBounds(87, 7, 46, 23);
		textField_Totaltests.setEditable(false);
		test_count_panel.add(textField_Totaltests);
		textField_Totaltests.setColumns(10);
		
		JLabel lblPassed = new JLabel("PASSED :");
		lblPassed.setBounds(143, 12, 46, 14);
		test_count_panel.add(lblPassed);
		lblPassed.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		
		this.textField_Passedtests = new JTextField();
		textField_Passedtests.setText("0");
		textField_Passedtests.setBounds(190, 7, 46, 23);
		textField_Passedtests.setEditable(false);
		test_count_panel.add(textField_Passedtests);
		textField_Passedtests.setColumns(10);
		
		JLabel lblFailed = new JLabel("FAILED :");
		lblFailed.setBounds(256, 12, 46, 14);
		test_count_panel.add(lblFailed); 
		lblFailed.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		
		this.textField_Failedtests = new JTextField();
		textField_Failedtests.setText("0");
		textField_Failedtests.setBounds(297, 7, 46, 23);
		textField_Failedtests.setEditable(false);
		test_count_panel.add(textField_Failedtests);
		textField_Failedtests.setColumns(10);
		
		
		
		
		
		
		test_control_panel = new JPanel();
		test_control_panel.setBounds(248, 5, 366, 38);
		frmStrykerManufacturingTests.getContentPane().add(test_control_panel);
		test_control_panel.setBackground(SystemColor.textHighlightText);
		test_control_panel.setBorder(new TitledBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.CENTER, TitledBorder.TOP,null, new Color(0, 0, 0)),"", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(51, 51, 51)));
		((TitledBorder) test_control_panel.getBorder()).setTitleFont(new Font("Tahoma", Font.BOLD, 10));
		test_control_panel.setLayout(null);
		
		
		
		this.btnStartButton = new JButton("");
		btnStartButton.setIcon(new ImageIcon(ManufacturingTests.class.getResource("/resources/Play.png")));
		btnStartButton.setName("RUN"); //Default name is start. This runs all the tests. Name of the button is used to map , different tests on bg_worker
		btnStartButton.setBounds(114, 6, 38, 25);
		btnStartButton.setMultiClickThreshhold(500);
		test_control_panel.add(btnStartButton);
		btnStartButton.addActionListener(new ActionListener() {
			FileHandler fh;
			
			

			public void actionPerformed(ActionEvent e) {	
					System.out.println("##########################################################################################");
					System.out.println("###########################PLAY - PAUSE BUTTON CLICKED####################################");
					System.out.println("##########################################################################################");
					//System.out.println("##########################################################################################");
					//StateMachine has to be synchronized with bg_worker's State_Machine Variable...
					//Send a string denoting the current action to be executed on the bg thread. No need to access the state machine variable here.
					System.out.println("##########################################################################################");
					System.out.println("Button Name : " + btnStartButton.getName());
					System.out.println("##########################################################################################");
					System.out.println("##########################################################################################");
					System.out.println("Initializer : " + Initializer_Value );
					System.out.println("##########################################################################################");
					
					if (worker_cancel_flag  == false) {
						System.out.println("##########################################################################################");
						System.out.println("Worker Cancel Flag : " + worker_cancel_flag);
						System.out.println("##########################################################################################");
						bg = new bg_worker(btnStartButton, true, true, serialport, Initializer_Value);
					}
					else
					{
						System.out.println("##########################################################################################");
						System.out.println("Cancelling Worker...");
						System.out.println("##########################################################################################");
					
						if(!Stop_Flag)
						{
							System.out.println("##########################################################################################");
							System.out.println("Pause Button Pressed");
							System.out.println("##########################################################################################");
							bg = new bg_worker(btnStartButton, false,true,serialport, Initializer_Value);
							worker_cancel_flag = false;
						}
						else
						{
							System.out.println("##########################################################################################");
							System.out.println("STOP Button Pressed");
							System.out.println("##########################################################################################");
							//textField_Failedtests.setText("");
							//textField_Passedtests.setText("");
							btnStartButton.setName("RUN");
							//bg = new bg_worker(btnStartButton, true,true,serialport, Initializer_Value);
							worker_cancel_flag = false;
							Stop_Flag = false;
						}
						
					}
					if(btnStartButton.getName().equals("RUN"))
					{
						//JOptionPane.showMessageDialog(frmStrykerManufacturingTests, "run", "btn name", JOptionPane.INFORMATION_MESSAGE);
						prepare_action_items(); //Only adds the test item to the actionitems if selected. No UI Update is done here
						System.out.println("TOTAL TESTS : " + togglebuttons.size());
						
						ManufacturingTests.logs_logger.info("<######----------------RUNNING---------------#######>");
						ManufacturingTests.report_logger.info("<######----------------RUNNING---------------#######>");
						
						timeoutflag = 0;
						total_test_count = 0;
						
						Board_Number = txtLboard.getText();
						FWVerNo = textField_FirmWareVerNo.getText();
						TimeNow = get_current_time_filename();
						System.out.println("Current Time : " + TimeNow);
						
						if(!Board_Number.equals(""))
						{
							
							ReportFilePath = ParentReportDirectoryName + File.separator ;
							ReportFileName = DateWiseReportDirectoryName + File.separator + Board_Number  + "_" +  FWVerNo +"_" +TimeNow+ ".txt";
							System.out.println("Report File Name : " + ReportFileName);
							
							try {
								report_logger.removeHandler(fh);
								fh = new FileHandler(ReportFileName,false);
								SimpleFormatter formatter = new SimpleFormatter();
								//System.setProperty("java.util.logging.SimpleFormatter.format",  "%1$tF %1$tT %4$s %2$s %5$s%6$s%n");
								fh.setFormatter(formatter);
								report_logger.setUseParentHandlers(false);
								report_logger.addHandler(fh);
								
								
								
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							
							
							
							report_logger.severe(ArrowDelimitter);
							
							report_logger.severe("                           REPORT FILE");
							report_logger.severe(ArrowDelimitter);
							report_logger.severe("Date\t\t\t" + get_current_date_filetext());
							report_logger.severe("Test Started at\t\t" + get_current_time_filetext());
							report_logger.info("Board Number\t\t" + Board_Number );
							report_logger.severe("Firmware Version\t" + FWVerNo);
							
							logs_logger.severe("\r\n\n");
							logs_logger.severe(ArrowDelimitter);
							logs_logger.severe("Date\t\t\t" + get_current_date_filetext());
							logs_logger.severe("Test Started at\t\t" + get_current_time_filetext());
							logs_logger.info("Board Number\t\t" + Board_Number );
							logs_logger.severe("Firmware Version\t" + FWVerNo);
							
							report_logger.severe(ArrowDelimitter);
							report_logger.severe("                      TEST CONFIGURATION");
							report_logger.severe(ArrowDelimitter);
							
							
							
							
							
							if(chckbxGroupTests.isSelected())
							{
								String Str = "\r\nLIGHTENGINE-TEST\tSELECTED\r\n" + "IRISMODULE-TEST\t\tSELECTED\r\n" + "FAN-TEST\t\tSELECTED\r\n" + "EXTERNALCAMFEED-TEST\tSELECTED\r\n" + "UARTLOOPBACK-TEST\tSELECTED\r\n" + "OPTICFIBER-TEST\t\tSELECTED\r\n" + "ESST-TEST\t\tSELECTED\r\n" + "ASST-TEST\t\tSELECTED\r\n" ;
								report_logger.severe(Str);
								
							}
							else
							{
								String Str = "";
								if(chckbxLightEngine.isSelected()) {Str = Str + "LIGHTENGINE-TEST\tSELECTED";}else {Str = Str + "LIGHTENGINE-TEST\tDESELECTED";}
								report_logger.severe(Str);
								Str = "";
								if(chckbxIrisModule.isSelected()) {Str = Str + "IRISMODULE-TEST\t\tSELECTED";}else {Str = Str + "IRISMODULE-TEST\t\tDESELECTED";}
								report_logger.severe(Str);
								Str = "";
								if(chckbxFans.isSelected()) {Str = Str + "FAN-TEST\t\tSELECTED";}else {Str = Str + "FAN-TEST\t\tDESELECTED";}
								report_logger.severe(Str);
								Str = "";
								if(chckbxExternalCamFeed.isSelected()) {Str = Str + "EXTERNALCAMFEED-TEST\tSELECTED";}else {Str = Str + "EXTERNALCAMFEED-TEST\tDESELECTED";}
								report_logger.severe(Str);
								Str = "";
								if(chckbxUartLoopBack.isSelected()) {Str = Str + "UARTLOOPBACK-TEST\tSELECTED";}else {Str = Str + "UARTLOOPBACK-TEST\tDESELECTED";}
								report_logger.severe(Str);
								Str = "";
								if(chckbxOpticFiber.isSelected()) {Str = Str + "OPTICFIBER-TEST\t\tSELECTED";}else {Str = Str + "OPTICFIBER-TEST\t\tDESELECTED";}
								report_logger.severe(Str);
								Str = "";
								if(chckbxEsst.isSelected()) {Str = Str + "ESST-TEST\t\tSELECTED";}else {Str = Str + "ESST-TEST\t\tDESELECTED";}
								report_logger.severe(Str);
								Str = "";
								if(chckbxAsst.isSelected()) {Str = Str + "ASST-TEST\t\tSELECTED";}else {Str = Str + "ASST-TEST\t\tDESELECTED";}
								report_logger.severe(Str);
								Str = "";
								
							}
							
							report_logger.severe(ArrowDelimitter);
							report_logger.severe("                        " + "TEST LOGS" );
							report_logger.severe(ArrowDelimitter);
							
							
							
							
							
							initialize_clearresults_bg_worker();
							cleartestresults_bg_worker.execute();
							
							System.out.println("Here");
							
							bg.execute();
							
							
						
						}
						else
						{
							JOptionPane.showMessageDialog(frmStrykerManufacturingTests, "Board Numberrr cannot be empty", "Board Number", JOptionPane.ERROR_MESSAGE);
						}
					
					//Initialize_auto_bg_worker();
					//autotest_bg_worker.execute();
					
				}
				else if(btnStartButton.getName().equals("PAUSE"))
				{
					if(bg_worker.futuree != null)
					{
						System.out.println("###############1.CANCELLING FUTUREE########################");
						bg_worker.futuree.cancel(true);
					}
					if(bg_worker.future != null)
					{
						System.out.println("###############2.CANCELLING FUTURE########################");
						bg_worker.future.cancel(true);
					}
					ManufacturingTests.logs_logger.info("<######----------------PAUSED---------------#######>");
					ManufacturingTests.report_logger.info("<######----------------PAUSED---------------#######>");
					if(bg != null)
					{
						//JOptionPane.showMessageDialog(frmStrykerManufacturingTests, "pause", "btn name", JOptionPane.INFORMATION_MESSAGE);
						Initializer_Value = bg_worker.test_count;
						
						worker_cancel_flag = true;
						System.out.println("For loop count when paused : " + Initializer_Value);
						//worker_cancel_flag = false;
						
						btnStartButton.setIcon(new ImageIcon(ManufacturingTests.class.getResource("/resources/Play.png")));
						
					}
					else
					{
						System.out.println("bg_worker Null!!");
					}
					
					
				}
			
			}
		});
		btnStartButton.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		
		this.btnStop = new JButton("");
		btnStop.setIcon(new ImageIcon(ManufacturingTests.class.getResource("/resources/Stop.png")));
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SwingWorker Stop_Worker = new SwingWorker<List<String>, String>()
				{

					@Override
					protected List<String> doInBackground() throws Exception {
						// TODO Auto-generated method stub
						if(bg != null)
						{
							if(bg_worker.futuree != null)
							{
								System.out.println("###############1.CANCELLING FUTUREE########################");
								bg_worker.futuree.cancel(true);
							}
							if(bg_worker.future != null)
							{
								System.out.println("###############2.CANCELLING FUTURE########################");
								bg_worker.future.cancel(true);
							}
						
							//JOptionPane.showMessageDialog(frmStrykerManufacturingTests, "pause", "btn name", JOptionPane.INFORMATION_MESSAGE);
							ManufacturingTests.logs_logger.info("<######----------------STOPPED---------------#######>");
							ManufacturingTests.report_logger.info("<######----------------STOPPED---------------#######>");
							Stop_Flag = true;
							worker_cancel_flag = true;
							System.out.println("For loop count when paused : " + Initializer_Value);
							//worker_cancel_flag = false;
							Initializer_Value = 0;
							serialport.openPort();
							serialport.removeDataListener();
							if(serialport.isOpen())
							{
								serialport.writeBytes("{MTESTEXIT}".getBytes(), "{MTESTEXIT}".length());
							}
							serialport.closePort();
							btnStartButton.setName("RUN");
							
							publish("STOPPED");							
							//bg.cancel(true);
						}
						else
						{
							System.out.println("bg_worker Null!!");
						}
						return null;
					}
					
					protected void process(List<String> chunks)
					{
						for(String str : chunks)
						{
							if(str.equals("STOPPED"))
							{
								btnStartButton.setIcon(new ImageIcon(ManufacturingTests.class.getResource("/resources/Play.png")));
								for(Component cmp : ManufacturingTests.togglebuttons)
								{
									cmp.setEnabled(true);
								}
								Component[] comp = test_configuration_panel.getComponents();
								for(Component com : comp)
								{
									com.setEnabled(true);
								}
								for(int i = 0;i<subpanellist.size();i++)
								{
									Component[] compo = subpanellist.get(i).getComponents();
									for(Component c : compo)
									{
										if(c instanceof JToggleButton)
										{
											((JToggleButton) c).setEnabled(true);
											//LIGHT ENGINE 
											if(c.getName() == btn_name_mapping.get(colorsensor1test))
											{
												if(chckbxLightEngine.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
											}
											if(c.getName() == btn_name_mapping.get(colorsensor2test))
											{
												if(chckbxLightEngine.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
											}
											if(c.getName() == btn_name_mapping.get(envpdtestzero))
											{
												if(chckbxLightEngine.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
											}
											if(c.getName() == btn_name_mapping.get(envpdtesthundred))
											{
												if(chckbxLightEngine.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
											}
											
											//IRIS_MODULE
											if(c.getName() == btn_name_mapping.get(iris1pdtestzero))
											{
												if(chckbxIrisModule.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
												
												
											}
											if(c.getName() == btn_name_mapping.get(iris2pdtestzero))
											{
												if(chckbxIrisModule.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
											}
											if(c.getName() == btn_name_mapping.get(iris1pdtesthundred))
											{
												if(chckbxIrisModule.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
											}
											if(c.getName() == btn_name_mapping.get(iris2pdtesthundred))
											{
												if(chckbxIrisModule.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
											}
											//UART LOOPBACK TESTS
											if(c.getName() == btn_name_mapping.get(uc1camuarttest))
											{
												if(chckbxUartLoopBack.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
												
												
											}
											if(c.getName() == btn_name_mapping.get(uc1debuguarttest))
											{
												if(chckbxUartLoopBack.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
											}
											if(c.getName() == btn_name_mapping.get(uc2camuarttest))
											{
												if(chckbxUartLoopBack.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
											}
											if(c.getName() == btn_name_mapping.get(uc2debuguarttest))
											{
												if(chckbxUartLoopBack.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
											}
											if(c.getName() == btn_name_mapping.get(uc2guiuarttest))
											{
												if(chckbxUartLoopBack.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
											}
											if(c.getName() == btn_name_mapping.get(uc2adduarttest))
											{
												if(chckbxUartLoopBack.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
											}
											//EXTERNAL CAM FEED
											if(c.getName() == btn_name_mapping.get(wlextpwmmuxtest))
											{
												if(chckbxExternalCamFeed.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
												
												//((JToggleButton) c).setSelected(true);
											}
											if(c.getName() == btn_name_mapping.get(envextpwmmuxtest))
											{
												if(chckbxExternalCamFeed.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
											}
											if(c.getName() == btn_name_mapping.get(irisextwlpwmmuxtest))
											{
												if(chckbxExternalCamFeed.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
											}
											if(c.getName() == btn_name_mapping.get(irisextenvpwmmuxtest))
											{
												if(chckbxExternalCamFeed.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
											}
											//FANS TESTS
											if(c.getName() == btn_name_mapping.get(lightenginefantest))
											{
												if(chckbxFans.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
												
												//((JToggleButton) c).setSelected(true);
											}
											if(c.getName() == btn_name_mapping.get(heatsinkfantest))
											{
												if(chckbxFans.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
												
											}
											if(c.getName() == btn_name_mapping.get(irisfantest))
											{
												if(chckbxFans.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
												
											}
											if(c.getName() == btn_name_mapping.get(fanfailuretest))
											{
												if(chckbxFans.isSelected())
												{
													((JToggleButton) c).setEnabled(true);
												}
												else
												{
													((JToggleButton) c).setEnabled(false);
												}
												
											}
										}
										
										
									}
								}
								
								if(bg_worker.fail_count == 0)
								{
									if(bg_worker.pass_count > 0 )
									{
										bg_worker.pass_count = 0;
										System.out.println("PASSSE");
										panel_4.setBackground(Color.GREEN);
										
										lblTestStatus.setText("PASS");
									}
								}
								else if(bg_worker.fail_count > 0)
								{
									bg_worker.fail_count = 0;
									System.out.println("FAIL");
									panel_4.setBackground(Color.RED);
									lblTestStatus.setText("FAILED");
								}
								total_test_count = 0;
								bg_worker.pass_count = 0;
								bg_worker.fail_count = 0;
								System.out.println("While Loop Breaks here");
								logs_logger.info(ArrowDelimitter);
								report_logger.info(ArrowDelimitter);
								report_logger.info("#########################  END OF REPORT  ##################################");
								report_logger.removeHandler(fh);
								
							}
						}
					}
					
					public void done()
					{
						worker_cancel_flag = false;
						Stop_Flag = false;
						
						
						
						
						//ManufacturingTests.btnStartButton.setName("RUN");
					}
			
				};
				Stop_Worker.execute();
				/*SwingWorker Stop_Worker = new SwingWorker<List<String>, String>()
				{

					@Override
					protected List<String> doInBackground() throws Exception {
						// TODO Auto-generated method stub
						if(bg != null)
						{
							
							worker_cancel_flag = true;
							publish("STOP");
							Stop_Flag = true;
						}
						
						return null;
					}
					protected void process(List<String> chunks)
					{
						for(String str : chunks)
						{
							if(str.equals("STOP"))
							{
								btnStartButton.setName("RUN");
								panel_4.setBackground(Color.pink);
								lblTestStatus.setText("READY");
								for(int t=0;t<subpanellist.size();t++)
								{
									Initializer_Value = 0;
									total_test_count = 0;
									pass_count = 0;
									fail_count = 0;
									Component[] comp = subpanellist.get(t).getComponents();
									for(Component c : comp)
									{
										if(c instanceof JPanel)
										{
											if(c.getBackground() == Color.green)
											{
												c.setBackground(Color.decode("#e3e3e3"));
											}
											else if(c.getBackground() == Color.red)
											{
												c.setBackground(Color.decode("#e3e3e3"));
											}
											else if(c.getBackground() == Color.MAGENTA)
											{
												c.setBackground(Color.decode("#e3e3e3"));
											}
										}
										if(c instanceof JToggleButton)
										{
											if(((JToggleButton) c).isSelected())
											{
												
											}
										}
									}
									
								}
								report_logger.removeHandler(report_handler);
								logs_logger.removeHandler(logs_handler);
								btnStartButton.setIcon(new ImageIcon(ManufacturingTests.class.getResource("/resources/Play.png")));
								
								//Send MTESTEXIT Command on stop button press
								serialport.openPort();
								if(serialport.isOpen())
								{
									serialport.writeBytes("{MTESTEXIT}".getBytes(), "{MTESTEXIT}".length());
									
								}
								serialport.closePort();
							}
						}
					}
					
					
					
				};
				Stop_Worker.execute();*/
				
			}
		});
		btnStop.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		btnStop.setBounds(201, 6, 38, 25);
		test_control_panel.add(btnStop);
		
		
		this.rdbtnClearTestSelections = new JRadioButton("CLEAR TEST RESULTS");
		rdbtnClearTestSelections.setEnabled(false);
		rdbtnClearTestSelections.setBounds(990, 5, 149, 23);
		frmStrykerManufacturingTests.getContentPane().add(rdbtnClearTestSelections);
		rdbtnClearTestSelections.setBackground(SystemColor.textHighlightText);
		rdbtnClearTestSelections.addItemListener(new ItemListener() {
			private Timer timer;
			
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					//togglebuttons.clear();
					panel_4.setBackground(Color.PINK);
					lblTestStatus.setText("READY");
					rdbtnClearTestSelections.setEnabled(false);
					for(int t=0;t<subpanellist.size();t++)
					{
						Component[] comp = subpanellist.get(t).getComponents();
						for(Component c : comp)
						{
							if(c instanceof JPanel)
							{
								if(c.getBackground() == Color.green)
								{
									c.setBackground(Color.decode("#e3e3e3"));
								}
								else if(c.getBackground() == Color.red)
								{
									c.setBackground(Color.decode("#e3e3e3"));
								}
								else if(c.getBackground() == Color.MAGENTA)
								{
									c.setBackground(Color.decode("#e3e3e3"));
								}
							}
							if(c instanceof JToggleButton)
							{
								if(((JToggleButton) c).isSelected())
								{
									
								}
							}
						}
						
					}
					
					
					
					
					

					ActionListener taskPerformer = new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
			// ...Perform a task...
						rdbtnClearTestSelections.setSelected(false);
						rdbtnClearTestSelections.setEnabled(true);

						timer.stop();
					}
					};
					this.timer = new Timer(100, taskPerformer);
					timer.setRepeats(false);
					timer.start();

				}
			}
		});
		rdbtnClearTestSelections.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		
		test_configuration_panel = new JPanel();
		test_configuration_panel.setBounds(622, 5, 358, 83);
		frmStrykerManufacturingTests.getContentPane().add(test_configuration_panel);
		test_configuration_panel.setBackground(SystemColor.textHighlightText);
		test_configuration_panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		test_configuration_panel.setLayout(null);
		this.chckbxGroupTests = new JCheckBox("GROUP TESTS");
		chckbxGroupTests.setBackground(SystemColor.textHighlightText);
		chckbxGroupTests.setBounds(6, 7, 85, 23);
		test_configuration_panel.add(chckbxGroupTests);
		chckbxGroupTests.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				
				if(e.getStateChange() == ItemEvent.SELECTED) {
				selectalltestbuttons();
				System.out.println("Checkbox Checked");
				chckbxGroupTests.setText("GROUP TESTS");
				chckbxLightEngine.setSelected(true);
				chckbxIrisModule.setSelected(true);
				chckbxFans.setSelected(true);
				chckbxExternalCamFeed.setSelected(true);
				chckbxUartLoopBack.setSelected(true);
				chckbxOpticFiber.setSelected(true);
				chckbxEsst.setSelected(true);
				chckbxAsst.setSelected(true);
				chckbxColorSensors.setSelected(true);
				
				//chckbxColorSensors.setEnabled(false);
				//chckbxLightEngine.setEnabled(false);
				//chckbxIrisModule.setEnabled(false);
				////chckbxFans.setEnabled(false);
				//chckbxExternalCamFeed.setEnabled(false);
				//chckbxUartLoopBack.setEnabled(false);
				//chckbxOpticFiber.setEnabled(false);
				//chckbxEsst.setEnabled(false);
				//chckbxAsst.setEnabled(false);
				
				
				
				}
				if(e.getStateChange() == ItemEvent.DESELECTED) {
					unselectalltestbuttons();
					chckbxLightEngine.setSelected(false);
					chckbxIrisModule.setSelected(false);
					chckbxFans.setSelected(false);
					chckbxExternalCamFeed.setSelected(false);
					chckbxUartLoopBack.setSelected(false);
					chckbxOpticFiber.setSelected(false);
					chckbxEsst.setSelected(false);
					chckbxAsst.setSelected(false);
					chckbxColorSensors.setSelected(false);
					
					chckbxColorSensors.setEnabled(true);
					
					chckbxLightEngine.setEnabled(true);
					chckbxIrisModule.setEnabled(true);
					chckbxFans.setEnabled(true);
					chckbxExternalCamFeed.setEnabled(true);
					chckbxUartLoopBack.setEnabled(true);
					chckbxOpticFiber.setEnabled(true);
					chckbxEsst.setEnabled(true);
					chckbxAsst.setEnabled(true);
					
					
				}
			}
		});
		
		chckbxGroupTests.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		
		chckbxLightEngine = new JCheckBox("LIGHT ENGINE");
		chckbxLightEngine.setBackground(SystemColor.textHighlightText);
		chckbxLightEngine.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				
				for(int t=0;t<subpanellist.size();t++)
				{
					Component[] comp = subpanellist.get(t).getComponents();
					if(e.getStateChange() == ItemEvent.SELECTED)
					{
					for(Component c : comp)
					{
						if(c instanceof JToggleButton)
						{
							//following tests should be run
							
							//enable color sensor tests alone
							if(c.getName() == btn_name_mapping.get(colorsensor1test))
							{
								((JToggleButton) c).setEnabled(true);
								((JToggleButton) c).setSelected(true);
								
							}
							else if(c.getName() == btn_name_mapping.get(colorsensor2test))
							{
								((JToggleButton) c).setEnabled(true);
								((JToggleButton) c).setSelected(true);
								
							}
							//PD tests laser should be run
							else if(c.getName() == btn_name_mapping.get(envpdtestzero))
							{
								((JToggleButton) c).setEnabled(true);
								((JToggleButton) c).setSelected(true);
							}
							else if(c.getName() == btn_name_mapping.get(envpdtesthundred))
							{
								((JToggleButton) c).setEnabled(true);
								((JToggleButton) c).setSelected(true);
							}
							
							
							
							
							
						}
					}
				}
				
				
				else if(e.getStateChange() == ItemEvent.DESELECTED)
				{
					chckbxGroupTests.setSelected(false);
					for(Component c : comp)
					{
						if(c instanceof JToggleButton)
						{
							//following tests should be run
							
							//enable color sensor tests alone
							if(c.getName() == btn_name_mapping.get(colorsensor1test))
							{
								((JToggleButton) c).setEnabled(false);
								((JToggleButton) c).setSelected(false);
							}
							else if(c.getName() == btn_name_mapping.get(colorsensor2test))
							{
								((JToggleButton) c).setEnabled(false);
								((JToggleButton) c).setSelected(false);
							}
							//PD tests laser should be run
							else if(c.getName() == btn_name_mapping.get(envpdtestzero))
							{
								((JToggleButton) c).setEnabled(false);
								((JToggleButton) c).setSelected(false);
							}
							else if(c.getName() == btn_name_mapping.get(envpdtesthundred))
							{
								((JToggleButton) c).setEnabled(false);
								((JToggleButton) c).setSelected(false);
							}
							
							
							
							
							
						}
					}
					
				}
			
				
			}
			}
		});
		chckbxLightEngine.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxLightEngine.setBounds(120, 7, 97, 23);
		test_configuration_panel.add(chckbxLightEngine);
		
		chckbxIrisModule = new JCheckBox("IRIS MODULE");
		chckbxIrisModule.setBackground(SystemColor.textHighlightText);
		chckbxIrisModule.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				for(int t=0;t<subpanellist.size();t++)
				{
					Component[] comp = subpanellist.get(t).getComponents();
					if(e.getStateChange() == ItemEvent.SELECTED)
					{
					for(Component c : comp)
					{
						
						if(c.getName() == btn_name_mapping.get(iris1pdtestzero))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						else if(c.getName() == btn_name_mapping.get(iris2pdtestzero))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						else if(c.getName() == btn_name_mapping.get(iris1pdtesthundred))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						else if(c.getName() == btn_name_mapping.get(iris2pdtesthundred))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						
						
					}
					
					
				}
				else if(e.getStateChange() == ItemEvent.DESELECTED)
				{
					chckbxGroupTests.setSelected(false);
					for(Component c : comp)
					{
						
						if(c.getName() == btn_name_mapping.get(iris1pdtestzero))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						else if(c.getName() == btn_name_mapping.get(iris2pdtestzero))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						else if(c.getName() == btn_name_mapping.get(iris1pdtesthundred))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						else if(c.getName() == btn_name_mapping.get(iris2pdtesthundred))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						
					}
					
					
				}
			}
		}
		});
		chckbxIrisModule.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxIrisModule.setBounds(242, 7, 97, 23);
		test_configuration_panel.add(chckbxIrisModule);
		
		chckbxFans = new JCheckBox("FANs");
		chckbxFans.setBackground(SystemColor.textHighlightText);
		chckbxFans.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				for(int t=0;t<subpanellist.size();t++)
				{
					Component[] comp = subpanellist.get(t).getComponents();
					if(e.getStateChange() == ItemEvent.SELECTED)
					{
					for(Component c : comp)
					{
						if(c.getName() == btn_name_mapping.get(lightenginefantest))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						else if(c.getName() == btn_name_mapping.get(heatsinkfantest))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						else if(c.getName() == btn_name_mapping.get(irisfantest))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						else if(c.getName() == btn_name_mapping.get(fanfailuretest))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						
					}
					
				}
				else if(e.getStateChange() == ItemEvent.DESELECTED)
				{
					chckbxGroupTests.setSelected(false);

					for(Component c: comp)
					{
						if(c.getName() == btn_name_mapping.get(lightenginefantest))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						else if(c.getName() == btn_name_mapping.get(heatsinkfantest))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						else if(c.getName() == btn_name_mapping.get(irisfantest))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						else if(c.getName() == btn_name_mapping.get(fanfailuretest))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						
					}
					
				}
				
			}
			}
		});
		chckbxFans.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxFans.setBounds(6, 31, 97, 23);
		test_configuration_panel.add(chckbxFans);
		
		chckbxExternalCamFeed = new JCheckBox("EXTERNAL CAM FEED");
		chckbxExternalCamFeed.setBackground(SystemColor.textHighlightText);
		chckbxExternalCamFeed.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				for(int t=0;t<subpanellist.size();t++)
				{
					Component[] comp = subpanellist.get(t).getComponents();
					if(e.getStateChange() == ItemEvent.SELECTED)
					{
					for(Component c : comp)
					{
						if(c.getName() == btn_name_mapping.get(wlextpwmmuxtest))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						else if(c.getName() == btn_name_mapping.get(envextpwmmuxtest))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						else if(c.getName() == btn_name_mapping.get(irisextwlpwmmuxtest))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						else if(c.getName() == btn_name_mapping.get(irisextenvpwmmuxtest))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						
					}
					
				}
				else if(e.getStateChange() == ItemEvent.DESELECTED)
				{
					chckbxGroupTests.setSelected(false);
					for(Component c: comp)
					{
						if(c.getName() == btn_name_mapping.get(wlextpwmmuxtest))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						else if(c.getName() == btn_name_mapping.get(envextpwmmuxtest))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						else if(c.getName() == btn_name_mapping.get(irisextwlpwmmuxtest))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						else if(c.getName() == btn_name_mapping.get(irisextenvpwmmuxtest))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						
					}
					
				}
				
			}
			}
		});
		chckbxExternalCamFeed.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxExternalCamFeed.setBounds(119, 31, 120, 23);
		test_configuration_panel.add(chckbxExternalCamFeed);
		
		chckbxUartLoopBack = new JCheckBox("UART LOOPBACK ");
		chckbxUartLoopBack.setBackground(SystemColor.textHighlightText);
		chckbxUartLoopBack.addItemListener(new ItemListener() {
			
			public void itemStateChanged(ItemEvent e) {
				for(int t=0;t<subpanellist.size();t++)
				{
					Component[] comp = subpanellist.get(t).getComponents();
					if(e.getStateChange() == ItemEvent.SELECTED)
					{
					for(Component c : comp)
					{
						if(c.getName() == btn_name_mapping.get(uc1camuarttest))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						else if(c.getName() == btn_name_mapping.get(uc1debuguarttest))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						else if(c.getName() == btn_name_mapping.get(uc2camuarttest))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						else if(c.getName() == btn_name_mapping.get(uc2debuguarttest))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						else if(c.getName() == btn_name_mapping.get(uc2guiuarttest))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						else if(c.getName() == btn_name_mapping.get(uc2adduarttest))
						{
							((JToggleButton) c).setEnabled(true);
							((JToggleButton) c).setSelected(true);
						}
						
					}
					
				}
				else if(e.getStateChange() == ItemEvent.DESELECTED)
				{
					chckbxGroupTests.setSelected(false);
					for(Component c: comp)
					{
						if(c.getName() == btn_name_mapping.get(uc1camuarttest))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						else if(c.getName() == btn_name_mapping.get(uc1debuguarttest))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						else if(c.getName() == btn_name_mapping.get(uc2camuarttest))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						else if(c.getName() == btn_name_mapping.get(uc2debuguarttest))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						else if(c.getName() == btn_name_mapping.get(uc2guiuarttest))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						else if(c.getName() == btn_name_mapping.get(uc2adduarttest))
						{
							((JToggleButton) c).setEnabled(false);
							((JToggleButton) c).setSelected(false);
						}
						
					}
					
				}
				
			}
			}
		});
		chckbxUartLoopBack.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxUartLoopBack.setBounds(242, 31, 103, 23);
		test_configuration_panel.add(chckbxUartLoopBack);
		
		chckbxOpticFiber = new JCheckBox("OPTIC FIBER");
		chckbxOpticFiber.setBackground(SystemColor.textHighlightText);
		chckbxOpticFiber.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				for(int t=0;t<subpanellist.size();t++)
				{
					Component[] comp = subpanellist.get(t).getComponents();
					if(e.getStateChange() == ItemEvent.SELECTED)
					{
					for(Component c : comp)
					{
						
						
					}
					
				}
				else if(e.getStateChange() == ItemEvent.DESELECTED)
				{
					
					for(Component c: comp)
					{
						
						
					}
					
				}
				
			}
			}
		});
		chckbxOpticFiber.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxOpticFiber.setBounds(6, 55, 85, 23);
		test_configuration_panel.add(chckbxOpticFiber);
		
		chckbxEsst = new JCheckBox("ESST");
		chckbxEsst.setBackground(SystemColor.textHighlightText);
		chckbxEsst.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				for(int t=0;t<subpanellist.size();t++)
				{
					Component[] comp = subpanellist.get(t).getComponents();
					if(e.getStateChange() == ItemEvent.SELECTED)
					{
					
					
				}
				else if(e.getStateChange() == ItemEvent.DESELECTED)
				{
					
					
					
				}
				
			}
			}
		});
		chckbxEsst.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxEsst.setBounds(120, 55, 57, 23);
		test_configuration_panel.add(chckbxEsst);
		
		chckbxAsst = new JCheckBox("ASST");
		chckbxAsst.setBackground(SystemColor.textHighlightText);
		chckbxAsst.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				for(int t=0;t<subpanellist.size();t++)
				{
					Component[] comp = subpanellist.get(t).getComponents();
					if(e.getStateChange() == ItemEvent.SELECTED)
					{
					
					
				}
				else if(e.getStateChange() == ItemEvent.DESELECTED)
				{

					
					
				}
				
			}
			}
		});
		chckbxAsst.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxAsst.setBounds(242, 55, 57, 23);
		test_configuration_panel.add(chckbxAsst);
		
		this.chckbxColorSensors = new JCheckBox("COLOR SENSORS");
		chckbxColorSensors.setBackground(SystemColor.textHighlightText);
		chckbxColorSensors.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				for(int t=0;t<subpanellist.size();t++)
				{
					Component[] comp = subpanellist.get(t).getComponents();
					if(e.getStateChange() == ItemEvent.SELECTED)
					{
					for(Component c : comp)
					{
						if(c.getName() == btn_name_mapping.get(colorsensor1test))
						{
							((JToggleButton) c).setSelected(true);
						}
						else if(c.getName() == btn_name_mapping.get(colorsensor2test))
						{
							((JToggleButton) c).setSelected(true);
						}
						
						
					}
					
				}
				else if(e.getStateChange() == ItemEvent.DESELECTED)
				{

					for(Component c: comp)
					{
						if(c.getName() == btn_name_mapping.get(colorsensor1test))
						{
							((JToggleButton) c).setSelected(false);
						}
						else if(c.getName() == btn_name_mapping.get(colorsensor2test))
						{
							((JToggleButton) c).setSelected(false);
						}
						
					}
					
				}
				
			}
			}
		});
		chckbxColorSensors.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxColorSensors.setBounds(6, 85, 120, 23);
		chckbxColorSensors.setVisible(false);
		test_configuration_panel.add(chckbxColorSensors);
		//Construct the UI Components here
		for(int i=1;i<TestItems.size()+1;i++)
		{
			if(i<=20) 
			{
				if((SubTestPanel_1.getParent() == null))
				{
					MainTestpanel.add(SubTestPanel_1);
				}
				Place_TestItem(TestItems.get(i),SubTestPanel_1,i);
				construct_indicatorpanel_on_button(TestItems.get(i), SubTestPanel_1,i);
				construct_result_panel(TestItems.get(i), SubTestPanel_1, i);
			}
			else if(i>20 && i<=40)
			{
				Place_TestItem(TestItems.get(i),SubTestPanel_2,i);
				construct_indicatorpanel_on_button(TestItems.get(i), SubTestPanel_2,i);
				construct_result_panel(TestItems.get(i), SubTestPanel_2, i);
			}
			else if(i>40 && i<=60)
			{
				Place_TestItem(TestItems.get(i),SubTestPanel_3,i);
				construct_indicatorpanel_on_button(TestItems.get(i), SubTestPanel_3,i);
				construct_result_panel(TestItems.get(i), SubTestPanel_3, i);
			}
			else if(i>60 && i <=80)
			{
				Place_TestItem(TestItems.get(i),SubTestPanel_4,i);
				construct_indicatorpanel_on_button(TestItems.get(i), SubTestPanel_4,i);
				construct_result_panel(TestItems.get(i), SubTestPanel_4, i);
			}
			disable_panel_items();
			
			result_indicator_mappings.put("{MTESTSTART}", null);
			selectalltestbuttons();
			if(subpanellist.size() >= 1)
			{
				for(int t=0;t<subpanellist.size();t++)
				{
					Component[] comp = subpanellist.get(t).getComponents();
					for(Component c : comp)
					{
						c.setEnabled(true);
						
					}
				}
			}
			//textField_Totaltests.setText("");
			//textField_Passedtests.setText("");
			//textField_Failedtests.setText("");
			//chckbxLightEngine.setSelected(false);
			//chckbxIrisModule.setSelected(false);
			//chckbxFans.setSelected(false);
			//chckbxExternalCamFeed.setSelected(false);
			//chckbxUartLoopBack.setSelected(false);
			//chckbxOpticFiber.setSelected(false);
			//chckbxEsst.setSelected(false);
			//chckbxAsst.setSelected(false);
			chckbxGroupTests.setEnabled(true);
			chckbxGroupTests.setSelected(true);
			
		}
		
	} //Manufacturing Tests Class Ends Here
	public String get_current_time_filename()
	{
		String Time = "";
		localDateTime = LocalDateTime.now();
		DateToday = dt.format(localDateTime);
		Time = tm.format(localDateTime);
		return Time;
		
	}
	public String get_current_time_filetext()
	{
		String Time = "";
		localDateTime = LocalDateTime.now();
		Time = localDateTime.format(time);
		return Time;
	}
	public String get_current_date_filename()
	{
		String Date = "";
		localDate = LocalDate.now();
		Date = localDate.format(dt);
		return Date;
	}
	public String get_current_date_filetext()
	{
		String Date = "";
		localDate = LocalDate.now();
		Date = localDate.format(date);
		return Date;
	}
	
	public void selectalltestbuttons()
	{
		if(subpanellist.size() >=1)
		{
			for(int t=0;t<subpanellist.size();t++)
			{
				Component[] comp = subpanellist.get(t).getComponents();
				for(Component c : comp)
				{
					if(c instanceof JToggleButton)
					{	
						((JToggleButton) c).setSelected(true);
					}
				}
			}
		}
		
	}
	public boolean Create_Directory(String FileName)
	{
		
		boolean status = false;
		File filed = new File(FileName);
	    if(!filed.exists())
	    {  
	    	if(filed.mkdirs())
	    	{ 
	    		status = true;
	    		 
	    	}
	    }
	    else
	    { 
	    	status = false;
	    	  
	    }
		return status;
		
	}
	public Logger Create_Handler(Logger logger, String FileName, FileHandler fh)
	{
		try {
			System.out.println("Name of the logger : " + logger.getName());
			if(!logger.getName().equals("Report"))
			{
				
				fh  = new FileHandler(FileName,true);
				SimpleFormatter formatter = new SimpleFormatter();
				//System.setProperty("java.util.logging.SimpleFormatter.format",  "%1$tF %1$tT %4$s %2$s %5$s%6$s%n");
				fh.setFormatter(formatter);
				
				logger.setUseParentHandlers(false);
				logger.addHandler(fh);
				
			}
			else
			{
				fh  = new FileHandler(FileName,false);
				SimpleFormatter formatter = new SimpleFormatter();
				//System.setProperty("java.util.logging.SimpleFormatter.format",  "%1$tF %1$tT %4$s %2$s %5$s%6$s%n");
				fh.setFormatter(formatter);
				
				logger.setUseParentHandlers(false);
				logger.addHandler(fh);
				
			}
			
			
			
			
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		
		return logger;
	}
	public void unselectalltestbuttons()
	{
		for(int t=0;t<subpanellist.size();t++)
		{
			Component[] comp = subpanellist.get(t).getComponents();
			for(Component c : comp)
			{
				if(c instanceof JToggleButton)
				{	
					((JToggleButton) c).setSelected(false);
				}
			}
		}
	}
	
	
	public void disable_panel_items()
	{
		for(int t=0;t<subpanellist.size();t++)
		{
			Component[] comp = subpanellist.get(t).getComponents();
			for(Component c : comp)
			{
				c.setEnabled(false);
			}
		}
		
		
	}
	public void Place_TestItem(String TestName, JPanel panel, int referenceno)
	{
		//System.out.println("Test Name : " + TestName);
		JLabel label = new JLabel();
		label.setText(TestName);
		label.setName(test_name_mappings.get(TestName));
		//System.out.println("Label Name : " + label.getName());
		label.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		if(referenceno >0 && referenceno <=20)
		{
			label.setBounds(testdescriptionxbasesubpanel1, testdescriptionybasesubpanel1, testdescriptionwidthsubpanel1, testdescriptionheightsubpanel1);
			testdescriptionybasesubpanel1 = testdescriptionybasesubpanel1 + TestDescriptionTextYdifference;
		}
		else if(referenceno>20 && referenceno<=40)
		{
			label.setBounds(testdescriptionxbasesubpanel2, testdescriptionybasesubpanel2, testdescriptionwidthsubpanel2, testdescriptionheightsubpanel2);
			testdescriptionybasesubpanel2 = testdescriptionybasesubpanel2 + TestDescriptionTextYdifference;
		}
		else if(referenceno >40 && referenceno <=60)
		{
			label.setBounds(testdescriptionxbasesubpanel3, testdescriptionybasesubpanel3, testdescriptionwidthsubpanel3, testdescriptionheightsubpanel3);
			testdescriptionybasesubpanel3 = testdescriptionybasesubpanel3 + TestDescriptionTextYdifference;
		}
		else if(referenceno >60 && referenceno <=80)
		{
			label.setBounds(testdescriptionxbasesubpanel4, testdescriptionybasesubpanel4, testdescriptionwidthsubpanel4, testdescriptionheightsubpanel4);
			testdescriptionybasesubpanel4 = testdescriptionybasesubpanel4 + TestDescriptionTextYdifference;
		}
		
		test_items.add(label);
		panel.add(label);
	 }
	
	
	
	
	public void construct_togglebutton(String CommandName, JPanel placementpanel,final JPanel indicatorpanel,  final int referenceno)
	{
		JToggleButton btn = new JToggleButton(); 
		//btn = Get_Button_Name(CommandName);
		//btn.setName(CommandName);
		btn.setName(btn_name_mapping.get(CommandName));
		if(referenceno >0 && referenceno <=20)
		{
			btn.setBounds(buttonxbasesubpanel1, buttonybasesubpanel1, buttonwidthsubpanel1, buttonheightsubpanel1);
			buttonybasesubpanel1 = buttonybasesubpanel1 + SelectionButtonYdifference;
		}
		else if(referenceno > 20 && referenceno <=40)
		{
			btn.setBounds(buttonxbasesubpanel2, buttonybasesubpanel2, buttonwidthsubpanel2, buttonheightsubpanel2);
			buttonybasesubpanel2 = buttonybasesubpanel2 + SelectionButtonYdifference;
		}
		else if(referenceno > 20 && referenceno <=60)
		{
			btn.setBounds(buttonxbasesubpanel3, buttonybasesubpanel3, buttonwidthsubpanel3, buttonheightsubpanel3);
			buttonybasesubpanel3 = buttonybasesubpanel3 + SelectionButtonYdifference;
		}
		else if(referenceno > 60 && referenceno <=80)
		{
			btn.setBounds(buttonxbasesubpanel4, buttonybasesubpanel4, buttonwidthsubpanel4, buttonheightsubpanel4);
			buttonybasesubpanel4 = buttonybasesubpanel4 + SelectionButtonYdifference;
		}
		
		selection_buttons.add(btn);
		btn.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED)
				{
					indicatorpanel.setBackground(Color.blue);
				}
				else if(e.getStateChange() == ItemEvent.DESELECTED)
				{
					indicatorpanel.setBackground(Color.gray);
					
				}
			}
		});
		
		placementpanel.add(btn);
	}
	
	public void construct_indicatorpanel_on_button(String PanelName, JPanel placementpanel, int referenceno)
	{
		JPanel test_control_panel = new JPanel();
		if(referenceno > 0 && referenceno <= 20)
		{
			test_control_panel.setBounds(panelxbasesubpanel1, panelybasesubpanel1, panelwidthsubpanel1, panelheightsubpanel1);
			panelybasesubpanel1 = panelybasesubpanel1 + SelectionIndicatorPanelYdifference;
		}
		else if(referenceno > 20 && referenceno <= 40)
		{
			test_control_panel.setBounds(panelxbasesubpanel2, panelybasesubpanel2, panelwidthsubpanel2, panelheightsubpanel2);
			panelybasesubpanel2 = panelybasesubpanel2 + SelectionIndicatorPanelYdifference;
		}
		else if(referenceno > 40 && referenceno <= 60)
		{
			test_control_panel.setBounds(panelxbasesubpanel3, panelybasesubpanel3, panelwidthsubpanel3, panelheightsubpanel3);
			panelybasesubpanel3 = panelybasesubpanel3 + SelectionIndicatorPanelYdifference;
		}
		else if(referenceno > 60 && referenceno <= 80)
		{
			test_control_panel.setBounds(panelxbasesubpanel4, panelybasesubpanel4, panelwidthsubpanel4, panelheightsubpanel4);
			panelybasesubpanel4 = panelybasesubpanel4 + SelectionIndicatorPanelYdifference;
		}
		
		placementpanel.setName(test_name_mappings.get(PanelName));
		//System.out.println(placementpanel.getName());
		test_control_panel.setOpaque(true);
		test_control_panel.setBackground(Color.GRAY);
		
		placementpanel.add(test_control_panel);
	
		placementpanel.validate();
		placementpanel.repaint();
		construct_togglebutton(TestItems.get(referenceno),placementpanel, test_control_panel,referenceno);
		
	}
	
	public void construct_result_panel(String PanelName, JPanel placementpanel, int referenceno)
	{
		JPanel result_indicator_panel = new JPanel();
		if(referenceno > 0 && referenceno <= 20)
		{
			result_indicator_panel.setBounds(resultindicatorxbasesubpanel1, resultindicatorybasesubpanel1, resultindicatorwidthsubpanel1, resultindicatorheightsubpanel1);
			resultindicatorybasesubpanel1 = resultindicatorybasesubpanel1 + SelectionIndicatorPanelYdifference;
		}
		else if(referenceno > 20 && referenceno <= 40)
		{
			result_indicator_panel.setBounds(resultindicatorxbasesubpanel2, resultindicatorybasesubpanel2, resultindicatorwidthsubpanel2, resultindicatorheightsubpanel2);
			resultindicatorybasesubpanel2 = resultindicatorybasesubpanel2 + SelectionIndicatorPanelYdifference;
		}
		else if(referenceno > 40 && referenceno <= 60)
		{
			result_indicator_panel.setBounds(resultindicatorxbasesubpanel3, resultindicatorybasesubpanel3, resultindicatorwidthsubpanel3, resultindicatorheightsubpanel3);
			resultindicatorybasesubpanel3 = resultindicatorybasesubpanel3 + SelectionIndicatorPanelYdifference;
		}
		else if(referenceno > 60 && referenceno <= 80)
		{
			result_indicator_panel.setBounds(resultindicatorxbasesubpanel4, resultindicatorybasesubpanel4, resultindicatorwidthsubpanel4, resultindicatorheightsubpanel4);
			resultindicatorybasesubpanel4 = resultindicatorybasesubpanel4 + SelectionIndicatorPanelYdifference;
		}
		result_indicator_panel.setName(panel_name_mappings.get(PanelName));
		result_indicator_mappings.put(selection_buttons.get(referenceno-1).getName(), result_indicator_panel);
		result_indicator_panel.setBackground(Color.decode("#e3e3e3"));
		placementpanel.add(result_indicator_panel);
		placementpanel.revalidate();
		placementpanel.repaint();
		
	}
	
	 public static TableModel toTableModel(Map map) {
	     DefaultTableModel model = new DefaultTableModel (
	   new Object[] { "Key", "Value" }, 0
	  );
	  for (Iterator it = map.entrySet().iterator(); it.hasNext();) {
	   Map.Entry entry = (Map.Entry)it.next();
	   model.addRow(new Object[] { entry.getKey(), entry.getValue() });
	  }
	  return model;
	 }
	public static Logger initialize_logger()
	{
		try {
			fh = new FileHandler("D:/MyLogFile.txt");
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		logger.addHandler(fh);
		SimpleFormatter formatter = new SimpleFormatter();  
		fh.setFormatter(formatter);
		return logger;  
	}
	//############################################################
	//Function that check , which tests should be performed based on the checkboxes selected
	public static ArrayList<Boolean> prepare_action_items()
	{
		int total_test = 0;
		Component[] panel_1comp = SubTestPanel_1.getComponents();
		Component[] panel_2comp = SubTestPanel_2.getComponents();
		Component[] panel_3comp = SubTestPanel_3.getComponents();
		Component[] panel_4comp = SubTestPanel_4.getComponents();
		
		togglebuttons.clear();
		for(Component c : panel_1comp)
		{
			if(c instanceof JToggleButton)
			{
				if(((JToggleButton) c).isSelected())
				{
					togglebuttons.add((JToggleButton) c);
					total_test = total_test + 1;
				}
				
			}
		}
		for(Component c : panel_2comp)
		{
			if(c instanceof JToggleButton)
			{
				if(((JToggleButton) c).isSelected())
				{
					togglebuttons.add((JToggleButton) c);
					total_test = total_test + 1;
				}
			}
		}
		for(Component c : panel_3comp)
		{
			if(c instanceof JToggleButton)
			{
				if(((JToggleButton) c).isSelected())
				{
					togglebuttons.add((JToggleButton) c);
					total_test = total_test + 1;
				}
			}
		}
		for(Component c : panel_4comp)
		{
			if(c instanceof JToggleButton)
			{
				if(((JToggleButton) c).isSelected())
				{
					togglebuttons.add((JToggleButton) c);
					total_test = total_test + 1;
				}
				
			}
		}
		System.out.println("Size of togglebuttons : " + togglebuttons.size());
		
		textField_Totaltests.setText(Integer.toString(total_test));
		action_items.clear(); // An Array that holds the boolean values based on checkbox selection. - If selected - True else false
		for(int i = 0;i<test_items.size();i++)
		{
			
		}
		//for(int i=0;i<action_items.size();i++)
		//{
		//	System.out.println(action_items.get(i));
		//}
		System.out.println("Total Tests to be performed : " + total_test);
		if(total_test > 0)
		{
			pbval_divisor = 100/total_test;
		}
		
		return action_items;
		
		
	}
	
	
	public static void UpdateTestResults( String result, String reason, String source, String value)
	{
	
	try
		{
			
			System.out.println("btnname to be mapped : " + btnname_mapping_topanel);
			//No Error
			if(reason.equals(""))
			{
				//Timeout Response
				if(result.equals(Stryk_Demo.Timeout))
				{
					fail_count = fail_count + 1;
					if(result_indicator_mappings.get(btnname_mapping_topanel) != null)
					{
						result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.MAGENTA);
						
					}
					else
					{
						//btnSyncMTest.setText("SYNC");
					}
					
					
				}
				//Failure response
				else if(result.equals(Stryk_Demo.Fail))
				{
					fail_count = fail_count + 1;
					if(result_indicator_mappings.get(btnname_mapping_topanel) != null)
					{
						result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.RED);
						
					}
					else
					{
						//btnSyncMTest.setText("SYNC");
					}
				
					
				}
				//Success response
				else if(result.equals(Stryk_Demo.Pass))
				{
					pass_count = pass_count + 1;
					result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.green);
					//System.out.println("Pass Count Incremented");
					
				}

				    // the following statement is used to log any messages  
				
				
				
				source = "Source : " + source;
				
			}
			else if(!reason.equals(""))
			{
				//Timeout Response
				if(result.equals(Stryk_Demo.Timeout))
				{
					fail_count = fail_count + 1;
					if(result_indicator_mappings.get(btnname_mapping_topanel) != null)
					{
						result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.MAGENTA);
					}
					else
					{
						//btnSyncMTest.setText("SYNC");
					}
					
				}
				//Failure response
				else if(result.equals(Stryk_Demo.Fail))
				{
					fail_count = fail_count + 1;
					if(result_indicator_mappings.get(btnname_mapping_topanel) != null)
					{
						result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.RED);
					}
					else
					{
						//btnSyncMTest.setText("SYNC");
					}
					
				}
				//Success response
				else if(result.equals(Stryk_Demo.Pass))
				{
					pass_count = pass_count + 1;
					result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.green);
					//System.out.println("Pass Count Incremented");
					
				}
			}
			//Error Response Received
			
			
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		
	}
	
	public static String return_command_string(String actual_string)
	{
		String return_string = "";
		if(actual_string.equals(uc1comtest))
		{
			return_string = Stryk_Demo.uC1COMTEST;
		}
		else if(actual_string.equals(uc2comtest))
		{
			return_string = Stryk_Demo.uC2COMTEST;
		}
		else if(actual_string.equals(ipctest))
		{
			return_string = Stryk_Demo.IPCTEST;
		}
		else if(actual_string.equals(uc1pwrtest))
		{
			return_string = Stryk_Demo.uC1PWRTEST;
		}
		else if(actual_string.equals(uc2pwrtest))
		{
			return_string = Stryk_Demo.uC2PWRTEST;
		}
		else if(actual_string.equals(uc1eepromtest))
		{
			return_string = Stryk_Demo.uC1EEPROMTEST;
		}
		else if(actual_string.equals(uc2eepromtest))
		{
			return_string = Stryk_Demo.uC2EEPROMTEST;
		}
		else if(actual_string.equals(uc2flashtest))
		{
			return_string = Stryk_Demo.uC2FLASHTEST;
		}
		else if(actual_string.equals(uc1camuarttest))
		{
			return_string = Stryk_Demo.uC1CAMUARTTEST;
		}
		else if(actual_string.equals(uc1debuguarttest))
		{
			return_string = Stryk_Demo.uC1DEBUGUARTTEST;
		}
		else if(actual_string.equals(uc2camuarttest))
		{
			return_string = Stryk_Demo.uC2CAMUARTTEST;
		}
		else if(actual_string.equals(uc2debuguarttest))
		{
			return_string = Stryk_Demo.uC2DEBUGUARTTEST;
		}
		else if(actual_string.equals(uc2guiuarttest))
		{
			return_string = Stryk_Demo.uC2GUIUARTTEST;
		}
		else if(actual_string.equals(uc2adduarttest))
		{
			return_string = Stryk_Demo.uC2ADDUARTTEST;
		}
		else if(actual_string.equals(lightenginefantest))
		{
			return_string = Stryk_Demo.LIGHTENGINEFANTEST;
		}
		else if(actual_string.equals(heatsinkfantest))
		{
			return_string = Stryk_Demo.HEATSINKFANTEST;
		}
		else if(actual_string.equals(irisfantest))
		{
			return_string = Stryk_Demo.IRISFANTEST;
		}
		else if(actual_string.equals(fanfailuretest))
		{
			return_string = Stryk_Demo.FANFAILURETEST;
		}
		else if(actual_string.equals(wluc1pwmmuxtest))
		{
			return_string = Stryk_Demo.WLuC1PWMMUXTEST;
		}
		else if(actual_string.equals(envuc1pwmmuxtest))
		{
			return_string = Stryk_Demo.ENVuC1PWMMUXTEST;
		}
		else if(actual_string.equals(irisuc1pwmmuxtest))
		{
			return_string = Stryk_Demo.IRISuC1PWMMUXTEST;
		}
		else if(actual_string.equals(wluc2pwmmuxtest))
		{
			return_string = Stryk_Demo.WLuC2PWMMUXTEST;
		}
		else if(actual_string.equals(envuc2pwmmuxtest))
		{
			return_string = Stryk_Demo.ENVuC2PWMMUXTEST;
		}
		else if(actual_string.equals(irisuc2pwmmuxtest))
		{
			return_string = Stryk_Demo.IRISuC2PWMMUXTEST;
		}
		else if(actual_string.equals(wlextpwmmuxtest))
		{
			return_string = Stryk_Demo.WLEXTPWMMUXTEST;
		}
		else if(actual_string.equals(envextpwmmuxtest))
		{
			return_string = Stryk_Demo.ENVEXTPWMMUXTEST;
		}
		else if(actual_string.equals(irisextwlpwmmuxtest))
		{
			return_string = Stryk_Demo.IRISEXTWLPWMMUXTEST;
		}
		else if(actual_string.equals(irisextenvpwmmuxtest))
		{
			return_string = Stryk_Demo.IRISEXTENVPWMMUXTEST;
		}
	
		
		//Red Led Tests
		else if(actual_string.equals(redledtestzero))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.REDLEDTEST_ZERO_LOAD;}
			else{return_string = Stryk_Demo.REDLEDTEST_ZERO_NOLOAD;}
		}
		else if(actual_string.equals(redledtestfifty))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.REDLEDTEST_FIFTY_LOAD;}
			else{return_string = Stryk_Demo.REDLEDTEST_FIFTY_NOLOAD;}
		}
		else if(actual_string.equals(redledtestseventyfive))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.REDLEDTEST_SEVENTY_FIVE_LOAD;}
			else{return_string = Stryk_Demo.REDLEDTEST_SEVENTY_FIVE_NOLOAD;}
		}
		else if(actual_string.equals(redledtesthundred))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.REDLEDTEST_HUNDRED_LOAD;}
			else{return_string = Stryk_Demo.REDLEDTEST_HUNDRED_NOLOAD;}
		}
		//Green LED Tests
		else if(actual_string.equals(greenledtestzero))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.GREENLEDTEST_ZERO_LOAD;}
			else{return_string = Stryk_Demo.GREENLEDTEST_ZERO_NOLOAD;}
		}
		else if(actual_string.equals(greenledtestfifty))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.GREENLEDTEST_FIFTY_LOAD;}
			else{return_string = Stryk_Demo.GREENLEDTEST_FIFTY_NOLOAD;}
		}
		else if(actual_string.equals(greenledtestseventyfive))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.GREENLEDTEST_SEVENTY_FIVE_LOAD;}
			else{return_string = Stryk_Demo.GREENLEDTEST_SEVENTY_FIVE_NOLOAD;}
		}
		else if(actual_string.equals(greenledtesthundred))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.GREENLEDTEST_HUNDRED_LOAD;}
			else{return_string = Stryk_Demo.GREENLEDTEST_HUNDRED_NOLOAD;}
		}
		else if(actual_string.equals(blueledtestzero))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.BLUELEDTEST_ZERO_LOAD;}
			else{return_string = Stryk_Demo.BLUELEDTEST_ZERO_NOLOAD;}
		}
		else if(actual_string.equals(blueledtestfifty))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.BLUELEDTEST_FIFTY_LOAD;}
			else{return_string = Stryk_Demo.BLUELEDTEST_FIFTY_NOLOAD;}
		}
		else if(actual_string.equals(blueledtestseventyfive))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.BLUELEDTEST_SEVENTY_FIVE_LOAD;}
			else{return_string = Stryk_Demo.BLUELEDTEST_SEVENTY_FIVE_NOLOAD;}
		}
		else if(actual_string.equals(blueledtesthundred))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.BLUELEDTEST_HUNDRED_LOAD;}
			else{return_string = Stryk_Demo.BLUELEDTEST_HUNDRED_NOLOAD;}
		}
		else if(actual_string.equals(laserledtestzero))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.ENVLEDTEST_ZERO_LOAD;}
			else{return_string = Stryk_Demo.ENVLEDTEST_ZERO_NOLOAD;}
		}
		else if(actual_string.equals(laserledtestfifty))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.ENVLEDTEST_FIFTY_LOAD;}
			else{return_string = Stryk_Demo.ENVLEDTEST_FIFTY_NOLOAD;}
		}
		else if(actual_string.equals(laserledtestseventyfive))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.ENVLEDTEST_SEVENTY_FIVE_LOAD;}
			else{return_string = Stryk_Demo.ENVLEDTEST_SEVENTY_FIVE_NOLOAD;}
		}
		else if(actual_string.equals(laserledtesthundred))
		{
			if(chckbxLightEngine.isSelected() == true){return_string = Stryk_Demo.ENVLEDTEST_HUNDRED_LOAD;}
			else{return_string = Stryk_Demo.ENVLEDTEST_HUNDRED_NOLOAD;}
		}
		else if(actual_string.equals(iris1ledtestzero))
		{
			if(chckbxIrisModule.isSelected() == true){return_string = Stryk_Demo.IRIS1LEDTEST_ZERO_LOAD;}
			else{return_string = Stryk_Demo.IRIS1LEDTEST_ZERO_NOLOAD;}
		}
		else if(actual_string.equals(iris2ledtestzero))
		{
			if(chckbxIrisModule.isSelected() == true){return_string = Stryk_Demo.IRIS2LEDTEST_ZERO_LOAD;}
			else{return_string = Stryk_Demo.IRIS2LEDTEST_ZERO_NOLOAD;}
		}
		else if(actual_string.equals(iris1ledtestfifty))
		{
			if(chckbxIrisModule.isSelected() == true){return_string = Stryk_Demo.IRIS1LEDTEST_FIFTY_LOAD;}
			else{return_string = Stryk_Demo.IRIS1LEDTEST_FIFTY_NOLOAD;}
		}
		else if(actual_string.equals(iris2ledtestfifty))
		{
			if(chckbxIrisModule.isSelected() == true){return_string = Stryk_Demo.IRIS2LEDTEST_FIFTY_LOAD;}
			else{return_string = Stryk_Demo.IRIS2LEDTEST_FIFTY_NOLOAD;}
		}
		else if(actual_string.equals(iris1ledtestseventyfive))
		{
			if(chckbxIrisModule.isSelected() == true){return_string = Stryk_Demo.IRIS1LEDTEST_SEVENTY_FIVE_LOAD;}
			else{return_string = Stryk_Demo.IRIS1LEDTEST_SEVENTY_FIVE_NOLOAD;}
		}
		else if(actual_string.equals(iris2ledtestseventyfive))
		{
			if(chckbxIrisModule.isSelected() == true){return_string = Stryk_Demo.IRIS2LEDTEST_SEVENTY_FIVE_LOAD;}
			else{return_string = Stryk_Demo.IRIS2LEDTEST_SEVENTY_FIVE_NOLOAD;}
		}
		else if(actual_string.equals(iris1ledtesthundred))
		{
			if(chckbxIrisModule.isSelected() == true){return_string = Stryk_Demo.IRIS1LEDTEST_HUNDRED_LOAD;}
			else{return_string = Stryk_Demo.IRIS1LEDTEST_HUNDRED_NOLOAD;}
		}
		else if(actual_string.equals(iris2ledtesthundred))
		{
			if(chckbxIrisModule.isSelected() == true){return_string = Stryk_Demo.IRIS2LEDTEST_HUNDRED_LOAD;}
			else{return_string = Stryk_Demo.IRIS2LEDTEST_HUNDRED_NOLOAD;}
		}
		else if(actual_string.equals(iris1pdtestzero))
		{
			return_string = Stryk_Demo.PDTEST_IRIS1_ZERO;
		}
		else if(actual_string.equals(iris1pdtesthundred))
		{
			return_string = Stryk_Demo.PDTEST_IRIS1_HUNDRED;
		}
		else if(actual_string.equals(iris2pdtestzero))
		{
			return_string = Stryk_Demo.PDTEST_IRIS2_ZERO;
		}
		else if(actual_string.equals(iris2pdtesthundred))
		{
			return_string = Stryk_Demo.PDTEST_IRIS2_HUNDRED;
		}
		else if(actual_string.equals(envpdtestzero))
		{
			return_string = Stryk_Demo.PDTEST_ENV_ZERO;
		}
		else if(actual_string.equals(envpdtesthundred))
		{
			return_string = Stryk_Demo.PDTEST_ENV_HUNDRED;
		}
		else if(actual_string.equals(iris1fiberdetecttest))
		{
			if(chckbxOpticFiber.isSelected() == true)
			{
				return_string = Stryk_Demo.FIBER1DETECTONTEST;
			}
			else
			{
				return_string = Stryk_Demo.FIBER1DETECTOFFTEST;
			}
		}
		else if(actual_string.equals(iris2fiberdetecttest))
		{
			if(chckbxOpticFiber.isSelected() == true)
			{
				return_string = Stryk_Demo.FIBER2DETECTONTEST;
			}
			else
			{
				return_string = Stryk_Demo.FIBER2DETECTOFFTEST;
			}
		}
		else if(actual_string.equals(beamsensortest))
		{
			if(chckbxOpticFiber.isSelected() == true)
			{
				return_string = Stryk_Demo.BEAMSENSORONTEST;
			}
			else
			{
				return_string = Stryk_Demo.BEAMSENSOROFFTEST;
			}
		}
		else if(actual_string.equals(essttest))
		{
			if(chckbxEsst.isSelected() == true)
			{
				return_string = Stryk_Demo.ESSTONTEST;
			}
			else
			{
				return_string = Stryk_Demo.ESSTOFFTEST;
			}
		}
		else if(actual_string.equals(assttest))
		{
			if(chckbxAsst.isSelected() == true)
			{
				return_string = Stryk_Demo.ASSTONTEST;
			}
			else
			{
				return_string = Stryk_Demo.ASSTOFFTEST;
			}
		}
		else if(actual_string.equals(frontboardtest))
		{
			Stryk_Demo.front_board_pwr_flag = true;
			return_string = Stryk_Demo.FRONTBAORDPOWERENONTEST;
			
		}
		else if(actual_string.equals(irispgoodontest))
		{
			
			return_string = Stryk_Demo.IRISPGOODONTEST;
			
		}
		else if(actual_string.equals(irispgoodofftest))
		{
			return_string = Stryk_Demo.IRISPGOODOFFTEST;
		}
		else if(actual_string.equals(colorsensor1test))
		{
			return_string = Stryk_Demo.COLORSENSOR1TEST;
		}
		else if(actual_string.equals(colorsensor2test))
		{
			return_string = Stryk_Demo.COLORSENSOR2TEST;
		}
		
		
		
		return return_string;
	}
	
	public static String get_source(String source_no)
	{
		if(source_no.equals("1"))
		{
			source_no = "uC1";
		}
		else if(source_no.equals("2"))
		{
			source_no = "uC2";
		}
		else if(source_no.equals("3"))
		{
			source_no = "IPC";
		}
		return source_no;
		
	}
	
	//############################################Function Description###########################
	//###########################################################################################
	public static String get_error_source(String source_no)
	{
		String error_string = "";
		if(source_no.equals("0"))
		{
			
		}
		else if(source_no.equals("1"))
		{
			error_string = "FRAMING ERROR";
		}
		else if(source_no.equals("2"))
		{
			error_string = "STUFFING ERROR";
		}
		else if(source_no.equals("3"))
		{
			error_string = "CHECKSUM ERROR";
		}
		else if(source_no.equals("4"))
		{
			error_string = "UNKNOWN_COMMAND_ERROR";
		}
		else if(source_no.equals("5"))
		{
			error_string = "PAYLOADLENGTH ERROR";
		}
		else if(source_no.equals("6"))
		{
			error_string = "IPC MUTEX TIMEOUT ERROR";
		}
		else if(source_no.equals("7"))
		{
			error_string = "UNKNOWN MESSAGE TYPE ERROR";
		}
		else if(source_no.equals("8"))
		{
			error_string = "UNKNOWN STATE ERROR";
		}
		else if(source_no.equals("9"))
		{
			error_string = "uC1 TIMEOUT ERROR";
		}
		else if(source_no.equals("10"))
		{
			error_string = "uC1 IPC ERROR";
		}
		else if(source_no.equals("11"))
		{
			error_string = "CMD EXECUTION ERROR";
		}
		else if(source_no.equals("177"))
		{
			error_string = "EEPROM WRITE FAIL";
		}
		else if(source_no.equals("178"))
		{
			error_string = "EEPROM READ FAIL";
		}
		else if(source_no.equals("179"))
		{
			error_string = "EEPROM COMPARE FAIL";
		}
		else if(source_no.equals("180"))
		{
			error_string = "FLASH INIT FAIL";
		}
		else if(source_no.equals("181"))
		{
			error_string = "FLASH WRITE FAIL";
		}
		else if(source_no.equals("182"))
		{
			error_string = "FLASH READ FAIL";
		}
		else if(source_no.equals("183"))
		{
			error_string = "FLASH COMPARE FAIL";
		}
		else if(source_no.equals("184"))
		{
			error_string = "UART TXRX FAIL";
		}
		else if(source_no.equals("185"))
		{
			error_string = "UART COMPARE FAIL";
		}
		else if(source_no.equals("186"))
		{
			error_string = "UART NOT DEFINED";
		}
		else if(source_no.equals("187"))
		{
			error_string = "FAN TACH COUNT READ MISMATCH";
		}
		else if(source_no.equals("188"))
		{
			error_string = "PWM EDGE COUNT READ MISMATCH";
		}
		else if(source_no.equals("189"))
		{
			error_string = "LED VOLTAGE VALUE MISMATCH";
		}
		else if(source_no.equals("190"))
		{
			error_string = "LED CURRENT VALUE MISMATCH";
		}
		return error_string;
		
	}
	
	//Function to set bg - color of the cell individually
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		       
		// TODO Auto-generated method stub
		
	    
	}
	///////////Function for sending the Manufacturing Test Commands to the board
	public static void perform_test(String Query_String)
	{
	try {
			System.out.println("Performing Required Test");
			//progressBar.setValue(pbval);
			//System.out.println(action_items.get(Current_Query_Number));
			
			
			
			if(Stryk_Demo.serialport != null)
			{
				if(!busyflag)
				{
					//logs_logger.info("Inside PerformTest Function");
					//System.out.print("Query String : ");
					//System.out.println(Query_String);
					MFTest_Flag = true;
					logs_logger.info("Opening Serial Port...");
					Stryk_Demo.serialport.openPort();
					if(Stryk_Demo.serialport.isOpen() == true)
					{
						logs_logger.info("SerialPort Opened");
						logs_logger.info("Clearing Serial buff");
						byte[] temp = null ;
						byte[] byte_1 = "A".getBytes();
						//Stryk_Demo.serialport.writeBytes(byte_1, byte_1.length);
						int bytes_available = Stryk_Demo.serialport.bytesAvailable();
						temp = new byte[bytes_available];
						//System.out.println("Total Bytes available at the buffer inside performtest : " + bytes_available);
						if(bytes_available > 0)
						{
							int d = Stryk_Demo.serialport.readBytes(temp, bytes_available);
						}
						logs_logger.info("Ser-Port buff Cleared!!");
						//System.out.println("Test Print");
						logs_logger.info("Clearing rx buffer");
					    Stryk_Demo.full_buffer.reset();
					    try {
							Stryk_Demo.full_buffer.flush();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					    logs_logger.info("rx buffer Cleared!");
						Stryk_Demo.send_data_in_bg(Query_String, null, true, Query_String.getBytes());
						//initialize_logger();
						//logger.info("Command Sent : " + Query_String);
						textArea.append("Command Sent : " + Query_String + "\r\n");
						logs_logger.info("Command Sent" + "\t\t" + Query_String);
						
						//#LOG
					}
					else
					{
						JOptionPane.showMessageDialog(frmStrykerManufacturingTests, "Could Not Open SerialPort");
						if(!MFTest_Flag)
						{
							btnStartButton.setEnabled(true);
						}
						else
						{
							//btnSyncMTest.setEnabled(true);
						}
						
					}
				}
				while(busyflag == true)
				{
					synchronized(lockObject)
					{
						lockObject.wait();
					}
				}
				Stryk_Demo.serialport.closePort();
				logs_logger.info("Serial Port Closed");
				logs_logger.info(ArrowDelimitter);
				
			}
			else
			{
				JOptionPane.showMessageDialog(frmStrykerManufacturingTests, "Invalid SerialPort!!");
				btnStartButton.setEnabled(true);
			}
	
			
			
			
		pbval = 0;
	
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
			JOptionPane.showMessageDialog(frmStrykerManufacturingTests, "Some Error During Normal Execution State!!", "Error", JOptionPane.ERROR_MESSAGE);
			btnStartButton.setEnabled(true);
			
		}
	}
	public static String perform_test_string(String str)
	{
		
		return str;
		
	}
	
	//Background Worker for handling Automated Tests
	public void Initialize_auto_bg_worker()
	{ 
		autotest_bg_worker = new SwingWorker<ArrayList<String>, String>() {
		@Override
		protected ArrayList<String> doInBackground() throws Exception 
		{
			try
			{
				
				publish("ACTIONITEMS");
				String Query_String = "";
				
				if(togglebuttons.size() != 0)
				{
					for(int j = 0;j<togglebuttons.size();j++)
					{
						
						btnname_mapping_topanel = "";
						JToggleButton jtgbtn = new JToggleButton();
						jtgbtn = (JToggleButton) togglebuttons.get(j);
						btnname_mapping_topanel = jtgbtn.getName();
						for (HashMap.Entry<String, String> entry : btn_name_mapping.entrySet()) {
						    if(jtgbtn.getName().equals(btn_name_mapping.get(entry.getKey())))
						    {
						    	
								Query_String = entry.getKey();
						    	check_string = Query_String;
						    	logs_logger.info("Test\t\t\t" + Query_String);
						    	report_logger.info("\r\n\nTest\t\t\t" + Query_String);
						    	
						    	Query_String = return_command_string(Query_String);
						    	
						    	logs_logger.info("Command String\t:\t" + Query_String);
						    	if(serialport != null)
								{
						    		while(busyflag)
						    		{
						    			synchronized(lockObject)
						    			{
						    				lockObject.wait();
						    			}
						    		}
									if(!busyflag)
									{
										System.out.print("Query String : ");
										System.out.println(Query_String);
										
										serialport.openPort();
										if(serialport.isOpen() == true)
										{
											
											byte[] temp = null ;
											int bytes_available = serialport.bytesAvailable();
											System.out.println("Total Bytes available at the buffer inside performtest : " + bytes_available);
											if(bytes_available > 0)
											{
												temp = new byte[bytes_available];
												int d = serialport.readBytes(temp, bytes_available);
											}
											
										    
										    try {
										    	full_buffer.reset();
												full_buffer.flush();
											} catch (IOException e1) {
												// TODO Auto-generated catch block
												e1.printStackTrace();
											}
										    
										    	System.out.println("Starting Test...");
										    	setlistener_mft(serialport);
										    	System.out.println("Autotest : Data Listener Set!!");
										    	serialport.writeBytes(Query_String.getBytes(), Query_String.length());
										    	writeflag  = true;
										    	synchronized(lockObject)
										    	{
										    		busyflag = true;
										    		lockObject.notifyAll();
										    	}
										    	
										    	publish("Command Sent : " + Query_String + "\r\n");
										    	
										    	
										    	final Runnable timeout_task_performer = new Runnable()
												{
													@Override
													public void run() 
													{
														synchronized(lockObject)
														{
															timeoutflag = 1;
															busyflag = false;
															lockObject.notifyAll();
														}
														
													}
													
												};
												final Runnable update_GUI_state = new Runnable() 
												{
													@Override
													public void run() 
													{
														synchronized(lockObject)
														{
															
															while (timeoutflag == 0 && notify_flag == 0)
															{
																try 
																{
																	synchronized(lockObject)
																	{
																		lockObject.wait(10);
																	}
																} 
																catch (Exception e)
																{
																	e.printStackTrace();
																}
																

															}
															
															Stryk_Demo.print("exited while LOPP");
															runfuture.cancel(true);
														}
														// While ends here
														
														//No Data Received for sending command to device
														if (timeoutflag == 1 && notify_flag == 0) 
														{
															serialport.openPort();
															if(serialport.isOpen() == true)
															{
																byte[] temp = null;
																int bytes_available = serialport.bytesAvailable();
																
																  
																System.out.println("Total Bytes available at timeout : " + bytes_available);
																if(bytes_available > 0)
																{
																	temp = new byte[bytes_available];
																	serialport.readBytes(temp, bytes_available);
																}
																
																serialport.removeDataListener();
																runfuture.cancel(true);
																
															}
															timeoutflag = 0;
															System.out.println("Response Timedout. No data received in the Serial Port");
															
																
															
															publish("RUN");
															UpdateTestResults(Stryk_Demo.Timeout, "", "", "");
															logs_logger.severe("Response Received\tResponse Timedout");
															report_logger.severe("Result\t\t\tTimeout\r\n\n");
															report_logger.severe(ArrowDelimitter);
															report_logger.severe(ArrowDelimitter);
														
															textArea.update(textArea.getGraphics());
															
															//textArea.append("Response Timedout for " + Cmd + " command!!\r\n\n");
															
														}
														//Valid Response received for sending command to the device
														else if (notify_flag == 1 && timeoutflag == 0) 
														{
															finish_button_flag = false;
															System.out.println("Error Free Data Received!!");
															notify_flag = 0;
															
															@SuppressWarnings("unused")
															bytedefinitions b = new bytedefinitions();
															//System.out.println("Valid Response in UART");
															
															
															runfuture.cancel(true);
															
														} 
														
														
														else
														{
															System.out.println("Peculiar");
														}
													}
												};

												
												
												//Check WriteFlag whether the data is sent to serialport successfully
												if (writeflag == true)
												{
													System.out.println("Write Flag Set");
													//Sets the timeout_task_performer to start exactly after serial_read_cmd_timeout milliseconds
													
													System.out.println("Triggering Timer...");
													runfuture = scheduler.schedule(timeout_task_performer, serial_read_cmd_timeout, TimeUnit.MILLISECONDS);
													System.out.println("Timer Triggered!!");
													
													//publish("Waiting For Response!!\r\n");
													while(busyflag == true)
													{
														synchronized(lockObject)
														{
															lockObject.wait(10);
														}
													}
														
													
													
													
													
													Thread appThread = new Thread() {
														public void run() {
															try {
																//Entry point of the function
																scheduler.schedule(update_GUI_state, 0, TimeUnit.MILLISECONDS);
																//SwingUtilities.invokeLater(update_GUI_state);
															} catch (Exception ex) {
																System.out.println("Some Error");
																ex.printStackTrace();
															}
														}
													};
														appThread.run();
												} 
												else
												{
													System.out.println("WriteFlag is False");
												}
										    	
										    
									    	
										    
										    
												
												
										    
										    
										   
											
									} 
									else 
									{
										System.out.println("Could not open Serial port. Exiting Now!!");
								    	publish("Could not send " + Query_String + " command \r\n");
								    	publish("Access to COM Port Denied!!. Port is in Access by Another Application!!\r\n\n");
								    	togglebuttons.clear();
								    	break;
										
									}
								}
								else
								{
									publish("BUSY");
								}
							}
							else
							{
								publish("INVALIDPORT");
							}
						    	
						    	
						    }
						    //System.out.println(entry.getValue());
						}
						
					}
				}
				else
				{
					publish("NO_TEST");
					
				}
			
				
				
				
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return null;
			
			
			 
			
			

		}
		protected void process(List<String> chunks)
		{
			for (String str : chunks)
			{
			if(str.equals("ACTIONITEMS"))
			{
				btnStartButton.setText("RUNNING...");
				btnStartButton.setEnabled(false);
				//btnBack.setEnabled(false);
				btnStop.setEnabled(true);
				//btnBrowseLogFile.setEnabled(false);
				//btnBrowseReportFile.setEnabled(false);
				
				chckbxGroupTests.setEnabled(false);
			}
			else if(str.equals("BUSY"))
				{
					textArea.append("Could Not Open SerialPort");
					if(!MFTest_Flag)
					{
						btnStartButton.setText("RUN");
						btnStartButton.setEnabled(true);
					}
					else
					{
						//btnSyncMTest.setEnabled(true);
					}
				}
				else if(str.equals("INVALIDPORT"))
				{
					textArea.append("Invalid SerialPort!!");
					btnStartButton.setText("RUN");
					btnStartButton.setEnabled(true);
				}
				else if(str.equals("EXCEPTION"))
				{
					textArea.append("Some Exception Occurred while running !!");
					btnStartButton.setText("RUN");
					btnStartButton.setEnabled(true);
				}
				else if(str.equals("ACCESS DENIED"))
				{
					btnStartButton.setText("RUN");
					btnStartButton.setEnabled(true);
				}
				else if(str.equals("NO_TEST"))
				{
					JOptionPane.showMessageDialog(frmStrykerManufacturingTests, "No Tests to be run", "Information", JOptionPane.INFORMATION_MESSAGE);
				}
				else if(str.equals("RUN"))
				{
					textArea.append("Response Received : Response Timedout!!\r\n");
					btnStartButton.setEnabled(true);
					btnStartButton.setText("RUN");
				}
				else
				{
					textArea.append(str);
				}
				
			}
		}

		public void done() 
		{
			total_test_count = pass_count + fail_count;
			
			
			if(fail_count == 0)
			{
				if(pass_count > 0 )
				{
					pass_count = 0;
					System.out.println("PASSSE");
					panel_4.setBackground(Color.GREEN);
					
					lblTestStatus.setText("PASS");
				}
			}
			else if(fail_count > 0)
			{
				fail_count = 0;
				System.out.println("FAIL");
				panel_4.setBackground(Color.RED);
				lblTestStatus.setText("FAILED");
			}
			total_test_count = 0;
			pass_count = 0;
			fail_count = 0;
			System.out.println("While Loop Breaks here");
			logs_logger.info(ArrowDelimitter);
			report_logger.info(ArrowDelimitter);
			report_logger.info("#########################  END OF REPORT  ##################################");
			report_logger.removeHandler(fh);
			
			btnStartButton.setText("RUN");
			btnStartButton.setEnabled(true);
			//btnBack.setEnabled(true);
			btnStop.setEnabled(false);
			
			
			chckbxGroupTests.setEnabled(true);
			update_value = 0;
			textArea.update(textArea.getGraphics());
		}

	};
	}
	public static void setlistener_mft(final SerialPort serialport) {
		//print("Serial Port Listener Set");
		if (serialport != null) {

			serialport.addDataListener(new SerialPortDataListener() {

				

				@Override
				public int getListeningEvents() {
					// System.out.println("Getting the list of events");
		return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
	}

	@Override
	public void serialEvent(SerialPortEvent event) {

		if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE) {
			textArea.append("No response\r\n\n");
		return;
	}

	try {
		//System.out.println("AA");
	byte[] newData = new byte[serialport.bytesAvailable()];
	//System.out.println("A");
	numRead = serialport.readBytes(newData, newData.length);
	//System.out.println("B");
	total_bytes = total_bytes + numRead;
	full_buffer.write(newData, 0, numRead);
	System.out.println("Something dfdf data arrived in the serial port");
	
	if(future!= null)
	{
		future.cancel(true);
	}

	
	future = scheduler.schedule(uart_recv_timeout_task ,serial_read_byte_timeout, TimeUnit.MILLISECONDS);

	

	// #uart_response_timer.start();
	} catch (Exception ex) {
		ex.printStackTrace();

		
					return;
				}

			}

		});

	} else {
		System.out.println("No Serial ports found in the system");
		}
	}
	final static Runnable uart_recv_timeout_task = new Runnable() {
		
		@Override
		public void run()
		
		{
			
			SwingWorker recv_worker = new SwingWorker<List<String>, String>()
			{
				
				
				
				@Override
				protected List<String> doInBackground() throws Exception {
					if (full_buffer == null)
					{
						
						System.out.println("No data in the response!!");
						busyflag = false;
						serialport.removeDataListener(); // Removes data listener associated with serial port Object
						total_bytes = 0;
						
					} 
					else 
					{
						
						
						//print("Some Response Atleast!!");
						//print(Arrays.toString(full_buffer.toByteArray()));
						System.out.println("Full Buffer size before calling function :" + full_buffer.size());
						byte[] recv_buff = full_buffer.toByteArray();
						
						try {
							full_buffer.flush();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println("Receive Buffer size before calling function :" + recv_buff.length );
						
						serialport.removeDataListener();
						String result = "";
						result = new String(recv_buff,"UTF-8");
						System.out.println("Response to be processed : " + result);
						System.out.println("Inside the function");
							logs_logger.severe("Response Received\t"+ result);
							
							full_buffer.reset();
							
							publish("Response Received : " + result + "\r\n");
							System.out.println("Receive Buffer in MTEST START: " + Arrays.toString(recv_buff));
							//System.out.println("Manufacturing Test Flag Set");
							MFTest_Flag = false;
							try 
							{
								String Resp = new String(recv_buff,"UTF-8");
								
								recv_buff = null;
								System.out.println("Response Received : " + Resp + " Of Length" + Resp.length());
								
								if(Resp.contains(RESPONSE_OK))
								{
									
									try
									{
										String resp1 = Resp.replace("{", "");
										resp1 = resp1.replace("}", "");
										String[] resp = resp1.split(",");
										logs_logger.severe("Response OK Received");
										//System.out.println(Arrays.deepToString(resp));
										//System.out.println("Response OK Received!!");
										recv_buff = null;
										Resp = "";
										
										
										if(Current_Query_Number == 0)
										{
											
											System.out.println("Actual Array : " + Arrays.toString(resp));
											String Ver_No = resp[2];
											publish("VER_NO," + Ver_No);
											Current_Query_Number = 1;
											
											publish("SUB_PANEL");
											synchronized(lockObject)
											{
												notify_flag = 1;
												busyflag = false;
												lockObject.notifyAll();
											}
											
										}
										else
										{
											
											
											String src = get_source(resp[1]);
											
											//UpdateTestResults(.Current_Query,Pass,"",src,"");
										
											logs_logger.severe("Result\t\t\tPass");
											report_logger.severe("Result\t\t\tPass" );
											//LIGHT ENGINE FAN TEST
											if(lightenginefantest.equals(check_string))
											{
												String Fan_Count = resp[2];
												report_logger.severe("Expected Tach Count -->  : " + Expected_LightEngine_FanTachCount+ "\t(+/- 25%)");
												report_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
												
												logs_logger.severe("Expected Tach Count -->  : " + Expected_LightEngine_FanTachCount+ "\t(+/- 25%)");
												logs_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
												check_string = "";
											}//HEAT SINK FAN TEST
											else if(heatsinkfantest.equals(check_string))
											{
												String Fan_Count = resp[2];
												report_logger.severe("Expected Tach Count -->  : " + Expected_HeatSink_FanTachCount+ "\t(+/- 25%)");
												report_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
												
												logs_logger.severe("Expected Tach Count -->  : " + Expected_HeatSink_FanTachCount+ "\t(+/- 25%)");
												logs_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
												check_string = "";
											}
											//IRIS FAN TEST
											else if(irisfantest.equals(check_string))
											{
												String Fan_Count = resp[2];
												report_logger.severe("Expected Tach Count -->  : " + Expected_Iris_FanTachCount+ "\t(+/- 25%)");
												report_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
												
												logs_logger.severe("Expected Tach Count -->  : " + Expected_Iris_FanTachCount+ "\t(+/- 25%)");
												logs_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
												check_string = "";
											}
											
											//LED TEST RESULT : LED ZERO PERCENT
											else if(redledtestzero.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_RedLed_Current_zeropercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_RedLed_Current_zeropercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												check_string = "";
											}
											else if(greenledtestzero.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_GreenLed_Current_zeropercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_GreenLed_Current_zeropercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												check_string = "";
											}
											else if(blueledtestzero.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_BlueLed_Current_zeropercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_BlueLed_Current_zeropercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												check_string = "";
											}
											else if(laserledtestzero.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_LaserLed_Current_zeropercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_LaserLed_Current_zeropercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												check_string = "";
											}
											else if(iris1ledtestzero.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_zeropercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_zeropercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												check_string = "";
											}
											else if(iris2ledtestzero.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_zeropercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_zeropercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												check_string = "";
											}
											
											
											//LED TEST RESULT : LED FIFTY PERCENT
							
											else if(redledtestfifty.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_RedLed_Current_fiftypercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_RedLed_Current_fiftypercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												check_string = "";
											}
											else if(greenledtestfifty.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_GreenLed_Current_fiftypercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_GreenLed_Current_fiftypercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												check_string = "";
											}
											else if(blueledtestfifty.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_BlueLed_Current_fiftypercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_BlueLed_Current_fiftypercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												check_string = "";
											}
											else if(laserledtestfifty.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_LaserLed_Current_fiftypercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_LaserLed_Current_fiftypercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												check_string = "";
											}
											else if(iris1ledtestfifty.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_fiftypercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_fiftypercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												check_string = "";
											}
											else if(iris2ledtestfifty.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_fiftypercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_fiftypercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												check_string = "";
											}
											
											//LED TEST RESULT : LED SEVENTY FIVE PERCENT
											
											else if(redledtestseventyfive.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												check_string = "";
											}
											else if(greenledtestseventyfive.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												check_string = "";
											}
											else if(blueledtestseventyfive.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												check_string = "";
											}
											else if(laserledtestseventyfive.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												check_string = "";
											}
											else if(iris1ledtestseventyfive.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												check_string = "";
											}
											else if(iris2ledtestseventyfive.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												check_string = "";
											}
											
											//LED TEST RESULT : LED HUNDRED PERCENT
											
											else if(redledtesthundred.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_RedLed_Current_hundredpercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_RedLed_Current_hundredpercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												check_string = "";
											}
											else if(greenledtesthundred.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_GreenLed_Current_hundredpercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_GreenLed_Current_hundredpercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												check_string = "";
											}
											else if(blueledtesthundred.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_BlueLed_Current_hundredpercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_BlueLed_Current_hundredpercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
											}
											else if(laserledtesthundred.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_LaserLed_Current_hundredpercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_LaserLed_Current_hundredpercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												check_string = "";
											}
											else if(iris1ledtesthundred.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_hundredpercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_hundredpercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												check_string = "";
											}
											else if(iris2ledtesthundred.equals(check_string))
											{
												String LedVoltage = resp[2];
												String LedCurrent = resp[3];
												if(chckbxLightEngine.isSelected())
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_hundredpercent + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_hundredpercent + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												else
												{
													report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
													
													logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
													logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												}
												
												
												check_string = "";
											}
											
											//PDTESTS : IRIS1ZERO and HUNDRED
											else if(iris1pdtestzero.equals(check_string))
											{
												String PhotoVoltage = resp[2];
												report_logger.severe("Expected Value --> \tVoltage : " + Exp_iris1pdvoltage_zero+" V\t(+/- 25%)");
												report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
												
												logs_logger.severe("Expected Value --> \tVoltage : " + Exp_iris1pdvoltage_zero+" V\t(+/- 25%)");
												logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
											}
											else if(iris1pdtesthundred.equals(check_string))
											{
												String PhotoVoltage = resp[2];
												report_logger.severe("Expected Value --> \tVoltage : " + Exp_iris1pdvoltage_hundred+" V\t(+/- 25%)");
												report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
												
												logs_logger.severe("Expected Value --> \tVoltage : " + Exp_iris1pdvoltage_hundred+" V\t(+/- 25%)");
												logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
											}
											
											//PDTESTS : IRIS2 ZERO AND HUNDRED
											
											else if(iris2pdtestzero.equals(check_string))
											{
												String PhotoVoltage = resp[2];
												report_logger.severe("Expected Value --> \tVoltage : " + Exp_iris2pdvoltage_zero+" V\t(+/- 25%)");
												report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
												
												logs_logger.severe("Expected Value --> \tVoltage : " + Exp_iris2pdvoltage_zero+" V\t(+/- 25%)");
												logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
											}
											else if(iris2pdtesthundred.equals(check_string))
											{
												String PhotoVoltage = resp[2];
												report_logger.severe("Expected Value --> \tVoltage : " + Exp_iris2pdvoltage_hundred+" V\t(+/- 25%)");
												report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
												
												logs_logger.severe("Expected Value --> \tVoltage : " + Exp_iris2pdvoltage_hundred+" V\t(+/- 25%)");
												logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
											}
//											PDTESTS : ENV ZERO AND HUNDRED
											
											else if(envpdtestzero.equals(check_string))
											{
												String PhotoVoltage = resp[2];
												report_logger.severe("Expected Value --> \tVoltage : " + Exp_envpdvoltage_zero+" V\t(+/- 25%)");
												report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
												
												logs_logger.severe("Expected Value --> \tVoltage : " + Exp_envpdvoltage_zero+" V\t(+/- 25%)");
												logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
											}
											else if(envpdtesthundred.equals(check_string))
											{
												String PhotoVoltage = resp[2];
												report_logger.severe("Expected Value --> \tVoltage : " + Exp_envpdvoltage_hundred+" V\t(+/- 25%)");
												report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
												
												logs_logger.severe("Expected Value --> \tVoltage : " + Exp_envpdvoltage_hundred+" V\t(+/- 25%)");
												logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
											}
//											CS TESTS : COLORSENSOR 1
											
											else if(colorsensor1test.equals(check_string))
											{
												String ColorsensorVal_1 = resp[2];
												String ColorsensorVal_2 = resp[3];
												String ColorsensorVal_3 = resp[4];
												String ColorsensorVal_4 = resp[5];
												report_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
												report_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4 + "\r\n\n");
												
												logs_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
												logs_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4 + "\r\n\n");
											}
											else if(colorsensor2test.equals(check_string))
											{
												String ColorsensorVal_1 = resp[2];
												String ColorsensorVal_2 = resp[3];
												String ColorsensorVal_3 = resp[4];
												String ColorsensorVal_4 = resp[5];
												report_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
												report_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4+ "\r\n\n");
												
												logs_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
												logs_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4+ "\r\n\n");
											}
											
											//PWM MUXTESTS 
											else if(wluc1pwmmuxtest.equals(check_string))
											{
												String MuxInterrupt_Count = resp[2];
												report_logger.severe("Expected Value --> \t" + Exp_wluc1pwmmuxtest_intcount + " (+/- 3)");
												report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
												
												logs_logger.severe("Expected Value --> \t" + Exp_wluc1pwmmuxtest_intcount+ " (+/- 3)");
												logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
											}
											else if(envuc1pwmmuxtest.equals(check_string))
											{
												String MuxInterrupt_Count = resp[2];
												report_logger.severe("Expected Value --> \t" + Exp_envuc1pwmmuxtest_intcount+ " (+/- 3)");
												report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
												
												logs_logger.severe("Expected Value --> \t" + Exp_envuc1pwmmuxtest_intcount+ " (+/- 3)");
												logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
											}
											else if(irisuc1pwmmuxtest.equals(check_string))
											{
												String MuxInterrupt_Count = resp[2];
												report_logger.severe("Expected Value --> \t" + Exp_irisuc1pwmmuxtest_intcount+ " (+/- 3)");
												report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
												
												logs_logger.severe("Expected Value --> \t" + Exp_irisuc1pwmmuxtest_intcount+ " (+/- 3)");
												logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
											}
											else if(wluc2pwmmuxtest.equals(check_string))
											{
												String MuxInterrupt_Count = resp[2];
												report_logger.severe("Expected Value --> \t" + Exp_wluc2pwmmuxtest_intcount+ " (+/- 3)");
												report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
												
												logs_logger.severe("Expected Value --> \t" + Exp_wluc2pwmmuxtest_intcount+ " (+/- 3)");
												logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
											}
											else if(envuc2pwmmuxtest.equals(check_string))
											{
												String MuxInterrupt_Count = resp[2];
												report_logger.severe("Expected Value --> \t" + Exp_envuc2pwmmuxtest_intcount+ " (+/- 3)");
												report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
												
												logs_logger.severe("Expected Value --> \t" + Exp_envuc2pwmmuxtest_intcount+ " (+/- 3)");
												logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
											}
											else if(irisuc2pwmmuxtest.equals(check_string))
											{
												String MuxInterrupt_Count = resp[2];
												report_logger.severe("Expected Value --> \t" + Exp_irisuc2pwmmuxtest_intcount+ " (+/- 3)");
												report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
												
												logs_logger.severe("Expected Value --> \t" + Exp_irisuc2pwmmuxtest_intcount+ " (+/- 3)");
												logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
											}
											else if(wlextpwmmuxtest.equals(check_string))
											{
												String MuxInterrupt_Count = resp[2];
												report_logger.severe("Expected Value --> \t" + Exp_wlextpwmmuxtest_intcount+ " (+/- 3)");
												report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
												
												logs_logger.severe("Expected Value --> \t" + Exp_wlextpwmmuxtest_intcount+ " (+/- 3)");
												logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
											}
											else if(envextpwmmuxtest.equals(check_string))
											{
												String MuxInterrupt_Count = resp[2];
												report_logger.severe("Expected Value --> \t" + Exp_envextpwmmuxtest_intcount+ " (+/- 3)");
												report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
												
												logs_logger.severe("Expected Value --> \t" + Exp_envextpwmmuxtest_intcount+ " (+/- 3)");
												logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
											}
											else if(irisextwlpwmmuxtest.equals(check_string))
											{
												String MuxInterrupt_Count = resp[2];
												report_logger.severe("Expected Value --> \t" + Exp_irisextwlpwmmuxtest_intcount+ " (+/- 3)");
												report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+ "\r\n\n" );
												
												logs_logger.severe("Expected Value --> \t" + Exp_irisextwlpwmmuxtest_intcount+ " (+/- 3)");
												logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+ "\r\n\n" );
											}
											else if(irisextenvpwmmuxtest.equals(check_string))
											{
												String MuxInterrupt_Count = resp[2];
												report_logger.severe("Expected Value --> \t" + Exp_irisextenvpwmmuxtest_intcount+ " (+/- 3)");
												report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
												
												logs_logger.severe("Expected Value --> \t" + Exp_irisextenvpwmmuxtest_intcount+ " (+/- 3)");
												logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count + "\r\n\n");
											}
											report_logger.severe(ArrowDelimitter);
											//publish("Status : Pass\r\n");
											//publish("UPDATE,PASS," + ""+"," + src + "," + "");
											UpdateTestResults(Pass,"",src,"");
											synchronized(lockObject)
											{
												notify_flag = 1;
												busyflag = false;
												lockObject.notifyAll();
											}
											
											
											
										}
										
									}catch(Exception ex)
									{
										ex.printStackTrace();
										
									}
									
									
								}
								else if(Resp.contains(RESPONSE_ERROR))
								{
									
									try
									{
										
										System.out.println("Error Response Received!!");
										
										String resp = Resp.replace("{", "");
										resp = resp.replace("}", "");
										String[] actual = resp.split(",");
										String reason = get_error_source(actual[1]);
										String source = get_source(actual[2]);
										String value = "";
										
										report_logger.severe("Result\t\t\tFail" );
										logs_logger.severe("Result\t\t\tFail" );
										if(lightenginefantest.equals(check_string))
										{
											String Fan_Count = actual[3];
											report_logger.severe("Expected Tach Count -->  : " + Expected_LightEngine_FanTachCount + "\t(+/- 25%)");
											report_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
											
											logs_logger.severe("Expected Tach Count -->  : " + Expected_LightEngine_FanTachCount+ "\t(+/- 25%)");
											logs_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
											check_string = "";
										}//HEAT SINK FAN TEST
										else if(heatsinkfantest.equals(check_string))
										{
											String Fan_Count = actual[3];
											report_logger.severe("Expected Tach Count -->  : " + Expected_HeatSink_FanTachCount+ "\t(+/- 25%)");
											report_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
											
											logs_logger.severe("Expected Tach Count -->  : " + Expected_HeatSink_FanTachCount+ "\t(+/- 25%)");
											logs_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
											check_string = "";
										}
										//IRIS FAN TEST
										else if(irisfantest.equals(check_string))
										{
											String Fan_Count = actual[3];
											report_logger.severe("Expected Tach Count -->  : " + Expected_Iris_FanTachCount+ "\t(+/- 25%)");
											report_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
											
											logs_logger.severe("Expected Tach Count -->  : " + Expected_Iris_FanTachCount+ "\t(+/- 25%)");
											logs_logger.severe("Observed Tach Count -->  : " + Fan_Count +"\r\n\n");
											check_string = "";
										}
										else if(redledtestzero.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_RedLed_Current_zeropercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_RedLed_Current_zeropercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											check_string = "";
										}
										else if(greenledtestzero.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_GreenLed_Current_zeropercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_GreenLed_Current_zeropercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											check_string = "";
										}
										else if(blueledtestzero.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_BlueLed_Current_zeropercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_BlueLed_Current_zeropercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											check_string = "";
										}
										else if(laserledtestzero.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_LaserLed_Current_zeropercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent+" V\t\tCurrent : " + Exp_LaserLed_Current_zeropercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											check_string = "";
										}
										else if(iris1ledtestzero.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_zeropercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_zeropercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											check_string = "";
										}
										else if(iris2ledtestzero.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_zeropercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_zeropercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											check_string = "";
										}
										
										
										//LED TEST RESULT : LED FIFTY PERCENT
						
										else if(redledtestfifty.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_RedLed_Current_fiftypercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_RedLed_Current_fiftypercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											check_string = "";
										}
										else if(greenledtestfifty.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_GreenLed_Current_fiftypercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_GreenLed_Current_fiftypercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											check_string = "";
										}
										else if(blueledtestfifty.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_BlueLed_Current_fiftypercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_BlueLed_Current_fiftypercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											check_string = "";
										}
										else if(laserledtestfifty.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_LaserLed_Current_fiftypercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_LaserLed_Current_fiftypercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											check_string = "";
										}
										else if(iris1ledtestfifty.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_fiftypercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_fiftypercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											check_string = "";
										}
										else if(iris2ledtestfifty.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_fiftypercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_fiftypercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_fiftypercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											check_string = "";
										}
										
										//LED TEST RESULT : LED SEVENTY FIVE PERCENT
										
										else if(redledtestseventyfive.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_RedLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											check_string = "";
										}
										else if(greenledtestseventyfive.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_GreenLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											check_string = "";
										}
										else if(blueledtestseventyfive.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_BlueLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											check_string = "";
										}
										else if(laserledtestseventyfive.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_LaserLed_Current_seventyfivepercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											check_string = "";
										}
										else if(iris1ledtestseventyfive.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_seventyfivepercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											check_string = "";
										}
										else if(iris2ledtestseventyfive.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_seventyfivepercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_seventyfivepercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											check_string = "";
										}
										
										//LED TEST RESULT : LED HUNDRED PERCENT
										
										else if(redledtesthundred.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_RedLed_Current_hundredpercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_RedLed_Current_hundredpercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_RedLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											check_string = "";
										}
										else if(greenledtesthundred.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_GreenLed_Current_hundredpercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_GreenLed_Current_hundredpercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_GreenLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											check_string = "";
										}
										else if(blueledtesthundred.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_BlueLed_Current_hundredpercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_BlueLed_Current_hundredpercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_BlueLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
										}
										else if(laserledtesthundred.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_LaserLed_Current_hundredpercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_LaserLed_Current_hundredpercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_LaserLed_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											check_string = "";
										}
										else if(iris1ledtesthundred.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_hundredpercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_Iris1Led_Current_hundredpercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris1Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											check_string = "";
										}
										else if(iris2ledtesthundred.equals(check_string))
										{
											String LedVoltage = actual[3];
											String LedCurrent = actual[4];
											if(chckbxLightEngine.isSelected())
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_hundredpercent + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_hundredpercent+" V\t\tCurrent : " + Exp_Iris2Led_Current_hundredpercent + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											else
											{
												report_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												report_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
												
												logs_logger.severe("Expected Values --> \tVoltage : " + Exp_Iris2Led_Voltage_zeropercent_noload+" V\t\tCurrent : " + ExpCurrent_LedCurrentValues_zeropercent_noload + " A\t(+/- 25%)");
												logs_logger.severe("Observed Values --> \tVoltage : " + LedVoltage + " V\tCurrent : " + LedCurrent + " A\r\n\n");
											}
											
											
											check_string = "";
										}
										
										//PDTESTS : IRIS1ZERO and HUNDRED
										else if(iris1pdtestzero.equals(check_string))
										{
											String PhotoVoltage = actual[3];
											report_logger.severe("Expected Value --> \tVoltage : " + Exp_iris1pdvoltage_zero+" V\t(+/- 25%)");
											report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
											
											logs_logger.severe("Expected Value --> \tVoltage : " + Exp_iris1pdvoltage_zero+" V\t(+/- 25%");
											logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										}
										else if(iris1pdtesthundred.equals(check_string))
										{
											String PhotoVoltage = actual[3];
											report_logger.severe("Expected Value --> \tVoltage : " + Exp_iris1pdvoltage_hundred+" V\t(+/- 25%)");
											report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
											
											logs_logger.severe("Expected Value --> \tVoltage : " + Exp_iris1pdvoltage_hundred+" V\t(+/- 25%)");
											logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										}
										
										//PDTESTS : IRIS2 ZERO AND HUNDRED
										
										else if(iris2pdtestzero.equals(check_string))
										{
											String PhotoVoltage = actual[3];
											report_logger.severe("Expected Value --> \tVoltage : " + Exp_iris2pdvoltage_zero+" V\t(+/- 25%)");
											report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
											
											logs_logger.severe("Expected Value --> \tVoltage : " + Exp_iris2pdvoltage_zero+" V\t(+/- 25%)");
											logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										}
										else if(iris2pdtesthundred.equals(check_string))
										{
											String PhotoVoltage = actual[3];
											report_logger.severe("Expected Value --> \tVoltage : " + Exp_iris2pdvoltage_hundred+" V\t(+/- 25%)");
											report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
											
											logs_logger.severe("Expected Value --> \tVoltage : " + Exp_iris2pdvoltage_hundred+" V\t(+/- 25%)");
											logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										}
//										PDTESTS : ENV ZERO AND HUNDRED
										
										else if(envpdtestzero.equals(check_string))
										{
											String PhotoVoltage = actual[3];
											report_logger.severe("Expected Value --> \tVoltage : " + Exp_envpdvoltage_zero+" V\t(+/- 25%)");
											report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
											
											logs_logger.severe("Expected Value --> \tVoltage : " + Exp_envpdvoltage_zero+" V\t(+/- 25%)");
											logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										}
										else if(envpdtesthundred.equals(check_string))
										{
											String PhotoVoltage = actual[3];
											report_logger.severe("Expected Value --> \tVoltage : " + Exp_envpdvoltage_hundred+" V\t(+/- 25%)");
											report_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
											
											logs_logger.severe("Expected Value --> \tVoltage : " + Exp_envpdvoltage_hundred+" V\t(+/- 25%)");
											logs_logger.severe("Observed Value --> \tVoltage : " + PhotoVoltage+" V\r\n\n");
										}
//										CS TESTS : COLORSENSOR 1
										
										else if(colorsensor1test.equals(check_string))
										{
											String ColorsensorVal_1 = actual[3];
											String ColorsensorVal_2 = actual[4];
											String ColorsensorVal_3 = actual[5];
											String ColorsensorVal_4 = actual[6];
											report_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
											report_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4 + "\r\n\n");
											
											logs_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
											logs_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4 + "\r\n\n");
										}
										else if(colorsensor2test.equals(check_string))
										{
											String ColorsensorVal_1 = actual[3];
											String ColorsensorVal_2 = actual[4];
											String ColorsensorVal_3 = actual[5];
											String ColorsensorVal_4 = actual[6];
											report_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
											report_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4+"\r\n\n");
											
											logs_logger.severe("Expected Value --> \t >100 and below \"FFFF\"" );
											logs_logger.severe("Observed Value --> \tVal 1 : " + ColorsensorVal_1 +" Val 2 : " + ColorsensorVal_2 +" Val 3 : " + ColorsensorVal_3 +" Val 4 : " + ColorsensorVal_4+"\r\n\n");
										}
										
										//PWM MUXTESTS 
										else if(wluc1pwmmuxtest.equals(check_string))
										{
											String MuxInterrupt_Count = actual[3];
											report_logger.severe("Expected Value --> \t" + Exp_wluc1pwmmuxtest_intcount+ " (+/- 3)");
											report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n" );
											
											logs_logger.severe("Expected Value --> \t" + Exp_wluc1pwmmuxtest_intcount+ " (+/- 3)");
											logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n" );
										}
										else if(envuc1pwmmuxtest.equals(check_string))
										{
											String MuxInterrupt_Count = actual[3];
											report_logger.severe("Expected Value --> \t" + Exp_envuc1pwmmuxtest_intcount+ " (+/- 3)");
											report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
											
											logs_logger.severe("Expected Value --> \t" + Exp_envuc1pwmmuxtest_intcount+ " (+/- 3)");
											logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
										}
										else if(irisuc1pwmmuxtest.equals(check_string))
										{
											String MuxInterrupt_Count = actual[3];
											report_logger.severe("Expected Value --> \t" + Exp_irisuc1pwmmuxtest_intcount+ " (+/- 3)");
											report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
											
											logs_logger.severe("Expected Value --> \t" + Exp_irisuc1pwmmuxtest_intcount+ " (+/- 3)");
											logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
										}
										else if(wluc2pwmmuxtest.equals(check_string))
										{
											String MuxInterrupt_Count = actual[3];
											report_logger.severe("Expected Value --> \t" + Exp_wluc2pwmmuxtest_intcount+ " (+/- 3)");
											report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
											
											logs_logger.severe("Expected Value --> \t" + Exp_wluc2pwmmuxtest_intcount+ " (+/- 3)");
											logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
										}
										else if(envuc2pwmmuxtest.equals(check_string))
										{
											String MuxInterrupt_Count = actual[3];
											report_logger.severe("Expected Value --> \t" + Exp_envuc2pwmmuxtest_intcount+ " (+/- 3)");
											report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
											
											logs_logger.severe("Expected Value --> \t" + Exp_envuc2pwmmuxtest_intcount+ " (+/- 3)");
											logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
										}
										else if(irisuc2pwmmuxtest.equals(check_string))
										{
											String MuxInterrupt_Count = actual[3];
											report_logger.severe("Expected Value --> \t" + Exp_irisuc2pwmmuxtest_intcount+ " (+/- 3)");
											report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
											
											logs_logger.severe("Expected Value --> \t" + Exp_irisuc2pwmmuxtest_intcount+ " (+/- 3)");
											logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
										}
										else if(wlextpwmmuxtest.equals(check_string))
										{
											String MuxInterrupt_Count = actual[3];
											report_logger.severe("Expected Value --> \t" + Exp_wlextpwmmuxtest_intcount+ " (+/- 3)");
											report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
											
											logs_logger.severe("Expected Value --> \t" + Exp_wlextpwmmuxtest_intcount+ " (+/- 3)");
											logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
										}
										else if(envextpwmmuxtest.equals(check_string))
										{
											String MuxInterrupt_Count = actual[3];
											report_logger.severe("Expected Value --> \t" + Exp_envextpwmmuxtest_intcount+ " (+/- 3)");
											report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
											
											logs_logger.severe("Expected Value --> \t" + Exp_envextpwmmuxtest_intcount+ " (+/- 3)");
											logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count+"\r\n\n" );
										}
										else if(irisextwlpwmmuxtest.equals(check_string))
										{
											String MuxInterrupt_Count = actual[3];
											report_logger.severe("Expected Value --> \t" + Exp_irisextwlpwmmuxtest_intcount+ " (+/- 3)");
											report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
											
											logs_logger.severe("Expected Value --> \t" + Exp_irisextwlpwmmuxtest_intcount+ " (+/- 3)");
											logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
										}
										else if(irisextenvpwmmuxtest.equals(check_string))
										{
											String MuxInterrupt_Count = actual[3];
											report_logger.severe("Expected Value --> \t" + Exp_irisextenvpwmmuxtest_intcount+ " (+/- 3)");
											report_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
											
											logs_logger.severe("Expected Value --> \t" + Exp_irisextenvpwmmuxtest_intcount+ " (+/- 3)");
											logs_logger.severe("Observed Value --> \t" +  MuxInterrupt_Count +"\r\n\n");
										}
										UpdateTestResults(Fail,reason,source,value);
										report_logger.severe(ArrowDelimitter);
										synchronized(lockObject)
										{
											notify_flag = 1;
											busyflag = false;
											lockObject.notifyAll();
										}
										//publish("UPDATE," + Fail + "," + reason + "," + source + "," + value);
										//publish("Status : Fail\r\n"); 
										
										recv_buff = null;
										Resp = "";
									}catch(Exception ex)
									{
										ex.printStackTrace();
										
									}
									
								}
								else
								{
									
									synchronized(lockObject)
									{
										busyflag = false;
										lockObject.notifyAll();
									}
									//.table.getModel().setValueAt("PASSED", 1, 4);
									
									System.out.println("Some Other FUCKING Response Received!!");
									btnStartButton.setEnabled(true);
									btnStartButton.setText("RUN");
									//btnBack.setEnabled(true);
									recv_buff = null;
									Resp = "";
								}
								
								
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
						
						
						
						recv_buff = null;
						//serialport.removeDataListener(); // Removes data listener associated with serial port Object
						total_bytes = 0;
						
					}
					// TODO Auto-generated method stub
					return null;
				}
				protected void process(List<String> chunks)
				{
					for (String str: chunks)
					{
						if(str.equals(""))
						{
							
						}
						else if(str.equals("SUB_PANEL"))
						{
							if(subpanellist.size() >= 1)
							{
								for(int t=0;t<subpanellist.size();t++)
								{
									Component[] comp = subpanellist.get(t).getComponents();
									for(Component c : comp)
									{
										c.setEnabled(true);
									}
								}
							}
							//btnSyncMTest.setEnabled(false);
							//textField_Totaltests.setText("");
							//textField_Passedtests.setText("");
						//textField_Failedtests.setText("");
							chckbxLightEngine.setSelected(false);
							chckbxIrisModule.setSelected(false);
							chckbxFans.setSelected(false);
						chckbxExternalCamFeed.setSelected(false);
							chckbxUartLoopBack.setSelected(false);
							chckbxOpticFiber.setSelected(false);
							chckbxEsst.setSelected(false);
							chckbxAsst.setSelected(false);
							chckbxGroupTests.setEnabled(true);
							chckbxGroupTests.setSelected(true);
							
							//btnBack.setEnabled(true);
							//btnSyncMTest.setText("SYNCED");
							logs_logger.severe("Response OK :\t\tMTESTSTART ");
							//disable MTESTSTART Button once {MTESTSTART} is successful
							btnStartButton.setEnabled(true);
							
						}
						else if(str.startsWith("VER_NO"))
						{
							String[] local = str.split(",");
							String Ver_No = local[1];
							textField_FirmWareVerNo.setText(Ver_No);
						}
						else if(str.startsWith("UPDATE"))
						{
							String[] local = str.split(",");
							System.out.println(Arrays.deepToString(local));
							String result = local[1];
							String reason = local[2];
							String source = local[3];
							
							
							System.out.println("btnname to be mapped : " + btnname_mapping_topanel);
							//No Error
							if(reason.equals(""))
							{
								//Timeout Response
								if(result.equals(Stryk_Demo.Timeout))
								{
									fail_count = fail_count + 1;
									if(result_indicator_mappings.get(btnname_mapping_topanel) != null)
									{
										result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.MAGENTA);
										
									}
									else
									{
										//btnSyncMTest.setText("SYNC");
									}
									
									
								}
								//Failure response
								else if(result.equals(Stryk_Demo.Fail))
								{
									fail_count = fail_count + 1;
									if(result_indicator_mappings.get(btnname_mapping_topanel) != null)
									{
										result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.RED);
										
									}
									else
									{
										//btnSyncMTest.setText("SYNC");
									}
								
									
								}
								//Success response
								else if(result.equals(Stryk_Demo.Pass))
								{
									pass_count = pass_count + 1;
									result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.green);
									
									
								}

								    
								
								
								
								source = "Source : " + source;
								
							}
							else if(!reason.equals(""))
							{
								//Timeout Response
								if(result.equals(Stryk_Demo.Timeout))
								{
									fail_count = fail_count + 1;
									if(result_indicator_mappings.get(btnname_mapping_topanel) != null)
									{
										result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.MAGENTA);
									}
									else
									{
										//btnSyncMTest.setText("SYNC");
									}
									
								}
								//Failure response
								else if(result.equals(Stryk_Demo.Fail))
								{
									fail_count = fail_count + 1;
									if(result_indicator_mappings.get(btnname_mapping_topanel) != null)
									{
										result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.RED);
									}
									else
									{
										//btnSyncMTest.setText("SYNC");
									}
									
								}
								//Success response
								else if(result.equals(Stryk_Demo.Pass))
								{
									pass_count = pass_count + 1;
									result_indicator_mappings.get(btnname_mapping_topanel).setBackground(Color.green);
									//System.out.println("Pass Count Incremented");
									
								}
							}
							//Error Response Received
							else
							{
								if(finish_button_flag)
								{
									//System.out.println("Yeah"); //Just for logs
								finish_button_flag = false;
									final Runnable my_thread = new Runnable()
									{
										@Override
										public void run() 
										{
											JOptionPane.showMessageDialog(frmStrykerManufacturingTests, "Error Response Received", "ERROR", JOptionPane.ERROR_MESSAGE);
										}
									};
									Thread appThread = new Thread() {
										@Override
										public void run() 
										{
											try
											{
												SwingUtilities.invokeLater(my_thread);
											} catch (Exception ex) {
												
												ex.printStackTrace();
											}
										}
									};
									appThread.run();
									
									
								}
								
								else
								{
									
									
									
									
								}
								
								
							}
						}
						else
						{
							textArea.append(str);
						}
					}
				}
		
			};
		recv_worker.execute();
		while(!recv_worker.isDone())
		{
			
		}
		}
	};
	
	public static void initialize_progressbar_bg_worker()
	{
		progressbar_bg_worker = new SwingWorker<Void, Void>()
		{

			@Override
			protected Void doInBackground() throws Exception {
				// TODO Auto-generated method stub
				
				//progressBar.setValue(progressBar.getValue() + update_value);
				//label.setText(progressBar.getValue() + " %");
				return null;
			}
			public void done()
			{
				
			}
			
		};
	}
	public void initialize_clearresults_bg_worker()
	{
		cleartestresults_bg_worker = new SwingWorker<List<String>, String>() {

			@Override
			protected List<String> doInBackground() throws Exception {
				
				publish("CLEAR");
				//progressBar.setValue(0);
				//label.setText(progressBar.getValue() + " %");
				// TODO Auto-generated method stub
				return null;
			}
			protected void process(List<String> chunks)
			{
				for(String s: chunks)
				{
					if(s.equals("CLEAR"))
					{
						
						panel_4.setBackground(Color.PINK);
						lblTestStatus.setText("READY");
					}
				}
			}
			public void done() 
			{
				
				
			}
			
		};
		
	}
	public static String get_current_item_int(int Current_Query_Number)
	{
		/*
		//TOTAL LINE ITEMS = 34
		String return_string = "";
		if(Current_Query_Number == 0){return_string = Stryk_Demo.uC1COMTEST;}
		else if(Current_Query_Number == 1){return_string = Stryk_Demo.uC2COMTEST;}
		else if(Current_Query_Number == 2){return_string = Stryk_Demo.uC1EEPROM;}
		else if(Current_Query_Number == 3){return_string = Stryk_Demo.uC2EEPROM;}
		else if(Current_Query_Number == 4){return_string = Stryk_Demo.uC1FLASH;}
		else if(Current_Query_Number == 5){return_string = Stryk_Demo.uC2FLASH;}
		else if(Current_Query_Number == 6){return_string = Stryk_Demo.uC1CAMUART;}
		else if(Current_Query_Number == 7){return_string = Stryk_Demo.uC1DEBUGUART;}
		else if(Current_Query_Number == 8){return_string = Stryk_Demo.uC2CAMUART;}
		else if(Current_Query_Number == 9){return_string = Stryk_Demo.uC2DEBUGUART;}
		else if(Current_Query_Number == 10){return_string = Stryk_Demo.uC2GUIUART;}
		else if(Current_Query_Number == 11){return_string = Stryk_Demo.uC2ADDUART;}
		else if(Current_Query_Number == 12) {return_string = Stryk_Demo.uC1PWR;}
		else if(Current_Query_Number == 13) {return_string = Stryk_Demo.uC2PWR;}
		else if(Current_Query_Number == 14) {return_string = Stryk_Demo.uC1WLPWMMUX;}
		else if(Current_Query_Number == 15) {return_string = Stryk_Demo.uC1ENVPWMMUX;}
		else if(Current_Query_Number == 16) {return_string = Stryk_Demo.uC1IRISPWMMUX;}
		else if(Current_Query_Number == 17) {return_string = Stryk_Demo.uC2WLPWMMUX;}
		else if(Current_Query_Number == 18) {return_string = Stryk_Demo.uC2ENVPWMMUX;}
		else if(Current_Query_Number == 19) {return_string = Stryk_Demo.uC2IRISPWMMUX;}
		else if(Current_Query_Number == 20) {return_string = Stryk_Demo.WLEXTWL;}
		else if(Current_Query_Number == 21) {return_string = Stryk_Demo.ENVEXTENV;}
		else if(Current_Query_Number == 22) {return_string = Stryk_Demo.IRISEXTCAMWHT;}
		else if(Current_Query_Number == 23) {return_string = Stryk_Demo.IRISEXTCAMENV;}
		else if(Current_Query_Number == 24) {return_string = Stryk_Demo.IPC_CONNECT;}
		else if(Current_Query_Number == 25) {return_string = Stryk_Demo.LIGHT_ENGINE_FAN;}
		else if(Current_Query_Number == 26) {return_string = Stryk_Demo.HEAT_SINK_FAN;}
		else if(Current_Query_Number == 27) {return_string = Stryk_Demo.IRIS_FAN;}
		else if(Current_Query_Number == 28) {return_string = Stryk_Demo.REDLEDTEST;}
		else if(Current_Query_Number == 29) {return_string = Stryk_Demo.GREENLEDTEST;}
		else if(Current_Query_Number == 30) {return_string = Stryk_Demo.BLUELEDTEST;}
		else if(Current_Query_Number == 31) {return_string = Stryk_Demo.ENVLEDTEST;}
		else if(Current_Query_Number == 32) {return_string = Stryk_Demo.IRIS1LEDTEST;}
		else if(Current_Query_Number == 33) {return_string = Stryk_Demo.IRIS2LEDTEST;}
		else if(Current_Query_Number == 34) {return_string = Stryk_Demo.GUI;}
		
		
		
		*/
		
		return "";
		
	}
	public static String get_current_item_string(String str)
	
		{
			//TOTAL LINE ITEMS = 34
			String return_string = "";
			if(str.equals(uc1pwrtest)) {return_string = Stryk_Demo.uC1PWRTEST;}								//1
			else if(str.equals(uc2pwrtest)) {return_string = Stryk_Demo.uC2PWRTEST;}							//2
			else if(str.equals(uc1comtest)){return_string = Stryk_Demo.uC1COMTEST;} 							//3
			else if(str.equals(uc2comtest)){return_string = Stryk_Demo.uC2COMTEST;}							//4
			else if(str.equals(ipctest)) {return_string = Stryk_Demo.IPCTEST;}								//5
			else if(str.equals(uc1eepromtest)){return_string = Stryk_Demo.uC1EEPROMTEST;}						//6
			else if(str.equals(uc2eepromtest)){return_string = Stryk_Demo.uC2EEPROMTEST;}						//7
			else if(str.equals("uC1FLASHTEST")){return_string = Stryk_Demo.uC1FLASHTEST;}						//8
			else if(str.equals(uc2flashtest)){return_string = Stryk_Demo.uC2FLASHTEST;}						//9
			else if(str.equals(uc1camuarttest)){return_string = Stryk_Demo.uC1CAMUARTTEST;}					//10
			else if(str.equals(uc1debuguarttest)){return_string = Stryk_Demo.uC1DEBUGUARTTEST;}				//11
			else if(str.equals(uc2camuarttest)){return_string = Stryk_Demo.uC2CAMUARTTEST;}					//12
			else if(str.equals(uc2debuguarttest)){return_string = Stryk_Demo.uC2DEBUGUARTTEST;}				//13
			else if(str.equals(uc2guiuarttest)){return_string = Stryk_Demo.uC2GUIUARTTEST;}					//14
			else if(str.equals(uc2adduarttest)){return_string = Stryk_Demo.uC2ADDUARTTEST;}					//15
			else if(str.equals(lightenginefantest)){return_string = Stryk_Demo.LIGHTENGINEFANTEST;}			//16
			else if(str.equals(heatsinkfantest)){return_string = Stryk_Demo.HEATSINKFANTEST;}					//17
			else if(str.equals(irisfantest)){return_string = Stryk_Demo.IRISFANTEST;}							//18
			else if(str.equals(wluc1pwmmuxtest)){return_string = Stryk_Demo.WLuC1PWMMUXTEST;}					//19
			else if(str.equals(envuc1pwmmuxtest)) {return_string = Stryk_Demo.ENVuC1PWMMUXTEST;}				//20
			else if(str.equals(irisuc1pwmmuxtest)) {return_string = Stryk_Demo.IRISuC1PWMMUXTEST;}			//21
			else if(str.equals(wluc2pwmmuxtest)) {return_string = Stryk_Demo.WLuC2PWMMUXTEST;}				//22
			else if(str.equals(envuc2pwmmuxtest)) {return_string = Stryk_Demo.ENVuC2PWMMUXTEST;}				//23
			else if(str.equals(irisuc2pwmmuxtest)) {return_string = Stryk_Demo.IRISuC2PWMMUXTEST;}			//24
			else if(str.equals(wlextpwmmuxtest)) {return_string = Stryk_Demo.WLEXTPWMMUXTEST;}				//25
			else if(str.equals(envextpwmmuxtest)) {return_string = Stryk_Demo.ENVEXTPWMMUXTEST;}				//26
			else if(str.equals(irisextwlpwmmuxtest)) {return_string = Stryk_Demo.IRISEXTWLPWMMUXTEST;}				//27
			else if(str.equals(irisextenvpwmmuxtest)) {return_string = Stryk_Demo.IRISEXTENVPWMMUXTEST;}			//28
			
			//LED NO LOAD 0%
			else if(str.equals(redledtestzero)) {return_string = Stryk_Demo.REDLEDTEST_ZERO_NOLOAD;}	//29 
			else if(str.equals(greenledtestzero)) {return_string = Stryk_Demo.GREENLEDTEST_ZERO_NOLOAD;}//30
			else if(str.equals(blueledtestzero)) {return_string = Stryk_Demo.BLUELEDTEST_ZERO_NOLOAD;}	//31
			else if(str.equals(laserledtestzero)) {return_string = Stryk_Demo.ENVLEDTEST_ZERO_NOLOAD;}	//32
			else if(str.equals(iris1ledtestzero)) {return_string = Stryk_Demo.IRIS1LEDTEST_ZERO_NOLOAD;}//33
			else if(str.equals(iris2ledtestzero)) {return_string = Stryk_Demo.IRIS2LEDTEST_ZERO_NOLOAD;}//34
			
			//LED NO LOAD 50%
			else if(str.equals(redledtestfifty)) {return_string = Stryk_Demo.REDLEDTEST_FIFTY_NOLOAD;}	//35
			else if(str.equals(greenledtestfifty)) {return_string = Stryk_Demo.GREENLEDTEST_FIFTY_NOLOAD;}//36
			else if(str.equals(blueledtestfifty)) {return_string = Stryk_Demo.BLUELEDTEST_FIFTY_NOLOAD;}//37
			else if(str.equals(laserledtestfifty)) {return_string = Stryk_Demo.ENVLEDTEST_FIFTY_NOLOAD;}//38
			else if(str.equals(iris1ledtestfifty)) {return_string = Stryk_Demo.IRIS1LEDTEST_FIFTY_NOLOAD;}//39
			else if(str.equals(iris2ledtestfifty)) {return_string = Stryk_Demo.IRIS2LEDTEST_FIFTY_NOLOAD;}//40
			
			//LED NO LOAD 75%
			else if(str.equals(redledtestseventyfive)) {return_string = Stryk_Demo.REDLEDTEST_SEVENTY_FIVE_NOLOAD;}	//41
			else if(str.equals(greenledtestseventyfive)) {return_string = Stryk_Demo.GREENLEDTEST_SEVENTY_FIVE_NOLOAD;}//42
			else if(str.equals(blueledtestseventyfive)) {return_string = Stryk_Demo.BLUELEDTEST_SEVENTY_FIVE_NOLOAD;}//43
			else if(str.equals(laserledtestseventyfive)) {return_string = Stryk_Demo.ENVLEDTEST_SEVENTY_FIVE_NOLOAD;}//44
			else if(str.equals(iris1ledtestseventyfive)) {return_string = Stryk_Demo.IRIS1LEDTEST_SEVENTY_FIVE_NOLOAD;}//45
			else if(str.equals(iris2ledtestseventyfive)) {return_string = Stryk_Demo.IRIS2LEDTEST_SEVENTY_FIVE_NOLOAD;}//46
			
			//LED NO LOAD 100%
			else if(str.equals(redledtesthundred)) {return_string = Stryk_Demo.REDLEDTEST_HUNDRED_NOLOAD;}	//47
			else if(str.equals(greenledtesthundred)) {return_string = Stryk_Demo.GREENLEDTEST_HUNDRED_NOLOAD;}//48
			else if(str.equals(blueledtesthundred)) {return_string = Stryk_Demo.BLUELEDTEST_HUNDRED_NOLOAD;}//49
			else if(str.equals(laserledtesthundred)) {return_string = Stryk_Demo.ENVLEDTEST_HUNDRED_NOLOAD;}//50
			else if(str.equals(iris1ledtesthundred)) {return_string = Stryk_Demo.IRIS1LEDTEST_HUNDRED_NOLOAD;}//51
			else if(str.equals(iris2ledtesthundred)) {return_string = Stryk_Demo.IRIS2LEDTEST_HUNDRED_NOLOAD;}//52
			
			//LED LOAD 0%
			else if(str.equals(redledtestzero)) {return_string = Stryk_Demo.REDLEDTEST_ZERO_LOAD;}	//53 
			else if(str.equals(greenledtestzero)) {return_string = Stryk_Demo.GREENLEDTEST_ZERO_LOAD;}//54
			else if(str.equals(blueledtestzero)) {return_string = Stryk_Demo.BLUELEDTEST_ZERO_LOAD;}	//55
			else if(str.equals(laserledtestzero)) {return_string = Stryk_Demo.ENVLEDTEST_ZERO_LOAD;}	//56
			else if(str.equals(iris1ledtestzero)) {return_string = Stryk_Demo.IRIS1LEDTEST_ZERO_LOAD;}//57
			else if(str.equals(iris2ledtestzero)) {return_string = Stryk_Demo.IRIS2LEDTEST_ZERO_LOAD;}//58
			
			//LED LOAD 50%
			else if(str.equals(redledtestfifty)) {return_string = Stryk_Demo.REDLEDTEST_FIFTY_LOAD;}	//59 
			else if(str.equals(greenledtestfifty)) {return_string = Stryk_Demo.GREENLEDTEST_FIFTY_LOAD;}//60
			else if(str.equals(blueledtestfifty)) {return_string = Stryk_Demo.BLUELEDTEST_FIFTY_LOAD;}	//61
			else if(str.equals(laserledtestfifty)) {return_string = Stryk_Demo.ENVLEDTEST_FIFTY_LOAD;}	//62
			else if(str.equals(iris1ledtestfifty)) {return_string = Stryk_Demo.IRIS1LEDTEST_FIFTY_LOAD;}//63
			else if(str.equals(iris2ledtestfifty)) {return_string = Stryk_Demo.IRIS2LEDTEST_FIFTY_LOAD;}//64
			
			//LED LOAD 75%
			else if(str.equals(redledtestseventyfive)) {return_string = Stryk_Demo.REDLEDTEST_SEVENTY_FIVE_LOAD;}	//65 
			else if(str.equals(greenledtestseventyfive)) {return_string = Stryk_Demo.GREENLEDTEST_SEVENTY_FIVE_LOAD;}//66
			else if(str.equals(blueledtestseventyfive)) {return_string = Stryk_Demo.BLUELEDTEST_SEVENTY_FIVE_LOAD;}	//67
			else if(str.equals(laserledtestseventyfive)) {return_string = Stryk_Demo.ENVLEDTEST_SEVENTY_FIVE_LOAD;}	//68
			else if(str.equals(iris1ledtestseventyfive)) {return_string = Stryk_Demo.IRIS1LEDTEST_SEVENTY_FIVE_LOAD;}//69
			else if(str.equals(iris2ledtestseventyfive)) {return_string = Stryk_Demo.IRIS2LEDTEST_SEVENTY_FIVE_LOAD;}//70
			
			//LED LOAD 100%
			else if(str.equals(redledtesthundred)) {return_string = Stryk_Demo.REDLEDTEST_HUNDRED_LOAD;}	//71 
			else if(str.equals(greenledtesthundred)) {return_string = Stryk_Demo.GREENLEDTEST_HUNDRED_LOAD;}//72
			else if(str.equals(blueledtesthundred)) {return_string = Stryk_Demo.BLUELEDTEST_HUNDRED_LOAD;}	//73
			else if(str.equals(laserledtesthundred)) {return_string = Stryk_Demo.ENVLEDTEST_HUNDRED_LOAD;}	//74
			else if(str.equals(iris1ledtesthundred)) {return_string = Stryk_Demo.IRIS1LEDTEST_HUNDRED_LOAD;}//75
			else if(str.equals(iris2ledtesthundred)) {return_string = Stryk_Demo.IRIS2LEDTEST_HUNDRED_LOAD;}//76
			
			//FIBER DETECT TESTS
			else if(str.equals(iris1fiberdetecttest)) {return_string = Stryk_Demo.FIBER1DETECTONTEST;}
			else if(str.equals(iris1fiberdetecttest)) {return_string = Stryk_Demo.FIBER1DETECTOFFTEST;}
			else if(str.equals(iris2fiberdetecttest))	{return_string = Stryk_Demo.FIBER2DETECTONTEST;}
			else if(str.equals(iris2fiberdetecttest)) {return_string = Stryk_Demo.FIBER2DETECTOFFTEST;}
			//BEAMSENSOR TEST
			else if(str.equals(beamsensortest)) {return_string = Stryk_Demo.BEAMSENSORONTEST;}
			
			//ESST TEST
			else if(str.equals(essttest)) {return_string = Stryk_Demo.ESSTONTEST;}
			
			//ASST TEST	
			else if(str.equals(assttest)) {return_string = Stryk_Demo.ASSTONTEST;}
			
			//IRIS PGOOD TEST
			else if(str.equals(irispgoodontest)) {return_string = Stryk_Demo.IRISPGOODTEST;}
			//FRONT BOARD POWER TEST
			else if(str.equals(frontboardtest)) {return_string = Stryk_Demo.FBPWRTEST;}
			//PD TEST - IRIS1
			else if(str.equals(iris1pdtestzero)) {return_string = Stryk_Demo.PDTEST_IRIS1_ZERO;}
			else if(str.equals(iris1pdtesthundred)) {return_string = Stryk_Demo.PDTEST_IRIS1_HUNDRED;}
			
			//PD TEST - IRIS2
			else if(str.equals(iris2pdtestzero)) {return_string = Stryk_Demo.PDTEST_IRIS2_ZERO;}
			else if(str.equals(iris2pdtesthundred)) {return_string = Stryk_Demo.PDTEST_IRIS2_HUNDRED;}
			
			//PD TEST - ENV
			else if(str.equals(envpdtestzero)) {return_string = Stryk_Demo.PDTEST_ENV_ZERO;}
			else if(str.equals(envpdtesthundred)) {return_string = Stryk_Demo.PDTEST_ENV_HUNDRED;}
			
			//FAN FAILURE TEST
			else if(str.equals(fanfailuretest)) {return_string = Stryk_Demo.FANFAILURETEST;}
			
			
			//COLOR SENSOR TEST
			else if(str.equals(colorsensor1test)) {return_string = Stryk_Demo.COLORSENSOR1TEST;}
			else if(str.equals(colorsensor2test)) {return_string = Stryk_Demo.COLORSENSOR2TEST;}
			
			
			return return_string;
			
			
		
		
	}
	public Border getScrollViewportBorder() {
		return scrollPane_4.getViewportBorder();
	}
	public void setScrollViewportBorder(Border viewportBorder) {
		scrollPane_4.setViewportBorder(viewportBorder);
	}
}





