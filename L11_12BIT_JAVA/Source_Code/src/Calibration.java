import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.TitledBorder;
import javax.swing.text.NumberFormatter;

import com.fazecast.jSerialComm.SerialPort;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Timer;



public class Calibration {

	public static JFrame frmLWlCalibration;
	public static JPanel rgbpanel;
	public static JPanel controlpanel;
	public static JPanel colorsensorpanel;
	public static JPanel adcpanel;
	public static JPanel commonpanel;
	public static JPanel Modeselectionpanel;
	public static JPanel Wlcpanel;
	public static JPanel Calibrationlogspanel;
	
	public static JLabel lblR;
	public static JLabel lblG;
	public static JLabel lblB;
	public static JLabel lblFreeze;
	public static JLabel redpercent;
	public static JLabel greenpercent;
	public static JLabel bluepercent;
	public static JLabel commonpercent;
	public static JLabel lblC;
	public static JLabel lblCS1;
	public static JLabel lblCS2;
	public static JLabel Amp1;
	public static JLabel Amp2;
	public static JLabel Amp3;
	public static JLabel adcwl;
	public static JLabel adcred;
	public static JLabel adcgreen;
	public static JLabel adcblue;
	public static JLabel redamps;
	public static JLabel greenamps;
	public static JLabel blueamps;
	public static JLabel csred;
	public static JLabel csgreen;
	public static JLabel csblue;
	public static JLabel cswl;
	
	public int FPS_MIN = 0;
	public int FPS_MAX = 255;
	public int FPS_INIT = 0;
	
	public byte dummy_byte 						= 0x0;
	
	public static JButton btnStart;
	public static JButton btnEnd;
	public static JButton btnVerify;
	public static JButton btnSave;
	public static JButton btnAlignToGreen;
	public static JButton btnExit;
	
	public static JSlider redslider;
	public static JSlider greenslider;
	public static JSlider blueslider;
	public static JSlider commonslider;
		
	public JCheckBox chckbxredfreeze;
	public JCheckBox chckbxgreenfreeze;
	public JCheckBox chckbxbluefreeze;
	
	public static JTextField formattedTextField_cs1a;
	public static JTextField formattedTextField_cs1b;
	public static JTextField formattedTextField_cs1c;
	public static JTextField formattedTextField_cs1d;
	public static JTextField formattedTextField_cs2a;
	public static JTextField formattedTextField_cs2b;
	public static JTextField formattedTextField_cs2c;
	public static JTextField formattedTextField_cs2d;
	public static JTextField formattedTextField_wlcx;
	public static JTextField formattedTextField_adcred;
	public static JTextField formattedTextField_adcgreen;
	public static JTextField formattedTextField_adcblue;
	public static JTextField formattedTextField_wlcy;
	
	public JScrollPane scroll;
	
	public static boolean Align_Flag = false;
	
	public static String Calibration_Start 				= "START_CALIBRATION";
	public static String Calibration_Init 					= "CALIBRATION_INIT";
	public static String Calibration_ReadADC 		= "CALIBRATION_READADC";
	public static String Calibration_ReadCS 			= "CALIBRATION_READCS";
	public static String Calibration_DAC 					= "CALIBRATION_DAC";
	public static String Calibration_Save					= "CALIBRATION_SAVE";
	public static String Calibration_Verify	 			= "CALIBRATION_VERIFY";
	public static String Calibration_End 					= "CALIBRATION_END";
	public static String Calibration_Align 				= "CALIBRATION_ALIGN";
	public static String Calibration_RGB                  = "CALIBRATION_RGB";
	
	public static float MFactor_Red_Slider;
	public static float MFactor_Green_Slider;
	public static float MFactor_Blue_Slider;
	
	public static int Current_White_Position;
	public static int Maximum_White_Value;
	public static int Previous_Value ;
	public static int Delta;
	
	
	public static Component[] common_controls;
	public static Component[] calibration_controls;
	public static Component[] rgb_controls;
	public static Component[] adc_panel;
	public static Component[] coordinates_panel;
	public static Component[] colorsensor_panel;
	
	public static SerialPort serialport = Stryk_Demo.calibrationserialport;
	
	public static JRadioButton rdbtnAuto;
	public static JRadioButton rdbtnManual;
	
	public static ButtonGroup modeselection = new ButtonGroup();
	public static  JTextArea textArea;
	public static  JButton btnReadAdc;
	public static JButton btnReadCs;
	
	//Hash Map that maps reference string received by the constructor to the current test to be run
		public static HashMap<String, String> calibration_btnname_mapping 	= new HashMap<String, String>();

		//Hash Map for Timeout Mapping 
		public static HashMap<String, Long> calibration_cmd_tout_mapping = new HashMap<String, Long>();

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					//UIManager.LookAndFeelInfo looks[] = UIManager.getInstalledLookAndFeels();
					//UIManager.setLookAndFeel(looks[1].getClassName()); // For MAC
					//Calibration window = new Calibration();
					//window.frmLWlCalibration.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public Calibration() {
		//Mapping Command String With the Reference Strings
		calibration_btnname_mapping.put("START_CALIBRATION", "{CAL_START}");
		calibration_btnname_mapping.put("CALIBRATION_READADC","{CAL_READ,ADC}");
		calibration_btnname_mapping.put("CALIBRATION_READCS", "{CAL_READ,CS}");
		calibration_btnname_mapping.put("CALIBRATION_DAC", "{CAL_DAC");
		calibration_btnname_mapping.put("CALIBRATION_SAVE", "{CAL_SAVE}");
		calibration_btnname_mapping.put("CALIBRATION_VERIFY", "{CAL_VERIFY}");
		calibration_btnname_mapping.put("CALIBRATION_END", "{CAL_END}");
		calibration_btnname_mapping.put("CALIBRATION_ALIGN", "{CAL_ALIGN}");
		calibration_btnname_mapping.put("CALIBRATION_RGB", "{CAL_RGB");
		
		//Timeout Mapping
		calibration_cmd_tout_mapping.put("{CAL_START}", 		(long) 2000);
		calibration_cmd_tout_mapping.put("{CAL_READ,ADC}", 	(long) 5000);
		calibration_cmd_tout_mapping.put("{CAL_READ,CS}",		(long) 5000);
		calibration_cmd_tout_mapping.put("{CAL_SAVE}",	 		(long) 2000);
		calibration_cmd_tout_mapping.put("{CAL_DAC", 			(long)2000);
		calibration_cmd_tout_mapping.put("{CAL_VERIFY}", 	(long) 2000);
		calibration_cmd_tout_mapping.put("{CAL_END}", 			(long) 2000);
		calibration_cmd_tout_mapping.put("{CAL_ALIGN}", (long) 2000);
		calibration_cmd_tout_mapping.put("{CAL_RGB", (long) 2000);
		
		//Stryk_Demo.calibrationserialport;
		if(serialport != null)
		{
			System.out.println("A Valid Serial Port inside Calibration");
		}
		else
		{
			System.out.println("Serial Port is null");
		}
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings("static-access")
	private void initialize() {
		System.out.println("Initializing Calibration Window...");
		NumberFormat format1 = NumberFormat.getInstance();
		
		NumberFormatter formatter1 = new NumberFormatter(format1);
		
		formatter1.setValueClass(Integer.class);
		formatter1.setMinimum(0);
		formatter1.setMaximum(100);
		formatter1.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formatter1.setCommitsOnValidEdit(true);
		this.frmLWlCalibration = new JFrame();
		//Set the title below
		frmLWlCalibration.setTitle("L11 WL CALIBRATION TOOL");
		//Set the foregroundcolor below
		frmLWlCalibration.getContentPane().setForeground(SystemColor.window);
		frmLWlCalibration.setBounds(100, 100, 826, 431);
		//Set foreground color below
		frmLWlCalibration.setForeground(UIManager.getColor("InternalFrame.borderColor"));
		//Set background color below
		frmLWlCalibration.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		//Set foreground color below
		frmLWlCalibration.getContentPane().setForeground(SystemColor.window);
		frmLWlCalibration.getContentPane().setBackground(SystemColor.textHighlightText);
		//Calibration frame cannot be resized
		frmLWlCalibration.setResizable(false);
		//No Layout for this form (or absolute layout)
		frmLWlCalibration.getContentPane().setLayout(null);
		
		//rgb panel definition
		this.rgbpanel = new JPanel();
		rgbpanel.setBounds(190, 63, 321, 133);
		
		//set border for this panel
		this.rgbpanel.setBorder(new TitledBorder(null, "RGB CONTROLS", TitledBorder.CENTER, TitledBorder.TOP,  new Font("Calibri Light", Font.BOLD, 11), new Color(51, 51, 51)));
		//set background color for this panel
		rgbpanel.setBackground((Color) null);
		//add this panel to the main calibration frame
		frmLWlCalibration.getContentPane().add(rgbpanel);
		//set layout null
		rgbpanel.setLayout(null);
		
		//Label definition
		this.lblR = new JLabel("R");
		//Set enabled - false by default
		lblR.setEnabled(false);
		//set the font attributes for this label
		lblR.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		//Set Bounds for this label
		lblR.setBounds(20, 30, 20, 14);
		//Add this label to the rgb panel
		rgbpanel.add(lblR);
		
		this.lblG = new JLabel("G");
		lblG.setEnabled(false);
		lblG.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblG.setBounds(20, 57, 20, 14);
		rgbpanel.add(lblG);
		
		this.lblB = new JLabel("B");
		lblB.setEnabled(false);
		lblB.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblB.setBounds(20, 85, 20, 14);
		rgbpanel.add(lblB);
		
		this.lblFreeze = new JLabel("Freeze");
		lblFreeze.setEnabled(false);
		lblFreeze.setFont(new Font("Calibri Light", Font.BOLD, 11));
		lblFreeze.setBounds(279, 8, 38, 14);
		rgbpanel.add(lblFreeze);
		
		this.redslider = new JSlider(JSlider.HORIZONTAL,FPS_MIN, FPS_MAX, FPS_INIT);
		redslider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				redpercent.setText(Integer.toString(redslider.getValue()));
				if(!source.getValueIsAdjusting())
				{
					if(!Align_Flag)
					{
						Calibration_Worker start = new Calibration_Worker(Calibration_DAC,"RED",redslider.getValue(),serialport, true);
						start.execute();
					}
					else
					{
						
					}
					
					//System.out.println(redslider.getValue());
				}
				
			}
		});
		redslider.setEnabled(false);
		
		//redslider.setMaximum(255);
		redslider.setBounds(40, 25, 200, 23);
		rgbpanel.add(redslider);
		
		this.greenslider = new JSlider(JSlider.HORIZONTAL,FPS_MIN, FPS_MAX, FPS_INIT);
		greenslider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				greenpercent.setText(Integer.toString(greenslider.getValue()));
				if(!source.getValueIsAdjusting())
				{
					if(!Align_Flag)
					{
						Calibration_Worker start = new Calibration_Worker(Calibration_DAC,"GREEN",greenslider.getValue(),serialport, true);
						start.execute();
					}
					else
					{
						
					}
					
					//System.out.println(redslider.getValue());
				}
			}
		});
		greenslider.setEnabled(false);
		greenslider.setBounds(40, 53, 200, 23);
		rgbpanel.add(greenslider);
		
		this.blueslider = new JSlider(JSlider.HORIZONTAL,FPS_MIN, FPS_MAX, FPS_INIT);
		blueslider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				bluepercent.setText(Integer.toString(blueslider.getValue()));
				if(!source.getValueIsAdjusting())
				{
					if(!Align_Flag)
					{
						Calibration_Worker start = new Calibration_Worker(Calibration_DAC,"BLUE",blueslider.getValue(),serialport, true);
						start.execute();
					}
					else
					{
						
					}
					
					//System.out.println(redslider.getValue());
				}
			}
		});
		blueslider.setEnabled(false);
		blueslider.setBounds(40, 80, 200, 23);
		rgbpanel.add(blueslider);
		
		this.chckbxredfreeze = new JCheckBox("");
		chckbxredfreeze.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
			
				if(e.getStateChange() == ItemEvent.SELECTED)
				{
					//Stuff to do on selection
					//Freeze red slider stuff
					lblR.setEnabled(false);
					redslider.setEnabled(false);
					redpercent.setEnabled(false);
					
					//Part that checks whether green and blue checkboxes are also selected
					//if selected, enable the common control panel and the associated controls
					if(chckbxgreenfreeze.isSelected() && chckbxbluefreeze.isSelected())
					{
						btnAlignToGreen.setEnabled(false);
						for(Component c : common_controls)
						{
							c.setEnabled(true);
						}
						//Set White Slider to Max 100. 
						//No Bg task.
						//Only UI Update
						Align_Flag = true;
						Previous_Value = greenslider.getValue();
						commonslider.setMaximum(greenslider.getValue());
						commonslider.setValue(greenslider.getValue());
						Align_Flag = false;
						
						
						//Multiplication Factor Definitions
						//Multiplication factor for red = redslider's current value / green slider's current value
						MFactor_Red_Slider = (float) redslider.getValue() / greenslider.getValue();
						System.out.println("MFactor for Red : " + MFactor_Red_Slider);
						//Multiplication factor for green slider = green slider's current value / 100;
						MFactor_Green_Slider =  Math.round(1.0);
						System.out.println("MFactor for Green : " + MFactor_Green_Slider);
						//Multiplication Factor for blue slider = blue slider's current value / green slider's current value
						MFactor_Blue_Slider = (float) blueslider.getValue() / greenslider.getValue();
						System.out.println("MFactor for Blue : " + MFactor_Blue_Slider);
						
						
						
						
					}
				}
				else
				{
					btnAlignToGreen.setEnabled(true);
					//Disable all the children component of common control panel
					for(Component c : common_controls)
					{
						c.setEnabled(false);
					}
					//Stuff to do on deselected state
					//Release red slider stuff
					lblR.setEnabled(true);
					redslider.setEnabled(true);
					redpercent.setEnabled(true);
				}
			}
		});
		chckbxredfreeze.setEnabled(false);
		chckbxredfreeze.setBounds(290, 25, 20, 23);
		rgbpanel.add(chckbxredfreeze);
		
		this.chckbxgreenfreeze = new JCheckBox("");
		chckbxgreenfreeze.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED)
				{
					//Freeze green slider stuff on selection
					lblG.setEnabled(false);
					greenslider.setEnabled(false);
					greenpercent.setEnabled(false);
					
					//Part that checks whether red and blue check boxes are also selected
					//if selected, enable the common control panel and the associated controls
					if(chckbxredfreeze.isSelected() && chckbxbluefreeze.isSelected())
					{
						btnAlignToGreen.setEnabled(false);
						for(Component c : common_controls)
						{
							c.setEnabled(true);
						}
						//Set White Slider to Max 100. 
						//No Bg task.
						//Only UI Update
						
						Align_Flag = true;
						Previous_Value = greenslider.getValue();
						commonslider.setMaximum(greenslider.getValue());
						commonslider.setValue(greenslider.getValue());
						Align_Flag = false;
						
						//Multiplication Factor Definitions
						//Multiplication factor for red = redslider's current value / green slider's current value
						MFactor_Red_Slider = (float) redslider.getValue() / greenslider.getValue();
						System.out.println("MFactor for Red : " + MFactor_Red_Slider);
						//Multiplication factor for green slider = green slider's current value / 100;
						MFactor_Green_Slider =  Math.round(1.0);
						System.out.println("MFactor for Green : " + MFactor_Green_Slider);
						//Multiplication Factor for blue slider = blue slider's current value / green slider's current value
						MFactor_Blue_Slider = (float) blueslider.getValue() / greenslider.getValue();
						System.out.println("MFactor for Blue : " + MFactor_Blue_Slider);
						
					}
					
				}
				else
				{
					btnAlignToGreen.setEnabled(true);
					//Disable all the children component of common control panel
					for(Component c : common_controls)
					{
						c.setEnabled(false);
					}
					//Release green slider stuff on deselection
					lblG.setEnabled(true);
					greenslider.setEnabled(true);
					greenpercent.setEnabled(true);
				}
			}
		});
		chckbxgreenfreeze.setEnabled(false);
		chckbxgreenfreeze.setBounds(290, 53, 20, 23);
		rgbpanel.add(chckbxgreenfreeze);
		
		this.chckbxbluefreeze = new JCheckBox("");
		chckbxbluefreeze.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED)
				{
					//Freeze blue slider stuff on selection
					lblB.setEnabled(false);
					blueslider.setEnabled(false);
					bluepercent.setEnabled(false);
					
					//Part that checks whether red and green checkboxes are also selected
					//if selected, enable the common control panel and the associated controls
					if(chckbxgreenfreeze.isSelected() && chckbxredfreeze.isSelected())
					{
						btnAlignToGreen.setEnabled(false);
						for(Component c : common_controls)
						{
							c.setEnabled(true);
						}
						//Set White Slider to Max 100. 
						//No Bg task.
						//Only UI Update
						Align_Flag = true;
						Previous_Value = greenslider.getValue();
						commonslider.setMaximum(greenslider.getValue());
						commonslider.setValue(greenslider.getValue());
						Align_Flag = false;
						//Multiplication Factor Definitions
						//Multiplication factor for red = redslider's current value / green slider's current value
						MFactor_Red_Slider = (float) redslider.getValue() / greenslider.getValue();
						System.out.println("MFactor for Red : " + MFactor_Red_Slider);
						//Multiplication factor for green slider = green slider's current value / 100;
						MFactor_Green_Slider =  Math.round(1.0);
						System.out.println("MFactor for Green : " + MFactor_Green_Slider);
						//Multiplication Factor for blue slider = blue slider's current value / green slider's current value
						MFactor_Blue_Slider = (float) blueslider.getValue() / greenslider.getValue();
						System.out.println("MFactor for Blue : " + MFactor_Blue_Slider);
						
					}
					
				}
				else
				{
					btnAlignToGreen.setEnabled(true);
					//Disable all the children component of common control panel
					for(Component c : common_controls)
					{
						c.setEnabled(false);
					}
					//Release blue slider stuff on deselction
					lblB.setEnabled(true);
					blueslider.setEnabled(true);
					bluepercent.setEnabled(true);
				}
			}
		});
		chckbxbluefreeze.setEnabled(false);
		chckbxbluefreeze.setBounds(290, 80, 20, 23);
		rgbpanel.add(chckbxbluefreeze);
		
		this.redpercent = new JLabel("0 ");
		redpercent.setEnabled(false);
		redpercent.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		redpercent.setBounds(255, 30, 32, 14);
		rgbpanel.add(redpercent);
		
		this.greenpercent = new JLabel("0 ");
		greenpercent.setEnabled(false);
		greenpercent.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		greenpercent.setBounds(255, 59, 32, 14);
		rgbpanel.add(greenpercent);
		
		this.bluepercent = new JLabel("0 ");
		bluepercent.setEnabled(false);
		bluepercent.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		bluepercent.setBounds(255, 88, 32, 14);
		rgbpanel.add(bluepercent);
		
		this.btnAlignToGreen = new JButton("ALIGN TO GREEN");
		btnAlignToGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Align_Flag = true;
				redslider.setValue(greenslider.getValue());
				blueslider.setValue(greenslider.getValue());
				Align_Flag = false;
				Calibration_Worker start = new Calibration_Worker(Calibration_Align,"",0,serialport, true);
				start.execute();
			}
		});
		btnAlignToGreen.setEnabled(false);
		btnAlignToGreen.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		btnAlignToGreen.setBounds(20, 107, 110, 18);
		rgbpanel.add(btnAlignToGreen);
		
		this.commonpanel = new JPanel();
		commonpanel.setBackground(null);
		commonpanel.setBorder(new TitledBorder(null, "COMMON CONTROL", TitledBorder.CENTER, TitledBorder.TOP,  new Font("Calibri Light", Font.BOLD, 11), new Color(51, 51, 51)));
		commonpanel.setBounds(190, 7, 321, 54);
		frmLWlCalibration.getContentPane().add(commonpanel);
		commonpanel.setLayout(null);
		
		this.commonslider = new JSlider(JSlider.HORIZONTAL,FPS_MIN, 255, FPS_INIT);
		commonslider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				commonpercent.setText(Integer.toString(commonslider.getValue()));
			
				if(!source.getValueIsAdjusting())
				{
					
					if(!Align_Flag)
					{
						
						Maximum_White_Value = commonslider.getMaximum();
						System.out.println("Maximum Value of White Slider : " + Maximum_White_Value);
						//Set Red , Green and Blue Sliders positions
						Current_White_Position =  commonslider.getValue();
						System.out.println("Current Position of the slider : " + Current_White_Position);
						//if current_white_position is incremented
						if(Current_White_Position > Previous_Value)
						{
							System.out.println("Increment!!");
							Delta = Current_White_Position - Previous_Value;
							System.out.println("DELTA : " + Delta);
							Align_Flag = true;
							
							//Set Green Slider Value here
							int green_val = Math.round(MFactor_Green_Slider * Delta);
							System.out.println("New Green Val : " + green_val);
							int gsv = greenslider.getValue() + green_val;
							greenslider.setValue(gsv);
							
							//Set Red Slider Value here
							int red_val = Math.round(MFactor_Red_Slider *Delta);
							System.out.println("New Red Val : " + red_val);
							int rsv = redslider.getValue() + red_val;
							redslider.setValue(rsv);
							
							//Set Blue Slider Value here
							int blue_val = Math.round(MFactor_Blue_Slider * Delta);
							System.out.println("New Blue Val : " + blue_val);
							int bsv = blueslider.getValue() + blue_val;
							blueslider.setValue(bsv);
							Align_Flag = false;
							 
							String rgb =  ",#" + rsv + "," + gsv + "," + bsv; 
							
							Calibration_Worker start = new Calibration_Worker(Calibration_RGB,rgb,commonslider.getValue(),serialport, true);
							start.execute();
						}
						//If current_white_position is decremented
						else
						{
							System.out.println("Decrement!!");
							Delta = Previous_Value - Current_White_Position;
							
							Align_Flag = true;
							
							//Set Green Slider Value here
							int green_val = Math.round(MFactor_Green_Slider * Delta);
							System.out.println("New Green Val : " + green_val);
							int gsv = greenslider.getValue() - green_val;
							greenslider.setValue(gsv);
							
							//Set Red Slider Value here
							int red_val = Math.round(MFactor_Red_Slider * Delta);
							System.out.println("New Red Val : " + red_val);
							int rsv = redslider.getValue() - red_val;
							redslider.setValue(rsv);
							
							//Set Blue Slider Value here
							int blue_val = Math.round(MFactor_Blue_Slider * Delta);
							System.out.println("New Blue Val : " + blue_val);
							int bsv = blueslider.getValue() - blue_val;
							blueslider.setValue(bsv);
							
							Align_Flag = false;
							String rgb =  ",#" + rsv + "," + gsv + "," + bsv; 
							Calibration_Worker start = new Calibration_Worker(Calibration_RGB,rgb,commonslider.getValue(),serialport, true);
							start.execute();
						}
						//Update the previous value from the slider's current position
						
						Previous_Value = commonslider.getValue();
						System.out.println("Delta : " + Delta);
						//Maximum_White_Value = Current_White_Position;
						System.out.println("Maximum White Value : " + Maximum_White_Value);
						//Load the red , green and blue sliders with respective values
						
						
						//Send the values to the device
						//First run the worker to update the Actual White Slider Value to the unit
						
						
						
					}
					
					//System.out.println(redslider.getValue());
				}
				
			}
		});
		commonslider.setEnabled(false);
	
		commonslider.setBounds(70, 17, 200, 23);
		commonpanel.add(commonslider);
		
		this.lblC = new JLabel("WHITE LIGHT");
		lblC.setEnabled(false);
		lblC.setFont(new Font("Calibri Light", Font.PLAIN, 10));
		lblC.setBounds(13, 22, 57, 14);
		commonpanel.add(lblC);
		
		this.commonpercent = new JLabel("0");
		commonpercent.setEnabled(false);
		commonpercent.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		commonpercent.setBounds(275, 20, 40, 14);
		commonpanel.add(commonpercent);
		
		this.controlpanel = new JPanel();
		controlpanel.setBorder(new TitledBorder(null, "CALIBRATION CONTROLS", TitledBorder.CENTER, TitledBorder.TOP,  new Font("Calibri Light", Font.BOLD, 11), new Color(51, 51, 51)));
		controlpanel.setBackground(null);
		controlpanel.setBounds(10, 63, 179, 321);
		frmLWlCalibration.getContentPane().add(controlpanel);
		controlpanel.setLayout(null);
		
		this.btnStart = new JButton("START");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			if(!rdbtnManual.isSelected() && !rdbtnAuto.isSelected())
			{
				JOptionPane.showMessageDialog(Calibration.frmLWlCalibration, "Please Choose a Mode", "Calibration Mode Selection", JOptionPane.ERROR_MESSAGE);
			}
			else
			{
				if(serialport != null)
				{
					//Command string - "START_CALIBRATION"
					//Which_DAC - None
					//Intensity Level - 0
					//serialport - COMPort
					//Listener Flag for waiting for the data to arrive - true.
					
					Calibration_Worker start = new Calibration_Worker(Calibration_Start,"",0,serialport, true);
					start.execute();
				}
				else
				{
					JOptionPane.showMessageDialog(frmLWlCalibration, "Invalid Serial Port", "COM Port", JOptionPane.ERROR_MESSAGE);	
				}
			}
				
			}
		});
		btnStart.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		btnStart.setBounds(44, 30, 89, 25);
		controlpanel.add(btnStart);
		
		this.btnEnd = new JButton("END");
		btnEnd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Calibration_Worker start = new Calibration_Worker(Calibration_End,"",0,serialport, true);
				start.execute();
			}
		});
		btnEnd.setEnabled(false);
		btnEnd.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		btnEnd.setBounds(44, 230, 89, 25);
		controlpanel.add(btnEnd);
		
		this.btnVerify = new JButton("VERIFY");
		btnVerify.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Calibration_Worker start = new Calibration_Worker(Calibration_Verify,"",0,serialport, true);
				start.execute();
			}
		});
		btnVerify.setEnabled(false);
		btnVerify.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		btnVerify.setBounds(45, 190, 89, 25);
		controlpanel.add(btnVerify);
		
		this.btnSave = new JButton("SAVE");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Calibration_Worker start = new Calibration_Worker(Calibration_Save,"",0,serialport, true);
				start.execute();
			}
		});
		btnSave.setEnabled(false);
		btnSave.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		btnSave.setBounds(44, 150, 89, 25);
		controlpanel.add(btnSave);
		
		this.btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Calibration_Worker start = new Calibration_Worker(Calibration_End,"",0,serialport, false);
				start.execute();
				System.out.println("Exiing the current page and opening the main page");
				Calibration.frmLWlCalibration.dispose();
				serialport.closePort();
				Stryk_Demo.frmStrykerVer.setVisible(true);
			}
		});
		btnExit.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		btnExit.setBounds(44, 270, 89, 23);
		controlpanel.add(btnExit);
		
		this.btnReadAdc = new JButton("READ ADC");
		btnReadAdc.setEnabled(false);
		btnReadAdc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Calibration_Worker start = new Calibration_Worker(Calibration_ReadADC,"",0,serialport, true);
				start.execute();
			}
		});
		btnReadAdc.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		btnReadAdc.setBounds(44, 70, 89, 23);
		controlpanel.add(btnReadAdc);
		
		this.btnReadCs = new JButton("READ CS");
		btnReadCs.setEnabled(false);
		btnReadCs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Calibration_Worker start = new Calibration_Worker(Calibration_ReadCS,"",0,serialport, true);
				start.execute();
			}
		});
		btnReadCs.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		btnReadCs.setBounds(44, 110, 89, 23);
		controlpanel.add(btnReadCs);
		
		this.colorsensorpanel = new JPanel();
		colorsensorpanel.setBounds(190, 278, 321, 105);
		colorsensorpanel.setBackground(null);
		colorsensorpanel.setBorder(new TitledBorder(null, "CS", TitledBorder.CENTER, TitledBorder.TOP,  new Font("Calibri Light", Font.BOLD, 11), new Color(51, 51, 51)));
		frmLWlCalibration.getContentPane().add(colorsensorpanel);
		colorsensorpanel.setLayout(null);
		
		this.lblCS1 = new JLabel("CS-1");
		lblCS1.setEnabled(false);
		lblCS1.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblCS1.setBounds(20, 34, 22, 14);
		colorsensorpanel.add(lblCS1);
		
		this.lblCS2 = new JLabel("CS-2");
		lblCS2.setEnabled(false);
		lblCS2.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		lblCS2.setBounds(20, 66, 22, 14);
		colorsensorpanel.add(lblCS2);
		
		this.formattedTextField_cs1a = new JTextField();
		formattedTextField_cs1a.setText("0");
		formattedTextField_cs1a.setEnabled(false);
		formattedTextField_cs1a.setEditable(false);
		formattedTextField_cs1a.setBounds(52, 28, 53, 25);
		colorsensorpanel.add(formattedTextField_cs1a);
		
		this.formattedTextField_cs1b = new JTextField();
		formattedTextField_cs1b.setText("0");
		formattedTextField_cs1b.setEnabled(false);
		formattedTextField_cs1b.setEditable(false);
		formattedTextField_cs1b.setBounds(115, 28, 54, 25);
		colorsensorpanel.add(formattedTextField_cs1b);
		
		this.formattedTextField_cs1c = new JTextField();
		formattedTextField_cs1c.setText("0");
		formattedTextField_cs1c.setEnabled(false);
		formattedTextField_cs1c.setEditable(false);
		formattedTextField_cs1c.setBounds(179, 28, 54, 25);
		colorsensorpanel.add(formattedTextField_cs1c);
		
		this.formattedTextField_cs1d = new JTextField();
		formattedTextField_cs1d.setText("0");
		formattedTextField_cs1d.setEnabled(false);
		formattedTextField_cs1d.setEditable(false);
		formattedTextField_cs1d.setBounds(243, 28, 54, 25);
		colorsensorpanel.add(formattedTextField_cs1d);
		
		this.formattedTextField_cs2a = new JTextField();
		formattedTextField_cs2a.setText("0");
		formattedTextField_cs2a.setEnabled(false);
		formattedTextField_cs2a.setEditable(false);
		formattedTextField_cs2a.setBounds(52, 60, 54, 25);
		colorsensorpanel.add(formattedTextField_cs2a);
		
		this.formattedTextField_cs2b = new JTextField();
		formattedTextField_cs2b.setText("0");
		formattedTextField_cs2b.setEnabled(false);
		formattedTextField_cs2b.setEditable(false);
		formattedTextField_cs2b.setBounds(115, 60, 54, 25);
		colorsensorpanel.add(formattedTextField_cs2b);
		
		this.formattedTextField_cs2c = new JTextField();
		formattedTextField_cs2c.setText("0");
		formattedTextField_cs2c.setEnabled(false);
		formattedTextField_cs2c.setEditable(false);
		formattedTextField_cs2c.setBounds(179, 60, 54, 25);
		colorsensorpanel.add(formattedTextField_cs2c);
		
		this.formattedTextField_cs2d = new JTextField();
		formattedTextField_cs2d.setText("0");
		formattedTextField_cs2d.setEnabled(false);
		formattedTextField_cs2d.setEditable(false);
		formattedTextField_cs2d.setBounds(243, 60, 54, 25);
		colorsensorpanel.add(formattedTextField_cs2d);
		
		this.csred= new JLabel("R");
		csred.setEnabled(false);
		csred.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		csred.setBounds(72, 13, 14, 14);
		colorsensorpanel.add(csred);
		
		this.csgreen = new JLabel("G");
		csgreen.setEnabled(false);
		csgreen.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		csgreen.setBounds(135, 12, 14, 14);
		colorsensorpanel.add(csgreen);
		
		this.csblue = new JLabel("B");
		csblue.setEnabled(false);
		csblue.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		csblue.setBounds(199, 12, 14, 14);
		colorsensorpanel.add(csblue);
		
		this.cswl = new JLabel("WL");
		cswl.setEnabled(false);
		cswl.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		cswl.setBounds(260, 12, 22, 14);
		colorsensorpanel.add(cswl);
		
		this.Modeselectionpanel = new JPanel();
		Modeselectionpanel.setBounds(10, 7, 179, 54);
		Modeselectionpanel.setBorder(new TitledBorder(null, "MODE SELECTION", TitledBorder.CENTER, TitledBorder.TOP, new Font("Calibri Light", Font.BOLD, 11), new Color(51, 51, 51)));
		Modeselectionpanel.setBackground(null);
		frmLWlCalibration.getContentPane().add(Modeselectionpanel);
		Modeselectionpanel.setLayout(null);
		

		
		this.adcpanel = new JPanel();
		adcpanel.setBounds(190, 197, 200, 80);
		adcpanel.setBackground(null);
		adcpanel.setBorder(new TitledBorder(null, "ADC", TitledBorder.CENTER, TitledBorder.TOP, new Font("Calibri Light", Font.BOLD, 11), new Color(51, 51, 51)));
		frmLWlCalibration.getContentPane().add(adcpanel);
		adcpanel.setLayout(null);
		
		this.formattedTextField_adcred = new JTextField();
		formattedTextField_adcred.setText("0");
		formattedTextField_adcred.setEnabled(false);
		formattedTextField_adcred.setEditable(false);
		formattedTextField_adcred.setBounds(10, 25, 53, 26);
		adcpanel.add(formattedTextField_adcred);
		
		this.formattedTextField_adcgreen = new JTextField();
		formattedTextField_adcgreen.setText("0");
		formattedTextField_adcgreen.setEnabled(false);
		formattedTextField_adcgreen.setEditable(false);
		formattedTextField_adcgreen.setBounds(73, 25, 53, 26);
		adcpanel.add(formattedTextField_adcgreen);
		
		this.formattedTextField_adcblue = new JTextField();
		formattedTextField_adcblue.setText("0");
		formattedTextField_adcblue.setEnabled(false);
		formattedTextField_adcblue.setEditable(false);
		formattedTextField_adcblue.setBounds(136, 25, 53, 26);
		adcpanel.add(formattedTextField_adcblue);
		
		this.adcred = new JLabel("R");
		adcred.setEnabled(false);
		adcred.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		adcred.setBounds(32, 11, 25, 14);
		adcpanel.add(adcred);
		
		this.adcgreen = new JLabel("G");
		adcgreen.setEnabled(false);
		adcgreen.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		adcgreen.setBounds(93, 11, 17, 14);
		adcpanel.add(adcgreen);
		
		this.adcblue = new JLabel("B");
		adcblue.setEnabled(false);
		adcblue.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		adcblue.setBounds(157, 11, 25, 14);
		adcpanel.add(adcblue);
		
		this.redamps = new JLabel("(A)");
		redamps.setEnabled(false);
		redamps.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		redamps.setBounds(32, 52, 17, 14);
		adcpanel.add(redamps);
		
		this.greenamps = new JLabel("(A)");
		greenamps.setEnabled(false);
		greenamps.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		greenamps.setBounds(94, 51, 17, 14);
		adcpanel.add(greenamps);
		
		this.blueamps = new JLabel("(A)");
		blueamps.setEnabled(false);
		blueamps.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		blueamps.setBounds(157, 51, 17, 14);
		adcpanel.add(blueamps);
		
		this.Wlcpanel = new JPanel();
		Wlcpanel.setBounds(392, 197, 119, 80);
		Wlcpanel.setBackground(null);
		Wlcpanel.setBorder(new TitledBorder(null, "COORDINATES", TitledBorder.CENTER, TitledBorder.TOP, new Font("Calibri Light", Font.BOLD, 10), new Color(51, 51, 51)));
		frmLWlCalibration.getContentPane().add(Wlcpanel);
		Wlcpanel.setLayout(null);
		
		this.formattedTextField_wlcx = new JTextField();
		formattedTextField_wlcx.setBounds(10, 25, 47, 26);
		Wlcpanel.add(formattedTextField_wlcx);
		formattedTextField_wlcx.setText("0");
		formattedTextField_wlcx.setEnabled(false);
		formattedTextField_wlcx.setEditable(false);
		
		this.adcwl = new JLabel("WLC");
		adcwl.setEnabled(false);
		adcwl.setBounds(44, 11, 25, 14);
		Wlcpanel.add(adcwl);
		adcwl.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		
		this.formattedTextField_wlcy = new JTextField();
		formattedTextField_wlcy.setText("0");
		formattedTextField_wlcy.setEnabled(false);
		formattedTextField_wlcy.setEditable(false);
		formattedTextField_wlcy.setBounds(52, 25, 49, 26);
		Wlcpanel.add(formattedTextField_wlcy);
		
		//Map components of a panel to its panel array
		calibration_controls = controlpanel.getComponents();
		rgb_controls = rgbpanel.getComponents();
		adc_panel = adcpanel.getComponents();
		coordinates_panel = Wlcpanel.getComponents();
		colorsensor_panel = colorsensorpanel.getComponents();
		common_controls = commonpanel.getComponents();
		
		//RadioButton Manual actionlistener event
		this.rdbtnManual = new JRadioButton("MANUAL");
		rdbtnManual.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED)
				{
					//unselect the freeze checkboxes, if in case they were previously selected
					if(chckbxredfreeze.isSelected())
					{
						chckbxredfreeze.setSelected(false);
					}
					if(chckbxgreenfreeze.isSelected())
					{
						chckbxgreenfreeze.setSelected(false);
					}
					if(chckbxbluefreeze.isSelected())
					{
						chckbxbluefreeze.setSelected(false);
					}
					System.out.println("Manual Option Selected");
					btnStart.setEnabled(true);
					
					//Enable the components of rgb control panel
					for(Component c : rgbpanel.getComponents())
					{
						c.setEnabled(true);
					}
					//Enable the components of adc display panel
					for(Component c: adcpanel.getComponents())
					{
							c.setEnabled(true);
					}
					//Enable the components of coordinates display panel
					for(Component c: coordinates_panel)
					{
						c.setEnabled(true);
					}
					//Enable the components in colorsensor panel
					for(Component c : colorsensor_panel)
					{
						c.setEnabled(true);
					}
				}
				else 
				{
					System.out.println("Deselected");
					
					//Disable the components of rgb control panel
					for(Component c : rgbpanel.getComponents())
					{
						
						c.setEnabled(false);
					}
					//Disable the components of adc display panel
					for(Component c: adcpanel.getComponents())
					{
							c.setEnabled(false);
					}
					//Disable the components of coordinates display panel
					for(Component c: coordinates_panel)
					{
						c.setEnabled(false);
					}
					//Disable the components in colorsensor panel
					for(Component c : colorsensor_panel)
					{
						c.setEnabled(false);
					}
				}
				
			}
		});
		rdbtnManual.setBounds(87, 16, 76, 23);
		Modeselectionpanel.add(rdbtnManual);
		rdbtnManual.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		modeselection.add(rdbtnManual);
		//Default states of UI
		modeselection.clearSelection();
		//Event Listsner for Auto Mode RadioButton
		this.rdbtnAuto = new JRadioButton("AUTO");
		rdbtnAuto.setSelected(true);
		rdbtnAuto.setBounds(16, 16, 60, 23);
		Modeselectionpanel.add(rdbtnAuto);
		rdbtnAuto.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		modeselection.add(rdbtnAuto);
		
		JLabel lblForEvalualtionPurposes = new JLabel("For Evalualtion Purposes Only!!");
		lblForEvalualtionPurposes.setFont(new Font("Calibri Light", Font.BOLD, 20));
		lblForEvalualtionPurposes.setBounds(250, 382, 300, 18);
		frmLWlCalibration.getContentPane().add(lblForEvalualtionPurposes);
		
		this.Calibrationlogspanel = new JPanel();
		Calibrationlogspanel.setBackground(null);
		Calibrationlogspanel.setBorder(new TitledBorder(null, "LOGS", TitledBorder.CENTER, TitledBorder.TOP,  new Font("Calibri Light", Font.BOLD, 11), new Color(51, 51, 51)));
		Calibrationlogspanel.setBounds(512, 7, 301, 375);
		frmLWlCalibration.getContentPane().add(Calibrationlogspanel);
		Calibrationlogspanel.setLayout(null);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBounds(25, 35, 250, 400);
		//Calibrationlogspanel.add(textArea);
		this.scroll = new JScrollPane(textArea,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroll.setBounds(20, 30, 262, 320);
		Calibrationlogspanel.add(scroll);
		scroll.setBackground(Color.WHITE);
		
		JRadioButton rdbtnClearLogs = new JRadioButton("CLEAR LOGS");
		rdbtnClearLogs.addItemListener(new ItemListener() {
			private Timer timer;
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED)
				{
					textArea.setText("");
					
					rdbtnClearLogs.setEnabled(false);
					
					
					ActionListener taskPerformer = new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
				// ...Perform a task...
							rdbtnClearLogs.setSelected(false);
							rdbtnClearLogs.setEnabled(true);
						
							timer.stop();
						}
					};
					this.timer = new Timer(100, taskPerformer);
					timer.setRepeats(false);
					timer.start();
				}
			}
		});
		rdbtnClearLogs.setFont(new Font("Calibri Light", Font.PLAIN, 9));
		rdbtnClearLogs.setBounds(210, 7, 71, 23);
		Calibrationlogspanel.add(rdbtnClearLogs);
		
		frmLWlCalibration.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		UIManager.LookAndFeelInfo looks[] = UIManager.getInstalledLookAndFeels();
		try {
			UIManager.setLookAndFeel(looks[1].getClassName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // For MAC
		//Calibration window = new Calibration();
		frmLWlCalibration.setVisible(true);
		System.out.println("Initialized");
	}
}
