import java.awt.CheckboxGroup;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.UIManager;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JLabel;
import java.awt.TextArea;
import javax.swing.JTextPane;
/**
 * 
 */

/**
 * @author LakshmiNarasimman R
 *
 */
public class ObservationSettings2 {
	public static final ButtonGroup chckbxgrp = new ButtonGroup();
	public static JButton btnFinish;
	
	//CheckBox Definitions goes here
	public static JCheckBox chckbxLightEngine;
	public static JCheckBox chckbxExternalCamFeed;
	public static JCheckBox chckbxOpticFiber;
	public static JCheckBox chckbxUartLoopBack;
	
	public static JTextPane loadlabelpane;
	public static JTextPane UartLoopPane;
	public static JTextPane ExternalPwmFeedPane;
	public static JTextPane Fiber1DetectPane;
	public static JTextPane Fiber2DetectPane;
	public static JTextPane BeamsensorPane;
	public static JTextPane ESSTPane;
	public static JTextPane ASSTPane;
	public static JTextPane FanConnectedPane;
	
	//Frame Definitions goes here
	public static JFrame frmObservationSettings2;
	public static JCheckBox chckbxIrisModule;
	public static JCheckBox chckbxEsst;
	public static JCheckBox chckbxAsst;
	public static JCheckBox chckbxFans;
	private JTextPane textPane;
	private JTextPane textPane_1;
	private JTextPane textPane_2;
	private JTextPane textPane_3;
	private JTextPane textPane_4;
	private JTextPane textPane_5;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ObservationSettings2 window = new ObservationSettings2();
					window.frmObservationSettings2.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ObservationSettings2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmObservationSettings2 = new JFrame();
		frmObservationSettings2.setTitle("Test Configuration");
		frmObservationSettings2.setBounds(100, 100, 402, 313);
		frmObservationSettings2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmObservationSettings2.setForeground(UIManager.getColor("InternalFrame.borderColor"));

		frmObservationSettings2.setBackground(UIManager.getColor("InternalFrame.borderColor"));
		frmObservationSettings2.getContentPane().setBackground(SystemColor.textHighlightText);
		frmObservationSettings2.getContentPane().setLayout(null);
		
		this.btnFinish = new JButton("Finish->");
		btnFinish.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ManufacturingTests.MFTest_Flag = true;
				ManufacturingTests.finish_button_flag = true;
				
				Stryk_Demo.send_data_in_bg("ManufacturingTest", Stryk_Demo.btnManufacturingTests, true, Stryk_Demo.Manufacturing_Test.getBytes());
				
			}
		});
		btnFinish.setBounds(136, 225, 89, 23);
		frmObservationSettings2.getContentPane().add(btnFinish);
		
		this.chckbxLightEngine = new JCheckBox("LIGHT ENGINE");
		chckbxLightEngine.setSelected(true);
		chckbxLightEngine.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED)
				{
					ObservationSettings.withload = true;
					loadlabelpane.setText("YES");
					
				}
				if(e.getStateChange() == ItemEvent.DESELECTED)
				{
					loadlabelpane.setText("NO");
				}
			}
		});
		chckbxLightEngine.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxLightEngine.setBounds(120, 7, 120, 23);
		
		frmObservationSettings2.getContentPane().add(chckbxLightEngine);
		
		this.chckbxExternalCamFeed = new JCheckBox("EXTERNAL CAM FEED");
		chckbxExternalCamFeed.setSelected(true);
		chckbxExternalCamFeed.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxExternalCamFeed.setBounds(120, 85, 131, 23);
		frmObservationSettings2.getContentPane().add(chckbxExternalCamFeed);
		
		this.chckbxOpticFiber = new JCheckBox("OPTIC FIBER");
		chckbxOpticFiber.setSelected(true);
		chckbxOpticFiber.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxOpticFiber.setBounds(120, 137, 120, 23);
		frmObservationSettings2.getContentPane().add(chckbxOpticFiber);
		
		this.chckbxUartLoopBack = new JCheckBox("UART LOOPBACK ");
		chckbxUartLoopBack.setSelected(true);
		chckbxUartLoopBack.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED)
				{
					UartLoopPane.setText("YES");
				}
				else if(e.getStateChange() == ItemEvent.DESELECTED)
				{
					UartLoopPane.setText("NO");
				}
			}
		});
		chckbxUartLoopBack.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxUartLoopBack.setBounds(120, 111, 159, 23);
		frmObservationSettings2.getContentPane().add(chckbxUartLoopBack);
		
		this.loadlabelpane = new JTextPane();
		loadlabelpane.setFont(new Font("Calibri Light", Font.PLAIN, 12));
		loadlabelpane.setBounds(343, 5, 45, 20);
		loadlabelpane.setEditable(false);
		
		frmObservationSettings2.getContentPane().add(loadlabelpane);
		
		this.chckbxIrisModule = new JCheckBox("IRIS MODULE");
		chckbxIrisModule.setSelected(true);
		chckbxIrisModule.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxIrisModule.setBounds(120, 33, 97, 23);
		frmObservationSettings2.getContentPane().add(chckbxIrisModule);
		
		this.chckbxEsst = new JCheckBox("ESST");
		chckbxEsst.setSelected(true);
		chckbxEsst.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxEsst.setBounds(120, 163, 97, 23);
		frmObservationSettings2.getContentPane().add(chckbxEsst);
		
		this.chckbxAsst = new JCheckBox("ASST");
		chckbxAsst.setSelected(true);
		chckbxAsst.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxAsst.setBounds(120, 189, 97, 23);
		frmObservationSettings2.getContentPane().add(chckbxAsst);
		
		this.chckbxFans = new JCheckBox("FANs");
		chckbxFans.setSelected(true);
		chckbxFans.setFont(new Font("Calibri Light", Font.PLAIN, 11));
		chckbxFans.setBounds(120, 59, 120, 23);
		frmObservationSettings2.getContentPane().add(chckbxFans);
		
		this.UartLoopPane = new JTextPane();
		UartLoopPane.setFont(new Font("Calibri Light", Font.PLAIN, 12));
		
		UartLoopPane.setBounds(353, 30, 35, 20);
		UartLoopPane.setEditable(false);
		frmObservationSettings2.getContentPane().add(UartLoopPane);
		
		this.ExternalPwmFeedPane = new JTextPane();
		ExternalPwmFeedPane.setBounds(363, 62, 25, 20);
		frmObservationSettings2.getContentPane().add(ExternalPwmFeedPane);
		
		this.Fiber1DetectPane = new JTextPane();
		Fiber1DetectPane.setBounds(340, 88, 48, 20);
		frmObservationSettings2.getContentPane().add(Fiber1DetectPane);
		
		this.Fiber2DetectPane = new JTextPane();
		Fiber2DetectPane.setBounds(350, 114, 38, 20);
		frmObservationSettings2.getContentPane().add(Fiber2DetectPane);
		
		this.BeamsensorPane = new JTextPane();
		BeamsensorPane.setBounds(343, 137, 45, 20);
		frmObservationSettings2.getContentPane().add(BeamsensorPane);
		
		this.ESSTPane = new JTextPane();
		ESSTPane.setBounds(353, 163, 35, 20);
		frmObservationSettings2.getContentPane().add(ESSTPane);
		
		this.ASSTPane = new JTextPane();
		ASSTPane.setBounds(363, 192, 25, 20);
		frmObservationSettings2.getContentPane().add(ASSTPane);
		
		this.FanConnectedPane = new JTextPane();
		FanConnectedPane.setBounds(351, 215, 37, 20);
		frmObservationSettings2.getContentPane().add(FanConnectedPane);
		
		
		
		
	}
}
